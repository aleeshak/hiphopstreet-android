package com.goigi.android.hiphopstreet.home;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.goigi.android.hiphopstreet.MainActivity;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.adapter.DashboardAdapter;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.TopReleases;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;

import android.R.string;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;


public class HomepageActivity extends MainActivity {
    private RecyclerView recyclerView;
    public ArrayList<TopReleases> TopalbumArraylist;
   //public ArrayList<TopReleases> TopSongsArraylist;
    ConnectionDetector connection;
    private int[] image={R.drawable.ic_launcher,R.drawable.ic_launcher};
    String[] title={"jubn","mkjki"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_main, container);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        connection=new ConnectionDetector(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new GridLayoutManager(HomepageActivity.this,2);
        recyclerView.setLayoutManager(layoutManager);
        callasyntask();
    }

	

	private void callasyntask() {
		// TODO Auto-generated method stub
		GeneralAsynctask submitAsync = new GeneralAsynctask(
				this) {

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {

					JSONObject jobj=new JSONObject(result);
								
					String Ack = jobj.getString("status");
					
					if (Ack.equals("success"))  {
						
						JSONArray jarray=jobj.getJSONArray("songanduserlist");
						
						TopalbumArraylist=new ArrayList<TopReleases>();
						
						for (int i = 0; i < jarray.length(); i++) {
							
							JSONObject jobj1=jarray.getJSONObject(i);
							
							TopReleases item=new TopReleases(jobj1.getString("songid"), jobj1.getString("song_name"),jobj1.getString("song_category"),jobj1.getString("song_image"),jobj1.getString("song_file"));

							
							TopalbumArraylist.add(item);
							
							
						}
						
						setlist();
					}
					
					else {
						
						showToastLong("No Songs in This Week");
					}
					
					
					
					

				} catch (Exception e) {
					e.printStackTrace();
					// showToastLong("Error: Unable to Login.");
				} finally {
					pdspinnerGeneral.dismiss();
				}
			}

		};

		try {
			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {

				Log.d("LogIn", "Action: login");
				
				 /* String url=getResources().getString(R.string.app_base_url)+
				  "Login/Email="+email+",Password="+password.trim(); String
				  finalurl=url.replaceAll(" ", "%20");*/
				 

				String url = getResources().getString(R.string.app_base_url)
						+ "songlist";
				String finalurl = url.replaceAll(" ", "%20");

				submitAsync.execute(finalurl);

			} else {
				showToastLong(getString(R.string.no_internet_message));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showToastLong(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}

	protected void setlist() {
		// TODO Auto-generated method stub
	
		
			 DashboardAdapter dashboardAdapter = new DashboardAdapter(HomepageActivity.this,TopalbumArraylist);
		     recyclerView.setAdapter(dashboardAdapter);
		 
	}
}

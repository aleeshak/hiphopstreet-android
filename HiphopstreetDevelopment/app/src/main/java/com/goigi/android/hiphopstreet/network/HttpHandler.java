package com.goigi.android.hiphopstreet.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.StrictMode;
import android.util.Log;


public class HttpHandler {

	static InputStream is = null;
	static JSONArray jArray = null;
	static JSONObject jObj = null;
	static String json = "";

	// constructor
	public HttpHandler() {

	}

	public JSONArray getJSONArray(String url) {
		// try parse the string to a JSON array

		getJSONStringFromUrl(url);

		try {
			Log.v("------JSONArray-----", "In Try");
			jArray = new JSONArray(json);


		} catch (JSONException e) {
			Log.e("JSON Parser Array",
					"Error parsing jsonArray " + e.toString());


		}

		// return JSON String
		return jArray;

	}

	public JSONObject getJSONObject(String url) {
		// try parse the string to a JSON object

		getJSONStringFromUrl(url);
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Parser Object Sumana ---------------------> ",
					"Error parsing jsonObject " + e.toString());
		}

		// return JSON String
		return jObj;


	}

	public String getJSONStringFromUrl(String url) {

		// Making HTTP request
		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
			.permitAll().build();

			StrictMode.setThreadPolicy(policy);
			// defaultHttpClient
			/*DefaultHttpClient httpClient = new DefaultHttpClient();*/
			HttpClient httpClient = new DefaultHttpClient();
			/*HttpPost httpPost = new HttpPost(url);*/
			HttpGet httpPost = new HttpGet(url);
			httpPost.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);

			/*
					BufferedReader reader = new BufferedReader(new InputStreamReader(
							is, "utf-8"), 8);*/
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error Sumana --------->", "Error converting result " + e.toString());
		}

		// return JSON String
		Log.i("RETURN JSON :-------------- > ",json);
		return json;

	}

	public String getJSONStringFromUrl(String url,Map<String, String> map) {

		// Making HTTP request
		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
			.permitAll().build();

			StrictMode.setThreadPolicy(policy);


			ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
			Iterator<String> it = map.keySet().iterator();
			while (it.hasNext()) {
				String key = it.next();
				nameValuePair.add(new BasicNameValuePair(key, map.get(key)));
			}
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			//HttpGet httpPost = new HttpGet(url);
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair, "UTF-8"));

			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());

		}

		// return JSON String
		Log.i("RETURN JSON: ",json);
		return json;

	}
}

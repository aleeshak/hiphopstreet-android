package com.goigi.android.hiphopstreet;






import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import com.goigi.android.hiphopstreet.UserPanel.UserDashBoard;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.home.HomepageActivity;
import customfonts.MyEditText;
import customfonts.MyTextView;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SigninActivity extends AppCompatActivity implements OnClickListener {

    
    ImageView back,signup;
    TextView signin_img;
    private MyTextView signup_now,forgot_password,artist_tv,signup_tv;
	private EditText username,password;
	private TextView login,done_btn;
	private ProgressDialog progressDialog;
	ConnectionDetector connection;
	private MultipartEntity reqEntity;
	String PASSWORD,USERNAME;
	SharedPreferences prefs;
	public Boolean isartist;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);
        connection=new ConnectionDetector(this);
		prefs=getSharedPreferences("MY_SHARED_PREF", Context.MODE_PRIVATE);
        xmlinitialisation();
        xmlonclick();
               
       
    }


private void xmlinitialisation() {
		// TODO Auto-generated method stub
signup_tv = (MyTextView)findViewById(R.id.register_btn_textviewww);
        
signin_img=(TextView)findViewById(R.id.login_button);
username=(EditText)findViewById(R.id.email_edt);
password=(EditText)findViewById(R.id.password_edtt);        	
forgot_password=(MyTextView)findViewById(R.id.forgotpassowd);
artist_tv=(MyTextView)findViewById(R.id.artist);



}


	private void xmlonclick() {
		// TODO Auto-generated method stub
	signin_img.setOnClickListener(this);	
	signup_tv.setOnClickListener(this);
	forgot_password.setOnClickListener(this);
	artist_tv.setOnClickListener(this);
	
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	switch (v.getId()) {
	case R.id.login_button:
		
		loginvalidation();
		
		
		
		break;

	case R.id.register_btn_textviewww:
		Intent it = new Intent(SigninActivity.this,SignupActivity.class);
        it.putExtra("key", "user");
		
		
		startActivity(it);
	break;
	
	case R.id.forgotpassowd:
	Intent i=new Intent(SigninActivity.this,Forgotpassword.class);
	startActivity(i);
	break;
	
	case R.id.artist:
		Intent iti = new Intent(SigninActivity.this,SignupActivity.class);
        iti.putExtra("key", "artist");
				
		startActivity(iti);
	    
        
        
        break;
	
	
	
	default:
		break;
	}
	
	
	
	
	}


	    private void loginvalidation() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				USERNAME = username.getText().toString().trim();
				PASSWORD = password.getText().toString().trim();


				boolean cancel = false;
				String message="";
				View focusView = null;
				boolean tempCond = false;




				if(TextUtils.isEmpty(USERNAME)){
					message = "Please enter your Email.";
					focusView = username;
					cancel = true;
					tempCond = false;
				}



				if(TextUtils.isEmpty(PASSWORD)){
					message = "Please enter your Password.";
					focusView = password;
					cancel = true;
					tempCond = false;
				}




				if (cancel) {
					// focusView.requestFocus();
					if (!tempCond) {
						focusView.requestFocus();
					}
					showToastLong(message);
				} else {
					InputMethodManager imm = (InputMethodManager) this
							.getSystemService(Context.INPUT_METHOD_SERVICE);

					try {

						reqEntity = null;
						reqEntity = new MultipartEntity(
								HttpMultipartMode.BROWSER_COMPATIBLE);

						reqEntity.addPart("email_id", new StringBody(USERNAME));
						reqEntity.addPart("password", new StringBody(PASSWORD));

					}

					catch (Exception e) {
						e.printStackTrace();
					}
				
					Boolean isInternetPresent = connection.isConnectingToInternet();

					if (isInternetPresent && !isFinishing()) {
						progressDialog = ProgressDialog.show(this,
								"",
								getString(R.string.progress_bar_loading_message),
								false);
/*
                        progressDialog = new ProgressDialog(this);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage("Loading ...");
                        progressDialog.show();*/

/*Toast.makeText(this,"Loading",Toast.LENGTH_LONG).show();*/
						UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
						editProfileAsyncTask.execute((Void) null);
					} else {
						showToastLong(getString(R.string.no_internet_message));
					}
				}
	
	}

	    public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
			String[] message;
			String responseString = "";

			@Override
			protected Boolean doInBackground(Void... params) {
				// TODO: attempt authentication against a network service.

				try {
					String apiUrl = getResources().getString(R.string.app_base_url)+"login";
					Log.d("TestActivity", "Action: accountupdate");
					responseString = "";

					HttpParams params1 = new BasicHttpParams();
					params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
							HttpVersion.HTTP_1_1);
					HttpClient httpClient = new DefaultHttpClient(params1);
					HttpPost postRequest = new HttpPost(apiUrl);
					
					postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");

					postRequest.setEntity(reqEntity);
					//HttpResponse response = httpClient.execute(postRequest);
                    HttpResponse response = null;

                    try {
                        response =  httpClient.execute(postRequest);
                    } catch (Exception e) {
						e.printStackTrace();

					}
                    BufferedReader reader = new BufferedReader(
							new InputStreamReader(
									response.getEntity().getContent(), "UTF-8"));

					String sResponse;

					StringBuilder s = new StringBuilder();

					while ((sResponse = reader.readLine()) != null) {
						s = s.append(sResponse);
					}

					responseString = s.toString();
					Log.d("ResponseString =>", responseString);

				} catch (Exception e) {
					e.printStackTrace();
				}

				return false;
			}

			@Override
			protected void onPostExecute(final Boolean success) {

				progressDialog.dismiss();
				progressDialog.cancel();

				try {
					if (!responseString.equals("")) {

						JSONObject jsonObject = new JSONObject(responseString);
						
						String Ack = jsonObject.getString("status");
						
					
						
						if (Ack .equals("success") ) {
							//showToastLong("Login Success");
							
							//progressDialog.dismiss();
							//progressDialog.cancel();
							//showToastLong("Data inserted successfully.");
							
							JSONObject jobj=jsonObject.getJSONObject("message");
							if (jobj.getString("user_cat").equals("artist")) {
								
								Editor editor=prefs.edit();
								editor.putString(getString(R.string.shared_user_id),jobj.getString("id"));
								editor.putString(getString(R.string.shared_user_name), jobj.getString("name"));
								editor.putString(getString(R.string.shared_user_email), jobj.getString("email_id"));
								editor.putString(getString(R.string.shared_user_pic), jobj.getString("pic"));
								editor.putString(getString(R.string.shared_user_type), jobj.getString("user_cat"));
								editor.putBoolean(getString(R.string.shared_loggedin_status_artist), true);
								editor.commit();
								Intent i=new Intent(SigninActivity.this,DashBoardActivity.class);
								startActivity(i);
								
							}
							else {
								
								
								Editor editor=prefs.edit();
								editor.putString(getString(R.string.shared_user_id),jobj.getString("id"));
								editor.putString(getString(R.string.shared_user_name), jobj.getString("name"));
								editor.putString(getString(R.string.shared_user_email), jobj.getString("email_id"));
								editor.putString(getString(R.string.shared_user_type), jobj.getString("user_cat"));
								editor.putBoolean(getString(R.string.shared_loggedin_status), true);
								editor.commit();
								Intent i=new Intent(SigninActivity.this,UserDashBoard.class);
								startActivity(i);
							}
							
							
						

						}
						else {
							/*progressDialog.dismiss();
							progressDialog.cancel();*/
							showToastLong("Invalid Email or Password");
						}
					}
					else {
						/*progressDialog.dismiss();
						progressDialog.cancel();*/
						showToastLong("Sorry! Problem cannot be recognized.");
					}
					
					
				} catch (Exception e) {
					progressDialog.dismiss();
					progressDialog.cancel();
					e.printStackTrace();
				}
			}




		}

		public void showToastLong(String string) {
			// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
		
		}


@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	//finish();


	new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert)
	.setTitle("Exit")
	// .setTitleColor(Color.parseColor(HALLOWEEN_ORANGE))
	//   .setDividerColor(HALLOWEEN_ORANGE)
	.setMessage("Are you sure you want to exit?")
	.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {

			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
	}).setNegativeButton("No", null).show();

}




}


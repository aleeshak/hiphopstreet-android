package com.goigi.android.hiphopstreet.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.ViewallActivity;
import com.goigi.android.hiphopstreet.modelclass.Categorylist;
import com.goigi.android.hiphopstreet.modelclass.MusiccategoryModel;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.modelclass.TopReleases;

import customfonts.MyTextView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


    public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
	     Context context;
	     HomeBodyViewPagerAdapter homeViewPagerAdapter;
	     Categoryitemnameadapter catadapter;
	     private ArrayList<TopRatedsong> dataSet;
	     private ArrayList<Categorylist> dataSet_1;
	     private ArrayList<MusiccategoryModel>dataSet_11;
	     private RecyclerView reclyerview;
	    
	    public HomeAdapter(Context context,ArrayList<TopRatedsong> data,ArrayList<Categorylist>favlist) {
	        this.context = context;
	        this.dataSet=data;
	        this.dataSet_1=favlist;

	    }

	    @Override
	    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
	    	 	View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dashboard_item, viewGroup, false);

	        return new ViewHolder(view);
	    }

	    @Override
	    public void onBindViewHolder(HomeAdapter.ViewHolder viewHolder, final int i) {
	       if(i==0){
	    		
	    		viewHolder.header_txt.setText("Most Featured Song");
	    		 LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
	    	        viewHolder.viewPager_body.setLayoutManager(layoutManager);
	    	        
	    	        if(dataSet.size()==0){
	    	        	//viewHolder.viewPager_body.setVisibility(View.GONE);
	    	        }
	    	        else{
	    	        	 homeViewPagerAdapter = new HomeBodyViewPagerAdapter(context,dataSet);
	    	  	       
	    	        	 viewHolder.viewPager_body.setAdapter(homeViewPagerAdapter);
	    	        }
	    	   	      
	    	       
	    	}
	    	else if(i==1){
	    		
	    		viewHolder.header_txt.setText("Categories");
	    		viewHolder.view_all_tv.setVisibility(View.GONE);
	    		
	    		LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
	 	        viewHolder.viewPager_body.setLayoutManager(layoutManager);
	 	      
	 	       if(dataSet_1.size()==0){
		        	

		        }
		        else{

		        	catadapter = new Categoryitemnameadapter(context, dataSet_1);
	    	  	    
   	        	   viewHolder.viewPager_body.setAdapter(catadapter);
		        }
	 	       
	    	}
	    	
	    	   viewHolder.view_all_tv.setOnClickListener(new  OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				//Toast.makeText(context, "Under progress", Toast.LENGTH_LONG).show();
				Intent i=new Intent(context,ViewallActivity.class);
				context.startActivity(i);
				
				
				}
			}) ;
	    
	    }

	    @Override
	    public int getItemCount() {
	        return 2;
	    }

	    public class ViewHolder extends RecyclerView.ViewHolder {
	        ImageView image;
	        MyTextView  view_all_tv,header_txt;
	        RecyclerView viewPager_body;

	        public ViewHolder(View view) {
	            super(view);

	            
	            Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
	            Point size = new Point();
	            display.getSize(size);
	            int width = size.x;
	            int height = size.y;
	            height=height-200;
	            height=height/2;
	           // image = (ImageView) view.findViewById(R.id.image);
	           
	            view_all_tv = (MyTextView) view.findViewById(R.id.view_all);
	            view_all_tv.setVisibility(View.VISIBLE);
	            
	            header_txt=(MyTextView)view.findViewById(R.id.body_header_tv);
	            viewPager_body = (RecyclerView) view.findViewById(R.id.viewPager_body);
	            ViewGroup.LayoutParams params=viewPager_body.getLayoutParams();
	            params.height=height;
	            viewPager_body.setLayoutParams(params);
          	            
	        }
	    }



}

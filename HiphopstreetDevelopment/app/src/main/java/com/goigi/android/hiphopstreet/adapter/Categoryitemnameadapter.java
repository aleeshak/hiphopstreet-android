package com.goigi.android.hiphopstreet.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.home.Mediaplayeractivitynew;
import com.goigi.android.hiphopstreet.modelclass.Categorylist;
import com.goigi.android.hiphopstreet.modelclass.MusiccategoryModel;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import customfonts.MyTextView;

public class Categoryitemnameadapter extends RecyclerView.Adapter<Categoryitemnameadapter.ViewHolder> {
	public class ViewHolder extends RecyclerView.ViewHolder{
		private final ImageView img_android;
		private final MyTextView tv_android,tv_songname;
		public ViewHolder(View view) {
			super(view);

			tv_android = (MyTextView)view.findViewById(R.id.catid2);
			tv_songname=(MyTextView)view.findViewById(R.id.songname2);
			img_android = (ImageView) view.findViewById(R.id.img2);
		}

	}
	JSONObject jobj2;
	String category_name_str;
	ConnectionDetector connection;
	private ArrayList<MusiccategoryModel> musiccategorylist;
	private final Context context;
	private final ArrayList<Categorylist> dataSet_1;
	String Imageview,category_str;
	private  String MY_URL;

	public Categoryitemnameadapter(Context context,ArrayList<Categorylist> dataSet_1) {
		this.dataSet_1 = dataSet_1;
		this.context = context;
        this.musiccategorylist=musiccategorylist;
	}

	@Override
	public int getItemCount() {
		return dataSet_1.size();
	}

	@Override
	public void onBindViewHolder(Categoryitemnameadapter.ViewHolder viewHolder, final int i) {
		connection=new ConnectionDetector(context);
		category_str=dataSet_1.get(i).category_name;
		ImageView imageView = viewHolder.img_android;
		imageView.setImageResource(dataSet_1.get(i).getImage());
		viewHolder.tv_songname.setText(dataSet_1.get(i).category_name);
		viewHolder.itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
												
		  callasynchtask(dataSet_1.get(i).category_name);

		}

			
		});


	}

	@Override
	public Categoryitemnameadapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.buylist_item, viewGroup, false);
		return new ViewHolder(view);
	}



	protected void showToastLong(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(context, string, Toast.LENGTH_LONG).show();
	}


	private void callasynchtask(String songid) {
		// TODO Auto-generated method stub
		GeneralAsynctask submitAsync = new GeneralAsynctask(
				context) {

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {

					JSONObject jobj=new JSONObject(result);
								
					String Ack = jobj.getString("status");
					
					if (Ack.equals("success"))  {
						
						JSONArray jarray=jobj.getJSONArray("songanduserlist");
						
						musiccategorylist=new ArrayList<MusiccategoryModel>();
						
						for (int i = 0; i < jarray.length(); i++) {
							
							JSONObject jobj1=jarray.getJSONObject(i);
							
							jobj2=jobj1.getJSONObject("user");
							
							MusiccategoryModel item=new MusiccategoryModel(jobj1.getString("songid"), 
							jobj1.getString("song_name"),jobj1.getString("song_category"),
							jobj1.getString("song_image"),jobj1.getString("song_file"),
							jobj1.getString("likecount"),
							jobj2.getString("id"),jobj2.getString("name"),
							jobj2.getString("pic"),jobj2.getString("biography"));
							 musiccategorylist.add(item);

						}

						Intent i= new Intent(context,Mediaplayeractivitynew.class);
						i.putExtra("allsong", jarray.toString());
						i.putExtra("id",jobj2.getString("id"));
					//	i.putExtra("id", dataSet_1.get(position).songid);
						context.startActivity(i);



					}
					
					else {
						
						showToastLong("No Songs in This Week");
					}
					
					
				} catch (Exception e) {
					e.printStackTrace();
					// showToastLong("Error: Unable to Login.");
				} finally {
					pdspinnerGeneral.dismiss();
				}
			}

		};

		try {
			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {

				Log.d("LogIn", "Action: login");
				
					String url = context.getResources().getString(R.string.app_base_url)
						+ "songlists/"+songid;
				String finalurl = url.replaceAll(" ", "%20");

				submitAsync.execute(finalurl);

			} else {
				showToastLong(context.getString(R.string.no_internet_message));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}	
	
		
	}

}

package com.goigi.android.hiphopstreet.home;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.entity.mime.MultipartEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.MainActivity;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.SimpleDividerItemDecoration;
import com.goigi.android.hiphopstreet.adapter.DashboardAdapter;

import com.goigi.android.hiphopstreet.adapter.Uploadmusiclistadapter;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.MusicModel;
import com.goigi.android.hiphopstreet.modelclass.TopReleases;
import com.goigi.android.hiphopstreet.modelclass.Uploadmusiclistmodelclass;
import com.goigi.android.hiphopstreet.modelclass.Uploadmusicmodel;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;
import com.goigi.android.hiphopstreet.shared.UserShared;

public class Uploadedmusiclist  extends MainActivity implements OnClickListener{

	public ArrayList<Uploadmusiclistmodelclass> UploadMusicArraylist;
	RecyclerView recyclerView;
	
	ConnectionDetector connection;
	Intent mINTENT;
	private MultipartEntity reqEntity;
	private ProgressDialog progressDialog;
	ImageView noMusic;
	UserShared psh;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getLayoutInflater().inflate(R.layout.upload_music_list, container);
		connection=new ConnectionDetector(this);
		 psh=new UserShared(this);
		 recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
		 recyclerView.setHasFixedSize(true);
		 
		 LinearLayoutManager layoutManager
	        = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
		
		 
		 
		 recyclerView.setLayoutManager(layoutManager);
		 recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
		 
		 uploadmusicapi();
		 
	}
		
	  private void uploadmusicapi() {
		// TODO Auto-generated method stub
		GeneralAsynctask submitAsync = new GeneralAsynctask(
				this) {

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {

					JSONObject jobj=new JSONObject(result);
								
					String Ack = jobj.getString("status");
					if (Ack.equals("success"))  {
					JSONArray jsonarray=jobj.getJSONArray("message");
					UploadMusicArraylist=new ArrayList<Uploadmusiclistmodelclass>();
					for (int i = 0; i < jsonarray.length(); i++) {
					JSONObject jobj1=jsonarray.getJSONObject((i));
					JSONObject jobj2=jobj1.getJSONObject("user");

					Uploadmusiclistmodelclass item=new Uploadmusiclistmodelclass(jobj1.getString("songid"),
								jobj1.getString("song_name"),
							    jobj1.getString("song_category"),
								jobj1.getString("song_image"),
							    jobj1.getString("likecount"),
							    jobj1.getString("song_file"),
								jobj1.getString("userid"),
								jobj2.getString("id"),jobj2.getString("name"),jobj2.getString("pic"));
						        UploadMusicArraylist.add(item);
							
							}
						
						setlist();
					}
					
					else {
						
						showToastLong("No Songs in This Week");
					}
					
					
				} catch (Exception e) {
					e.printStackTrace();
					// showToastLong("Error: Unable to Login.");
				} finally {
					pdspinnerGeneral.dismiss();
				}
			}

		};

		try {
			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {

				Log.d("LogIn", "Action: login");
				
							 

				String url = getResources().getString(R.string.app_base_url)
						+ "song/"+psh.getUserId();
				String finalurl = url.replaceAll(" ", "%20");

				submitAsync.execute(finalurl);

			} else {
				showToastLong(getString(R.string.no_internet_message));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showToastLong(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}

	protected void setlist() {
		// TODO Auto-generated method stub
	
		
	Uploadmusiclistadapter dashboardAdapter = new Uploadmusiclistadapter(Uploadedmusiclist.this,UploadMusicArraylist);
	recyclerView.setAdapter(dashboardAdapter);
		 
	}	
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}



}

package com.goigi.android.hiphopstreet;



import android.app.Application;
import android.net.Uri;
import android.util.Log;

public class ApplicationSingleton  extends Application{

	  private static final String TAG = ApplicationSingleton.class.getSimpleName();

	   
	    private static ApplicationSingleton instance;
	    public static ApplicationSingleton getInstance() {
	        return instance;
	    }

	    @Override
	    public void onCreate() {
	        super.onCreate();

	        Log.d(TAG, "onCreate");

	        instance = this;

	        
	    }
	    
	    Uri selectedImage;
	    public void setImageUri(Uri selectedImage_){
	    	selectedImage = selectedImage_;
	    }
	    public Uri getImageUri(){
	    	return selectedImage;
	    }








}

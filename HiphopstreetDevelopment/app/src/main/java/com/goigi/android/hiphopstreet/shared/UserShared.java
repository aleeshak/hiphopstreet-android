package com.goigi.android.hiphopstreet.shared;







import com.goigi.android.hiphopstreet.R;

import android.content.Context;
import android.content.SharedPreferences;



public class UserShared {
	private final Context context;
	private final SharedPreferences prefs;

	public UserShared(Context context) {
		this.context = context;
		prefs = context.getSharedPreferences("MY_SHARED_PREF", context.MODE_PRIVATE);
	}


	public boolean getLoggedInStatus(){
		return prefs.getBoolean(context.getString(R.string.shared_loggedin_status), false);
	}
	
	public boolean getLoggedInStatusArtist(){
		return prefs.getBoolean(context.getString(R.string.shared_loggedin_status_artist), false);
	}

		
	public String getUserEmail(){
		return prefs.getString(context.getString(R.string.shared_user_email), context.getString(R.string.shared_no_data));
	}


	public String getUserName(){
		return prefs.getString(context.getString(R.string.shared_user_name), context.getString(R.string.shared_no_data));
	}
	
		

	public String getUserId(){
		return prefs.getString(context.getString(R.string.shared_user_id), context.getString(R.string.shared_no_data));
	}

	public String getArtistpic(){
		return prefs.getString(context.getString(R.string.shared_user_pic), context.getString(R.string.shared_no_data));
	}


}

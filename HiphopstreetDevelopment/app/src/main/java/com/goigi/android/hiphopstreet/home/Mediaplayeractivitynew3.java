package com.goigi.android.hiphopstreet.home;



import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.session.MediaSession;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.session.MediaSessionCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.Artistprofilecategory;
import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.audionotification.MediaPlayerService;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.Song;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.modelclass.Uploadmusicmodel;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;
import com.goigi.android.hiphopstreet.shared.UserShared;
import com.goigi.android.hiphopstreet.sqlite.DBHelper;
import com.squareup.picasso.Picasso;

@SuppressLint("ClickableViewAccessibility")
public class Mediaplayeractivitynew3 extends Activity implements OnClickListener, OnTouchListener, OnCompletionListener,OnBufferingUpdateListener ,SeekBar.OnSeekBarChangeListener{
	Uploadmusicmodel  brandModell;
	private static String ACTION_EXIT="1";
	ImageView add_playlist;
	private ImageView addtoplaylist;
	private ImageView banner,like_view;
	private ImageButton btnBackward;
	private ImageButton btnForward;
	private ImageButton btnNext,showplaylist;
	private ImageButton btnPrevious;
	ConnectionDetector connection;
	private int currentSongIndex = 1;

	ArrayList<TopRatedsong> dataSett;
	String fullar;
	private final Handler handler = new Handler();
	private boolean isRepeat = false;
	private boolean isShuffle = false;
	String LIKE_STATUS, IMg,song_url;
	private int mediaFileLengthInMilliseconds;
	private MediaPlayer mediaPlayer;
	// Handler to update UI timer, progress bar etc,.
	private final Handler mHandler = new Handler();
	Intent mIntent;
	private MediaSessionCompat mMediaSessionCompat;
	MediaSession mSession;
	TopRatedsong item;
   String position_str;

	private final Runnable mUpdateTimeTask = new Runnable() {
		@Override
		public void run() {

			long totalDuration = mediaPlayer.getDuration();
			long currentDuration = mediaPlayer.getCurrentPosition();

			// Displaying Total Duration time
			songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
			// Displaying time completed playing
			songCurrentDurationLabel.setText(""+utils.milliSecondsToTimer(currentDuration));

			// Updating progress bar
			int progress = (utils.getProgressPercentage(currentDuration, totalDuration));
			//Log.d("Progress", ""+progress);
			songProgressBar.setProgress(progress);

			// Running this thread after 100 milliseconds
			mHandler.postDelayed(this, 100);
		}
	};
	ImageButton playbt, pausebt;
	private MediaPlayerService player;
	ProgressDialog progressDialog;
	UserShared psh;
	private MultipartEntity reqEntity;
	private final int seekBackwardTime = 5000; // 5000 milliseconds;
	private final int seekForwardTime = 5000; // 5000 milliseconds
	private TextView songCurrentDurationLabel;
	SeekBar songProgressBar;
	public ArrayList<Song> songsLists = new ArrayList<Song>();
	private TextView songTotalDurationLabel,song_Name,artist_floow, numberoflike_img;
	private Utilities utils;
	ArrayList<Uploadmusicmodel> songArray;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.musicplayer);
		//changes by me

		brandModell = (Uploadmusicmodel) getIntent().getSerializableExtra("BrandDetails");
		//fullar=getIntent().getStringExtra("fullarray");
		songArray = (ArrayList<Uploadmusicmodel>) getIntent().getSerializableExtra("allsong");
         PhoneStateListener phoneStateListener = new PhoneStateListener() {
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				if (state == TelephonyManager.CALL_STATE_RINGING) {
					//INCOMING call
					//do all necessary action to pause the audio
					if(mediaPlayer!=null){//check mediaPlayer
						setPlayerButton(true, false, true);

						if(mediaPlayer.isPlaying()){

							mediaPlayer.pause();
						}
					}

				} else if(state == TelephonyManager.CALL_STATE_IDLE) {
					//Not IN CALL
					//do anything if the phone-state is idle
				} else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
					//A call is dialing, active or on hold
					//do all necessary action to pause the audio
					//do something here
					if(mediaPlayer!=null){//check mediaPlayer
						setPlayerButton(true, false, true);

						if(mediaPlayer.isPlaying()){

							mediaPlayer.pause();
						}
					}
				}
				super.onCallStateChanged(state, incomingNumber);
			}
		};//end PhoneStateListener

		TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if(mgr != null) {
			mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
		}
		//changes ends here

		mediaPlayer=new MediaPlayer();
		psh=new UserShared(this);
		connection=new ConnectionDetector(this);
		mIntent = getIntent();
		utils = new Utilities();

		position_str=mIntent.getStringExtra("position");



		playbt = (ImageButton) findViewById(R.id.btnPlay);
		btnForward = (ImageButton) findViewById(R.id.btnForward);
		songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
		songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
		songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
		banner = (ImageView) findViewById(R.id.banner_pic);
		like_view=(ImageView)findViewById(R.id.like);
		numberoflike_img=(TextView)findViewById(R.id.numberoflike);
		numberoflike_img.setText(mIntent.getStringExtra("like_count"));
		song_Name=(TextView)findViewById(R.id.songTitle);
		song_Name.setText(mIntent.getStringExtra("name"));
		add_playlist=(ImageView)findViewById(R.id.addto_playlist);
		artist_floow=(TextView)findViewById(R.id.floww_artist);
		artist_floow.setText("Artist :"+mIntent.getStringExtra("artist_name")+" >");
		//artist_floow.setText("Artist :"+brandModell.ar" >");
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		addtoplaylist=(ImageView)findViewById(R.id.btnRepeat);
		showplaylist=(ImageButton)findViewById(R.id.btnPlaylist);
		btnPrevious=(ImageButton)findViewById(R.id.btnPrevious);


		showplaylist.setVisibility(View.GONE);
		add_playlist.setVisibility(View.GONE);
		like_view.setVisibility(View.GONE);
		addtoplaylist.setVisibility(View.GONE);
		songProgressBar.setMax(99); // It means 100% .0-99
		songProgressBar.setOnTouchListener(this);
		playbt.setOnClickListener(this);
		like_view.setOnClickListener(this);
		artist_floow.setOnClickListener(this);
		btnForward.setOnClickListener(this);
		btnNext.setOnClickListener(this);
		btnPrevious.setOnClickListener(this);

		mediaPlayer.setOnBufferingUpdateListener(this);
		mediaPlayer.setOnCompletionListener(this);
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnBufferingUpdateListener(this);
		mediaPlayer.setOnCompletionListener(this);
		addtoplaylist.setOnClickListener(this);
		showplaylist.setOnClickListener(this);
		add_playlist.setOnClickListener(this);
		dataSett=new ArrayList<TopRatedsong>();

		try {
			for (int i = 0; i < songArray.size(); i++) {
				item=new TopRatedsong(songArray.get(i).songid, songArray.get(i).song_name,
						songArray.get(i).song_image, songArray.get(i).likecount,
						songArray.get(i).song_file,songArray.get(i).id,
						songArray.get(i).name,songArray.get(i).pic);


				dataSett.add(item);


			}

		} catch (Exception e) {
			e.printStackTrace();

		}


		if(dataSett.size()==0){

		}
		else{

		  //TopRatedsong item;


			playSong(Integer.parseInt(position_str));



		}

		//checklikesatus();


	}// end of oncreate




	    private void checklikesatus() {
		// TODO Auto-generated method stub
		GeneralAsynctask submitAsync = new GeneralAsynctask(
				this) {

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {

					JSONObject jobj=new JSONObject(result);

					String Ack = jobj.getString("status");

					if (Ack.equals("found"))  {


						like_view.setImageResource(R.drawable.liked);
						LIKE_STATUS="1";

						//setlist();
					}

					else {

						//showToastLong("No Songs in This Week");
					}


				} catch (Exception e) {
					e.printStackTrace();
					// showToastLong("Error: Unable to Login.");
				} finally {
					pdspinnerGeneral.dismiss();
				}
			}
		};

		try {
			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {

				Log.d("LogIn", "Action: login");

				String url = getResources().getString(R.string.app_base_url)
						+ "like/"+mIntent.getStringExtra("id")+"/"+psh.getUserId();
				String finalurl = url.replaceAll(" ", "%20");

				submitAsync.execute(finalurl);

			} else {
				showToastLong(getString(R.string.no_internet_message));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	private void likeasynchtask() {
		// TODO Auto-generated method stub
		try {

			reqEntity = null;
			reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			reqEntity.addPart("userid", new StringBody(psh.getUserId()));
			reqEntity.addPart("songid", new StringBody(brandModell.id));

		}
		catch (Exception e) {
			e.printStackTrace();
		}


		Boolean isInternetPresent = connection.isConnectingToInternet();

		if (isInternetPresent) {
			progressDialog = ProgressDialog.show(this,
					"",
					getString(R.string.progress_bar_loading_message),
					false);

			UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
			editProfileAsyncTask.execute((Void) null);
		} else {
			showToastLong(getString(R.string.no_internet_message));
		}

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		if ( mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
			finish();

		} else {
			finish();

		}

	}
	@Override
	public void onBufferingUpdate(MediaPlayer mediaPlayer, int percent) {
		// TODO Auto-generated method stub
		/** Method which updates the SeekBar secondary progress by current song loading from URL position*/
		songProgressBar.setSecondaryProgress(percent);
	}
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btnPlay:

			// check for already playing
			if(mediaPlayer.isPlaying()){
				if(mediaPlayer!=null){
					mediaPlayer.pause();
					// Changing button image to play button
					playbt.setImageResource(R.drawable.btn_play);
				}
			}else{
				// Resume song
				if(mediaPlayer!=null){
					mediaPlayer.start();
					// Changing button image to pause button
					playbt.setImageResource(R.drawable.btn_pause);
				}
			}

			primarySeekBarProgressUpdater();
			break;

		case R.id.like:
			likeasynchtask();
			break;

		case R.id.floww_artist:
			if ( mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
			}
			Intent o=new Intent(this,Artistprofilecategory.class);
			o.putExtra("artistid",brandModell.id);
			o.putExtra(" artistname", brandModell.name);
			o.putExtra("img_str", brandModell.pic);
			startActivity(o);

			break;


		case R.id.btnForward:
			// get current song position
			int currentPosition = mediaPlayer.getCurrentPosition();
			// check if seekForward time is lesser than song duration
			if(currentPosition + seekForwardTime <= mediaPlayer.getDuration()){
				// forward song
				mediaPlayer.seekTo(currentPosition + seekForwardTime);
			}else{
				// forward to end position
				mediaPlayer.seekTo(mediaPlayer.getDuration());
			}


			break;

		 case R.id.btnBackward:

			// get current song position
			int currentPosition2 = mediaPlayer.getCurrentPosition();
			// check if seekBackward time is greater than 0 sec
			if(currentPosition2 - seekBackwardTime >= 0){
				// forward song
				mediaPlayer.seekTo(currentPosition2 - seekBackwardTime);
			}else{
				// backward to starting position
				mediaPlayer.seekTo(0);
			}


			break;

		case R.id.btnNext:
			if(dataSett.size()==0){

				Toast.makeText(Mediaplayeractivitynew3.this, "No Song found", Toast.LENGTH_SHORT).show();

			}
		if(currentSongIndex < (dataSett.size() - 1)){
		playSong(currentSongIndex + 1);
		currentSongIndex = currentSongIndex + 1;

			}else{
				// play first song
				playSong(0);
				currentSongIndex = 0;
			}

			break;


		case R.id.btnRepeat:
			if(isRepeat){
				isRepeat = false;
				//Toast.makeText(this, "Repeat is OFF", Toast.LENGTH_SHORT).show();
				addtoplaylist.setImageResource(R.drawable.replay);
			}else{
				// make repeat to true
				isRepeat = true;
				//Toast.makeText(this, "Repeat is ON", Toast.LENGTH_SHORT).show();
				// make shuffle to false
				isShuffle = false;
				addtoplaylist.setImageResource(R.drawable.replay);
				//btnShuffle.setImageResource(R.drawable.btn_shuffle);
			}

			break;

		case R.id.addto_playlist:

			Toast.makeText(getApplicationContext(), "Song Added to Playlist", Toast.LENGTH_SHORT).show();

			DBHelper db=new DBHelper(this);

			db.insertMUSIC_TABLE(mIntent.getStringExtra("id"),mIntent.getStringExtra("name"),
					mIntent.getStringExtra("image"), psh.getUserId(),song_url,
					mIntent.getStringExtra("songfile"),
					mIntent.getStringExtra("likecount"),
					mIntent.getStringExtra("artistname"),
					mIntent.getStringExtra("artist_id"),mIntent.getStringExtra("artist_image"));


			break;



		case R.id.btnPlaylist:
			// Toast.makeText(getApplicationContext(), "Coming Soon", Toast.LENGTH_LONG).show();
			Intent i=new Intent(this,ListActivity.class);
			mediaPlayer.stop();
			startActivity(i);
			break;

			case R.id.btnPrevious:
				if(dataSett.size()==0){

					Toast.makeText(Mediaplayeractivitynew3.this, "No Song found", Toast.LENGTH_SHORT).show();

				}else if(currentSongIndex < (dataSett.size() - 1)){
					playSong(currentSongIndex - 1);
					currentSongIndex = currentSongIndex -1 ;
				}else{
					// play first song
					//playSong(0);
					playSong(currentSongIndex);
					currentSongIndex = dataSett.size() - 1;
				}

				break;

			default:
			break;
		}

	}

	@Override
	public void onCompletion(MediaPlayer mediaPlayer) {

		// check for repeat is ON or OFF
		if(isRepeat){
			// repeat is on play same song again
			playSong(currentSongIndex);
		} else if(isShuffle){
			// shuffle is on - play a random song
			Random rand = new Random();
			currentSongIndex = rand.nextInt((dataSett.size() - 1) - 0 + 1) + 0;
			playSong(currentSongIndex);
		} else{
			// no repeat or shuffle ON - play next song
			if(currentSongIndex < (dataSett.size() - 1)){
				playSong(currentSongIndex + 1);
				currentSongIndex = currentSongIndex + 1;
			}else{
				// play first song
				//playSong(0);
				currentSongIndex = 0;
			}
		}
	}





	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub

	}

	/**
	 * When user starts moving the progress handler
	 * */
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// remove message Handler from updating progress bar
		mHandler.removeCallbacks(mUpdateTimeTask);
	}

	/**
	 * When user stops moving the progress hanlder
	 * */
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		mHandler.removeCallbacks(mUpdateTimeTask);
		int totalDuration = mediaPlayer.getDuration();
		int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

		// forward or backward to certain seconds
		mediaPlayer.seekTo(currentPosition);

		// update timer progress again
		updateProgressBar();


	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.songProgressBar) {
			/** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
			if (mediaPlayer.isPlaying()) {
				SeekBar sb = (SeekBar) v;
				int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
				mediaPlayer.seekTo(playPositionInMillisecconds);
			}
		}


		return false;
	}

	/**
	 * Function to play a song
	 * @param songIndex - index of song
	 * */
	public void  playSong(int songIndex){
		// Play song


		try {
			//IMg = dataSett.get(currentSongIndex).song_image;
			IMg = dataSett.get(songIndex).song_image;
			if (!IMg.equals("")) {
				try {
					String apiLink = getResources().getString(R.string.image_base_url)+IMg;

					Log.d("apiLink", apiLink);
					String encodedurl = "";
					encodedurl = apiLink.substring(0, apiLink.lastIndexOf('/'))
							+ "/"
							+ Uri.encode(apiLink.substring(apiLink.lastIndexOf('/') + 1));
					Log.d("List adapter", "encodedurl:"
							+ encodedurl);
					if (!apiLink.equals("") && apiLink != null) {
				/*		Picasso.with(this).load(encodedurl) // load: This path may
						// be a remote URL,
						.placeholder(R.drawable.placeholdergb)
						// .resize(130, 130)
						.error(R.drawable.placeholdergb)
						.into(banner);*/

						Picasso.get().load(encodedurl).into(banner);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			//dataSett.get(songIndex).song_file
			//dataSett.get(songIndex)
			//dataSett.get(songIndex)

			mediaPlayer.reset();
			mediaPlayer.setDataSource(getResources().getString(R.string.song_base_url)+dataSett.get(songIndex).song_file);
			mediaPlayer.prepare();
			mediaPlayer.start();



			/*for(int i=0;i<songArray.size();i++)
			{
				String nameSing= songArray.get(i).song_file;
				mediaPlayer.reset();
				mediaPlayer.setDataSource(getResources().getString(R.string.song_base_url)+nameSing);
				mediaPlayer.prepare();
				mediaPlayer.start();
			}
*/



			// Displaying Song title
			String songTitle = dataSett.get(songIndex).song_name;

			song_Name.setText(songTitle);
			//song_url=mIntent.getStringExtra("myurl");
			// Changing Button Image to pause image
			playbt.setImageResource(R.drawable.btn_pause);
			artist_floow.setText("Artist :"+dataSett.get(songIndex).name+" >");
			// set Progress bar values
			songProgressBar.setProgress(0);
			songProgressBar.setMax(100);
			// Updating progress bar
			updateProgressBar();

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}///try ends


	    private void primarySeekBarProgressUpdater() {
		songProgressBar.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song length"
		if (mediaPlayer.isPlaying()) {
			Runnable notification = new Runnable() {
				@Override
				public void run() {
					primarySeekBarProgressUpdater();
				}
			};
			handler.postDelayed(notification, 1000);
		}
	}

	private void setPlayerButton(Boolean play, Boolean pause, Boolean stop){
		playbt.setEnabled(play);
		if(play==true)
			playbt.setEnabled(true);
		else
			playbt.setEnabled(false);

	}

	public void showNotification(View view){
		new MyNotification(this);
		finish();
	}
	private void showToastLong(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}


	public void updateProgressBar() {
	mHandler.postDelayed(mUpdateTimeTask, 100);
	}

	public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
		String[] message;
		String responseString = "";

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {
				String apiUrl = getResources().getString(R.string.app_base_url)+"like";
				Log.d("TestActivity", "Action: accountupdate");
				responseString = "";

				HttpParams params1 = new BasicHttpParams();
				params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
						HttpVersion.HTTP_1_1);
				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost postRequest = new HttpPost(apiUrl);
				//HttpPut postRequest = new HttpPut(apiUrl);
				postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");



                 /*to print in log*/

				ByteArrayOutputStream bytes = new ByteArrayOutputStream();

				reqEntity.writeTo(bytes);

				String content = bytes.toString();

				Log.e("MultiPartEntityRequest:",content);

                /*to print in log*/

				postRequest.setEntity(reqEntity);
				HttpResponse response = httpClient.execute(postRequest);

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(
								response.getEntity().getContent(), "UTF-8"));

				String sResponse;

				StringBuilder s = new StringBuilder();

				while ((sResponse = reader.readLine()) != null) {
					s = s.append(sResponse);
				}

				responseString = s.toString();
				Log.d("ResponseString =>", responseString);

			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			try {
				if (!responseString.equals("")) {

					JSONObject jsonObject = new JSONObject(responseString);
					String Ack = jsonObject.getString("status");
					String msg=jsonObject.getString("message");

					if (Ack.equals("success")   ) {
						progressDialog.dismiss();
						progressDialog.cancel();
						showToastLong(msg);

						like_view.setImageResource(R.drawable.liked);
         			}

					else {
						progressDialog.dismiss();
						progressDialog.cancel();
						showToastLong(msg);
					}
				}
				else {
					progressDialog.dismiss();
					progressDialog.cancel();
					showToastLong("Sorry! Problem cannot be recognized.");
				}
			} catch (Exception e) {
				progressDialog.dismiss();
				progressDialog.cancel();
				e.printStackTrace();
			}
		}

	}






}

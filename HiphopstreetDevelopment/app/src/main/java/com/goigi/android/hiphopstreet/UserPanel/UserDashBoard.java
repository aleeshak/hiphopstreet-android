package com.goigi.android.hiphopstreet.UserPanel;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.Changepassword;
import com.goigi.android.hiphopstreet.DashBoardActivity;
import com.goigi.android.hiphopstreet.MainActivityUser;
import com.goigi.android.hiphopstreet.ProfileActivity;
import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.SigninActivity;
import com.goigi.android.hiphopstreet.adapter.HomeAdapter;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.helper.BottomNavigationViewHelper;
import com.goigi.android.hiphopstreet.modelclass.Categorylist;
import com.goigi.android.hiphopstreet.modelclass.MusiccategoryModel;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;
import com.goigi.android.hiphopstreet.search.SearchActivity;
import com.roughike.bottombar.BottomBar;

import org.apache.http.entity.mime.MultipartEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class UserDashBoard extends MainActivityUser {
    private RecyclerView recyclerView;
    public ArrayList<TopRatedsong> topratedsong;
    public ArrayList<Categorylist> Favlist;
    ArrayList<MusiccategoryModel>musiccategorylist;

	BottomNavigationView bottomNavigationView;
    private MultipartEntity reqEntity;
    private ProgressDialog progressDialog;
    ConnectionDetector connection;  
    
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_main, container);
        connection=new ConnectionDetector(this);
        
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(UserDashBoard.this);
        recyclerView.setLayoutManager(layoutManager);
		BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        callasynchtask();
		bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
		BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

		BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
		for (int i = 0; i < menuView.getChildCount(); i++) {
			final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
			final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
			final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
			// If it is my special menu item change the size, otherwise take other size
			if (i == 4){
				// set your height here
				layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, displayMetrics);
				// set your width here
				layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, displayMetrics);
			}
			else {
				// set your height here
				layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, displayMetrics);
				// set your width here
				layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, displayMetrics);
			}
			iconView.setLayoutParams(layoutParams);
		}
        
       setcategory();
      // setimagecategory();

	/*	bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
			@Override
			public void onTabReSelected(@IdRes int tabId) {
				if (tabId == R.id.tab_home) {
					// The tab with id R.id.tab_favorites was reselected,
					// change your content accordingly.
					Intent i=new Intent(UserDashBoard.this,DashBoardActivity.class);
					startActivity(i);
				}

				else if (tabId == R.id.tab_search) {
					// The tab with id R.id.tab_favorites was reselected,
					// change your content accordingly.
					Intent i=new Intent(UserDashBoard.this,SearchActivity.class);
					startActivity(i);
				}

				else if (tabId == R.id.tab_player) {
					// The tab with id R.id.tab_favorites was reselected,
					// change your content accordingly.
					Intent i = new Intent(UserDashBoard.this, com.goigi.android.hiphopstreet.home.ListActivity.class);
					startActivity(i);
				}

				else if (tabId == R.id.tab_settings) {
					// The tab with id R.id.tab_favorites was reselected,
					// change your content accordingly.
					Intent i=new Intent(UserDashBoard.this,ProfileActivity.class);
					startActivity(i);
				}
			}
		});*/

		bottomNavigationView.setOnNavigationItemSelectedListener(
				new BottomNavigationView.OnNavigationItemSelectedListener() {
					@Override
					public boolean onNavigationItemSelected(@NonNull MenuItem item) {
						switch (item.getItemId()) {
							case R.id.action_home:
								Intent i=new Intent(UserDashBoard.this,DashBoardActivity.class);
								startActivity(i);
								break;
							case R.id.action_search:
								Intent s=new Intent(UserDashBoard.this,SearchActivity.class);
								startActivity(s);
								break;
							case R.id.action_player:
								Intent p = new Intent(UserDashBoard.this, com.goigi.android.hiphopstreet.home.ListActivity.class);
								startActivity(p);
								break;
							case R.id.action_settings:
								Intent settings=new Intent(UserDashBoard.this,ProfileActivity.class);
								startActivity(settings
								);
								break;
						}
						return false;
					}
				});
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.logout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_logout) {
			new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Logout")
					.setMessage("Do you want to logout?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
													int which) {
									SharedPreferences myPrefs = getSharedPreferences(
											"MY_SHARED_PREF",
											Context.MODE_PRIVATE);
									SharedPreferences.Editor editor = myPrefs.edit();
									editor.clear();
									editor.commit();
									Intent intent = new Intent(
											UserDashBoard.this,
											SigninActivity.class);
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									startActivity(intent);
								}
							}).setNegativeButton("No", null).show();
		} else if (id == R.id.action_pass) {
			Intent i=new Intent(UserDashBoard.this,Changepassword.class);
			startActivity(i);
		}

		return super.onOptionsItemSelected(item);
	}
	private void callasynchtask() {
		// TODO Auto-generated method stub
		GeneralAsynctask submitAsync = new GeneralAsynctask(
				this) {

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {

					JSONObject jobj=new JSONObject(result);
								
					String Ack = jobj.getString("status");
					
					if (Ack.equals("success"))  {
						
						JSONArray jarray=jobj.getJSONArray("featuredsonglist");
						
						topratedsong=new ArrayList<TopRatedsong>();
						
						for (int i = 0; i < jarray.length(); i++) {
							
							JSONObject jobj1=jarray.getJSONObject(i);
							JSONObject jobj2=jobj1.getJSONObject("user");
							
							TopRatedsong item=new TopRatedsong(jobj1.getString("songid"), jobj1.getString("song_name"),
									jobj1.getString("song_image"),jobj1.getString("likecount"),
									jobj1.getString("song_file"),jobj2.getString("id"),jobj2.getString("name"),jobj2.getString("pic"));
							
							topratedsong.add(item);
							
							
						}
						
						setlist();
					}
					
					else {
						
						showToastLong("No Songs in This Week");
					}
					
					
					
					

				} catch (Exception e) {
					e.printStackTrace();
					// showToastLong("Error: Unable to Login.");
				} finally {
					pdspinnerGeneral.dismiss();
				}
			}

			

		};

		try {
			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {

				Log.d("LogIn", "Action: login");
				
					String url = getResources().getString(R.string.app_base_url)
							+ "featuredsonglist?"+"format="+"json";
				String finalurl = url.replaceAll(" ", "%20");

				submitAsync.execute(finalurl);

			} else {
				showToastLong(getString(R.string.no_internet_message));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}	
	
	
	}	
	


	private void setlist() {
		// TODO Auto-generated method stub
		HomeAdapter dashboardAdapter = new HomeAdapter(UserDashBoard.this,topratedsong, Favlist);
        recyclerView.setAdapter(dashboardAdapter);
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle("Exit")
		// .setTitleColor(Color.parseColor(HALLOWEEN_ORANGE))
		//   .setDividerColor(HALLOWEEN_ORANGE)
		.setMessage("Are you sure you want to exit?")
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		}).setNegativeButton("No", null).show();
	}

	private void showToastLong(String string) {
		// TODO Auto-generated method stub
	Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG).show();	
	
	
	
	}
	     private void setcategory() {
		// TODO Auto-generated method stub
	    	 Favlist=new ArrayList<Categorylist>();
	 		Categorylist item= new Categorylist("Club",R.drawable.clubh);	
	 		Favlist.add(item);	
	 		Categorylist item1= new Categorylist("Conscious",R.drawable.conscioush);	
	 		Favlist.add(item1);
	 		Categorylist item2= new Categorylist("Freestyle",R.drawable.freestyleh);	
	 		Favlist.add(item2);
	 		Categorylist item3= new Categorylist("Gangsta",R.drawable.gang);	
	 		Favlist.add(item3);
	 		Categorylist item4= new Categorylist("R&B",R.drawable.rbhiphop);	
	 		Favlist.add(item4);
	 		Categorylist item5= new Categorylist("Trap",R.drawable.traph);	
	 		Favlist.add(item5);
	 		Categorylist item6= new Categorylist("Tribute",R.drawable.tributeh);	
	 		Favlist.add(item6);
	     
    
		//setcategorylist();
		//setlist();
		
		
		
	
	
	}




}
package com.goigi.android.hiphopstreet.audionotification;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;

import com.goigi.android.hiphopstreet.modelclass.MusiccategoryModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Created by Valdio Veliu on 16-07-30.
 */
public class StorageUtil {

	private final Context context;
	private SharedPreferences preferences;
	private final String STORAGE = " com.valdioveliu.valdio.audioplayer.STORAGE";

	public StorageUtil(Context context) {
		this.context = context;
	}

	public void clearCachedAudioPlaylist() {
		preferences = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.clear();
		editor.commit();
	}

	public ArrayList<MusiccategoryModel> loadAudio() {
		preferences = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
		Gson gson = new Gson();
		String json = preferences.getString("audioArrayList", null);
		Type type = new TypeToken<ArrayList<MusiccategoryModel>>() {
		}.getType();
		return gson.fromJson(json, type);
	}

	public int loadAudioIndex() {
		preferences = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
		return preferences.getInt("audioIndex", -1);//return -1 if no data found
	}

	public void storeAudio(ArrayList<MusiccategoryModel> arrayList) {
		preferences = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);

		SharedPreferences.Editor editor = preferences.edit();
		Gson gson = new Gson();
		String json = gson.toJson(arrayList);
		editor.putString("audioArrayList", json);
		editor.apply();
	}

	public void storeAudioIndex(int index) {
		preferences = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("audioIndex", index);
		editor.apply();
	}
}

package com.goigi.android.hiphopstreet;



import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.goigi.android.hiphopstreet.UserPanel.UserDashBoard;
import com.goigi.android.hiphopstreet.UserPanel.UserProfileActivity;

public class MainActivityUser extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
	protected FrameLayout container;
	ImageView edit;

	MenuItem log;
	TextView mail,name;
	private NavigationView navigationView;
	String pro_image;
	ImageView profile_img_user;

	ImageView search,optionwhite_img;

	@Override
	public void onBackPressed() {
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_user);


		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		container = (FrameLayout) findViewById(R.id.container);

		setSupportActionBar(toolbar);




	/*	DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
				toolbar, R.string.navigation_drawer_open,
				R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();*/

	//	navigationView = (NavigationView) findViewById(R.id.nav_view);

	//	navigationView.setNavigationItemSelectedListener(this);
//		View headerview = navigationView.getHeaderView(0);
	/*	edit = (ImageView) headerview.findViewById(R.id.edit_icon);
		name=(TextView)headerview.findViewById(R.id.name);
		name.setText("App");
		mail=(TextView)headerview.findViewById(R.id.mail);
		mail.setText("test");*/
//		navigationView.setVerticalScrollBarEnabled(false);
	//	search=(ImageView)findViewById(R.id.search_icon);
		
	/*	search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i=new Intent(MainActivityUser.this,SearchActivity.class);
				startActivity(i);



			}
		});*/

		
	}





	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		// Handle navigation view item clicks here.
		int id = item.getItemId();



		if (id == R.id.nav_home) {
			Intent i=new Intent(MainActivityUser.this,UserDashBoard.class);
			startActivity(i);
		}



		
		else if (id==R.id.chartsandplaylist) {
			Intent ii=new Intent(MainActivityUser.this,UserProfileActivity.class);
			startActivity(ii);


		}

	

		else if (id == R.id.nav_change_password) {

          Intent i=new Intent(MainActivityUser.this,Changepassword.class);
		  startActivity(i);	
			
			
			//Toast.makeText(this, "User Development", Toast.LENGTH_SHORT).show();
		}

		else if (id == R.id.nav_logout) {

			new AlertDialog.Builder(this)
			.setIcon(android.R.drawable.ic_dialog_alert)
			.setTitle("Logout")
			.setMessage("Do you want to logout?")
			.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					SharedPreferences myPrefs = getSharedPreferences(
							"MY_SHARED_PREF",
							Context.MODE_PRIVATE);
					Editor editor = myPrefs.edit();
					editor.clear();
					editor.commit();
					Intent intent = new Intent(
							MainActivityUser.this,
							SigninActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}
			}).setNegativeButton("No", null).show();


		}



		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}
}

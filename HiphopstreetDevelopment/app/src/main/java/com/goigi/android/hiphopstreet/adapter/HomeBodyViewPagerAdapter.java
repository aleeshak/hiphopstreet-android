package com.goigi.android.hiphopstreet.adapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.home.MediaplayerActivity_search;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.sqlite.DBHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import customfonts.MyTextView;

public class HomeBodyViewPagerAdapter extends RecyclerView.Adapter<HomeBodyViewPagerAdapter.ViewHolder> {
	// inner class to hold a reference to each item of RecyclerView
	public static class ViewHolder extends RecyclerView.ViewHolder {

		public ImageView imgflag;
		public MyTextView song_name,artistname;

		public ViewHolder(View itemLayoutView) {
			super(itemLayoutView);
			imgflag = (ImageView) itemLayoutView.findViewById(R.id.img2);
			song_name=(MyTextView)itemLayoutView.findViewById(R.id.songname2);

		}
	}
	public static final int progress_bar_type = 0;
	String apiLink;
	ConnectionDetector connection;
	private final Context context;
	private final ArrayList<TopRatedsong> dataSet1;
	private String fileName ;
	private String ID,Name,Image,songfile;
	private ImageView imaheview;
	private String Imgview;
	private LayoutInflater layoutInflater;
	private ArrayList<String> musicid;
	private  String MY_URL ;
	private DBHelper mydb ;
	String Path;
	private ProgressDialog pDialog;
	int[] propic;
	private long mLastClickTime = 0;


	//private ArrayList<Categorylist>dataset2;
	String song_id;

	public HomeBodyViewPagerAdapter(Context context,ArrayList<TopRatedsong> dataSet1) {
		this.context = context;
		this.dataSet1=dataSet1;
		//this.dataset2=dataset2;


	}

	// Return the size of your itemsData (invoked by the layout manager)
	@Override
	public int getItemCount() {
		return dataSet1.size();
	}

	@Override
	public void onBindViewHolder( final ViewHolder viewHolder, final int position) {
		//MY_URL= context.getResources().getString(R.string.app_base_url)+"songlist";

		//ImageView imageView = viewHolder.imgflag;
		Imgview=dataSet1.get(position).song_image;
		final MyTextView name=viewHolder.song_name;
		name.setText(dataSet1.get(position).song_name);
		name.setVisibility(View.VISIBLE);
		song_id=dataSet1.get(position).songid;
		songfile=dataSet1.get(position).song_file;
		Name=dataSet1.get(position).song_name;

		// mydb = new DBHelper(context);
		connection=new ConnectionDetector(context);
		//musicid=mydb.getAllMusic();

		if (!Imgview.equals("")) {
			try {
				//String apiLink = Link+Imgview;
				apiLink = context.getResources().getString(R.string.image_base_url)+Imgview;
				Log.d("link", apiLink);
				String encodedurl = "";
				encodedurl = apiLink.substring(0,apiLink.lastIndexOf('/'))+ "/"+ Uri.encode(apiLink.substring(
						apiLink.lastIndexOf('/') + 1));
				Log.d("UserProfile adapter", "encodedurl:"+encodedurl);
				if (!apiLink.equals("") && apiLink != null) {
		/*			Picasso.with(context)
					.load(encodedurl) // load: This path may be a remote URL,
					.placeholder(R.drawable.placeholdergb)
					.error(R.drawable.placeholdergb)
					//.resize(1280, 720)
					.into(viewHolder.imgflag); // Into: ImageView into which the final image has to be passed
					//.resize(130, 130);*/

					Picasso.get().load(encodedurl).into(viewHolder.imgflag);

					//Picasso.get().load(encodedurl).placeholder(R.drawable.default_pic).into(viewHolder.imgflag);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


		        viewHolder.itemView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


				if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
					return;
				}
				mLastClickTime = SystemClock.elapsedRealtime();

				
				Intent intent = new Intent(context, MediaplayerActivity_search.class);
				intent.putExtra("BrandDetails", dataSet1.get(position));
				intent.putExtra("id",dataSet1.get(position).songid);
				intent.putExtra("name", dataSet1.get(position).song_name);
				intent.putExtra("image",dataSet1.get(position).song_image);
				intent.putExtra("like_count", dataSet1.get(position).likecount);
				intent.putExtra("songfile",dataSet1.get(position).song_file);
				intent.putExtra("artist_id", dataSet1.get(position).userid);
				intent.putExtra("artist_name", dataSet1.get(position).name);
				intent.putExtra("artist_image", dataSet1.get(position).pic);
				intent.putExtra("position",	String.valueOf(position));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("allsongs", dataSet1);



				context.startActivity(intent);
				

			}
		});

	}

	// Create new views (invoked by the layout manager)
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		// create a new view
		View itemLayoutView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.buylist_item, null);

		// create ViewHolder

		ViewHolder viewHolder = new ViewHolder(itemLayoutView);
		return viewHolder;
	}

	

}

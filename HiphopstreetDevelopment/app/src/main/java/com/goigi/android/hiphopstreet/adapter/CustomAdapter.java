package com.goigi.android.hiphopstreet.adapter;

import com.goigi.android.hiphopstreet.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class CustomAdapter extends ArrayAdapter<Integer> {

	private Activity activity;
	
	public CustomAdapter(Activity activity, int resource, int textViewResourceId, Integer[] osimage) {
		super(activity, resource, textViewResourceId, osimage);
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder = null;
		 
		// inflate layout from xml
		LayoutInflater inflater = (LayoutInflater) activity
			.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		
		// If holder not exist then locate all view from UI file.
		if (convertView == null) {
		    // inflate UI from XML file
		    convertView = inflater.inflate(R.layout.cards_layout, parent, false);
		    // get all UI view
		    holder = new ViewHolder(convertView);
		    // set tag for holder
		    convertView.setTag(holder);
		} else {
		    // if holder created, get tag from view
		    holder = (ViewHolder) convertView.getTag();
		}
		
		holder.image.setImageResource(getItem(position));
		
		return convertView;
	}
	
	private class ViewHolder {
		private ImageView image;
		
		public ViewHolder(View v) {
			image = (ImageView)v.findViewById(R.id.img);
		}
	}
}

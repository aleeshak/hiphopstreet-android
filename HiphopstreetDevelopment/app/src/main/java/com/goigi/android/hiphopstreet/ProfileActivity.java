package com.goigi.android.hiphopstreet;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import com.goigi.android.hiphopstreet.UserPanel.UserDashBoard;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.home.UploadmusicActivity;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;
import com.goigi.android.hiphopstreet.shared.UserShared;
import com.squareup.picasso.Picasso;
import customfonts.MyEditText;
import customfonts.MyTextView;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity  extends MainActivity implements OnClickListener,OnCheckedChangeListener{
private  TextView editbtn,updatebtn;
private EditText name_edt,bio_edt;
private TextView email;
UserShared psh;
ConnectionDetector connection;
private String Name_str,email_str,biography_str;
MultipartEntity reqEntity;
private ProgressDialog progressDialog;
List<NameValuePair> nameValuePairs;
SharedPreferences prefs;
ImageView imgeview,camera_img;	
Uri fileUri;
final int GALLARY_CAPTURE = 3;
final int CAMERA_CAPTURE = 100;	
public static final int MEDIA_TYPE_IMAGE = 1;	
private static final String TAG = UploadmusicActivity.class.getSimpleName();
Boolean cameraboolean = false,cropboolean = false;
RadioButton radio_btn_female;
RadioButton radio_btn_male;
private RadioButton radioSexButton;
private RadioGroup radioSexGroup;
private String selectedType="";
String picturePath=null;
private Bitmap photo = null;
private String image_str;
String apiLink;

@Override
protected void onCreate(Bundle savedInstancestate){
super.onCreate(savedInstancestate);
//setContentView(R.layout.profile);
getLayoutInflater().inflate(R.layout.profile, container);

connection=new ConnectionDetector(this);
psh=new UserShared(this);
prefs=getSharedPreferences("MY_SHARED_PREF", Context.MODE_PRIVATE);

xmlinitilization();
xmlonclick();
setsharedData();
	
}

private void setsharedData() {
	
	email.setText(psh.getUserEmail());
	name_edt.setText(psh.getUserName());
	
}


private void xmlinitilization() {
	name_edt=(EditText)findViewById(R.id.name);
	email=(TextView)findViewById(R.id.emailaddress);
	editbtn=(TextView)findViewById(R.id.edit_btn);
	bio_edt=(EditText)findViewById(R.id.bio);
	
	
	updatebtn=(TextView)findViewById(R.id.update_btn);
	imgeview=(ImageView)findViewById(R.id.addimagee);
	camera_img=(ImageView)findViewById(R.id.addimg);
	radioSexGroup = (RadioGroup)findViewById(R.id.radioSex);
	radio_btn_male=(RadioButton)radioSexGroup.findViewById(R.id.radioMale);
	radio_btn_female=(RadioButton)radioSexGroup.findViewById(R.id.radioFemale);
     image_str=psh.getArtistpic();

     if (!image_str.equals("")) {
			try {
				//String apiLink = Link+Imgview;
				apiLink = getResources().getString(R.string.artist_image_url)+image_str;
				Log.d("link", apiLink);
				String encodedurl = "";
				encodedurl = apiLink.substring(0,apiLink.lastIndexOf('/'))+ "/"+ Uri.encode(apiLink.substring(
						apiLink.lastIndexOf('/') + 1));
				Log.d("UserProfile Social List adapter", "encodedurl:"+encodedurl);
				if (!apiLink.equals("") && apiLink != null) {
	/*				Picasso.with(this)
					.load(encodedurl) // load: This path may be a remote URL,
					.placeholder(R.drawable.placeholdergb)
					.error(R.drawable.placeholdergb)
					//.resize(1280, 720)
					.into(imgeview); // Into: ImageView into which the final image has to be passed
					//.resize(130, 130);*/

					Picasso.get().load(encodedurl).into(imgeview);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


}


private void xmlonclick() {
	
editbtn.setOnClickListener(this);
updatebtn.setOnClickListener(this);
camera_img.setOnClickListener(this);
radioSexGroup.setOnCheckedChangeListener(this);

}


@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
	case R.id.edit_btn:
				
		updatebtn.setVisibility(View.VISIBLE);
		editbtn.setVisibility(View.GONE);
		
		break;
		
		
	case R.id.update_btn:
		
		validation();
		
		break;
	
	
	case R.id.addimg:
			
			captureimage();
			
			break;

	default:
		break;
	}
}


private void captureimage() {
	// TODO Auto-generated method stub
	Context mContext = ProfileActivity.this; 
    final Dialog d = new Dialog(mContext);
    d.requestWindowFeature(Window.FEATURE_NO_TITLE);
    
    Button b,camera,gallary;
   
    d.setContentView(R.layout.dialog_cameragallary);	
    b=(Button)d.findViewById(R.id.button1);
    camera=(Button)d.findViewById(R.id.camera_bt_image);
    gallary=(Button)d.findViewById(R.id.gallary_bt_image);
    final Window window = d.getWindow();
//    window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
    window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    d.show();
    
    b.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			d.dismiss();
		}
	});

	camera.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {

			try {
				Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
						
				captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
				startActivityForResult(captureIntent, CAMERA_CAPTURE);
				cameraboolean = true;
				cropboolean = true;
				 
			}catch (ActivityNotFoundException anfe) {
			
				// display an error message
				String errorMessage = "oops! your device doesn't support capturing images!";
		
				Toast.makeText(getApplicationContext(), errorMessage,
				Toast.LENGTH_SHORT).show();
		
				}
				
				d.dismiss();
		}
	});
	gallary.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {

			Intent i = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

			startActivityForResult(i, GALLARY_CAPTURE);
			d.dismiss();
		}
	});
	

}

/** Create a file Uri for saving an image or video */
private static Uri getOutputMediaFileUri(int type){
      return Uri.fromFile(getOutputMediaFile(type));
}


/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type){
	    
				File mediaStorageDir = new File(
						Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
						com.goigi.android.hiphopstreet.Config.IMAGE_DIRECTORY_NAME);

				// Create the storage directory if it does not exist
				if (!mediaStorageDir.exists()) {
					if (!mediaStorageDir.mkdirs()) {
						Log.d(TAG, "Oops! Failed create "
								+ com.goigi.android.hiphopstreet.Config.IMAGE_DIRECTORY_NAME + " directory");


						return null;
					}

				}
				String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
						Locale.getDefault()).format(new Date());
				File mediaFile;
				if (type == MEDIA_TYPE_IMAGE) {
					mediaFile = new File(mediaStorageDir.getPath() + File.separator
							+ "IMG_" + timeStamp + ".png");
				} /*else if (type == MEDIA_TYPE_VIDEO) {
					mediaFile = new File(mediaStorageDir.getPath() + File.separator
							+ "VID_" + timeStamp + ".mp4");
				}*/ else {
					return null;
				}

				return mediaFile;
	}




private void validation() {
	
	Name_str = name_edt.getText().toString().trim();
	biography_str=bio_edt.getText().toString().trim();

	boolean cancel = false;
	String message="";
	View focusView = null;
	boolean tempCond = false;

	if(TextUtils.isEmpty(Name_str)){
		message = "Please enter your  Name.";
		focusView = name_edt;
		cancel = true;
		tempCond = false;
	}



	if(TextUtils.isEmpty(biography_str)){
		message = "Please brief about yourself.";
		focusView = bio_edt;
		cancel = true;
		tempCond = false;
	}

	

	if (cancel) {
		// focusView.requestFocus();
		if (!tempCond) {
			focusView.requestFocus();
		}
		showToastLong(message);
	} else {
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);

		 imm.hideSoftInputFromWindow(name_edt.getWindowToken(), 0);
		
		
		 try {

				reqEntity = null;
				reqEntity = new MultipartEntity(
						HttpMultipartMode.BROWSER_COMPATIBLE);

				
				reqEntity.addPart("uid", new StringBody(psh.getUserId()));
				reqEntity.addPart("name", new StringBody(psh.getUserName()));
				reqEntity.addPart("sex", new StringBody(selectedType));
				reqEntity.addPart("biography", new StringBody(biography_str));
					
				File sourceFile = new File(picturePath);
				reqEntity.addPart("pic", new FileBody(sourceFile));
				
		
			}
			catch (Exception e) {
				e.printStackTrace();
			}

	


		
		Boolean isInternetPresent = connection.isConnectingToInternet();

		if (isInternetPresent) {
			progressDialog = ProgressDialog.show(this,
					"",
					getString(R.string.progress_bar_loading_message),
					false);

			UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
			editProfileAsyncTask.execute((Void) null);
		} else {
			showToastLong(getString(R.string.no_internet_message));
		}
	}
	
	
	
}////end of validation

private void showToastLong(String message) {
	// TODO Auto-generated method stub
	
}

public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
		String[] message;
		String responseString = "";

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {
				String apiUrl = getResources().getString(R.string.app_base_url)+"userprofileupdate";
				Log.d("TestActivity", "Action: accountupdate");
				responseString = "";

				HttpParams params1 = new BasicHttpParams();
				params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
						HttpVersion.HTTP_1_1);
				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost postRequest = new HttpPost(apiUrl);
				//HttpPut postRequest = new HttpPut(apiUrl);
				postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");

				
				
			/*to print in log*/
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();

			reqEntity.writeTo(bytes);

			String content = bytes.toString();

			Log.e("MultiPartEntityRequest:",content);
			
			/*to print in log*/
				
				postRequest.setEntity(reqEntity);
				HttpResponse response = httpClient.execute(postRequest);

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(
								response.getEntity().getContent(), "UTF-8"));

				String sResponse;

				StringBuilder s = new StringBuilder();

				while ((sResponse = reader.readLine()) != null) {
					s = s.append(sResponse);
				}

				responseString = s.toString();
				Log.d("TestActivity ResponseString =>", responseString);

			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			try {
				if (!responseString.equals("")) {

					JSONObject jsonObject = new JSONObject(responseString);
					String Ack = jsonObject.getString("status");
					if (Ack.equals("successfully updated")   ) {
						showToastLong("profile updated");
						JSONObject jobj=jsonObject.getJSONObject("message");
						if (jobj.getString("user_cat").equals("artist")) {
						Editor editor=prefs.edit();
						editor.putString(getString(R.string.shared_user_id),jobj.getString("id"));
						editor.putString(getString(R.string.shared_user_name), jobj.getString("name"));
						editor.putString(getString(R.string.shared_user_pic), jobj.getString("pic"));
											
						editor.putString(getString(R.string.shared_user_type), jobj.getString("user_cat"));
						editor.putBoolean(getString(R.string.shared_loggedin_status_artist), true);
						editor.commit();
						Intent i=new Intent(ProfileActivity.this,SigninActivity.class);
						startActivity(i);
						
					}
					else {
						
						
						Editor editor=prefs.edit();
						editor.putString(getString(R.string.shared_user_id),jobj.getString("id"));
						editor.putString(getString(R.string.shared_user_name), jobj.getString("name"));
						editor.putString(getString(R.string.shared_user_pic), jobj.getString("pic"));
						editor.putString(getString(R.string.shared_user_type), jobj.getString("user_cat"));
						editor.putBoolean(getString(R.string.shared_loggedin_status), true);
						editor.commit();
						Intent i=new Intent(ProfileActivity.this,SigninActivity.class);
						startActivity(i);
					}
					
					
						
						

					}
					else {
						progressDialog.dismiss();
						progressDialog.cancel();
						showToastLong("");
					}
				}
				else {
					progressDialog.dismiss();
					progressDialog.cancel();
					showToastLong("Sorry! Problem cannot be recognized.");
				}
			} catch (Exception e) {
				progressDialog.dismiss();
				progressDialog.cancel();
				e.printStackTrace();
			}
		}




}	




@Override
public void onCheckedChanged(RadioGroup group, int checkedId) {
	// TODO Auto-generated method stub
	
	if(checkedId==R.id.radioMale){
		selectedType = radio_btn_male.getText().toString();

	}else if(checkedId==R.id.radioFemale){
		selectedType = radio_btn_female.getText().toString();


}


}

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
// TODO Auto-generated method stub
//super.onActivityResult(requestCode, resultCode, data);

 if (requestCode == CAMERA_CAPTURE) {
        if (resultCode == RESULT_OK) {
            
        	            	
        	picturePath=fileUri.getPath();
        	
        	BitmapFactory.Options options = new BitmapFactory.Options();

			
			options.inSampleSize = 8;

			photo = BitmapFactory.decodeFile(picturePath, options);

			imgeview.setImageBitmap(photo);
        
        }
        else if (resultCode == RESULT_CANCELED) {
            
        	// user cancelled Image capture
            Toast.makeText(getApplicationContext(),
                    "User cancelled image capture", Toast.LENGTH_SHORT)
                    .show();
        }
        else{
        	
        	// failed to capture image
            Toast.makeText(getApplicationContext(),
                    "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                    .show(); 
                    
        } 
        
 
 
 }else if (requestCode == GALLARY_CAPTURE) {
        if (resultCode == RESULT_OK) {
            
        	// video successfully recorded
            // launching upload activity
        	//launchUploadActivity(false);
        	Log.v("data", data.toString());
			
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };
		    Log.v("selectedImage", selectedImage.toString());
            Log.v("filePathColumn", filePathColumn.toString());
            
            Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			
			if(cursor!=null) {
				cursor.moveToFirst();
	
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
			}else{
				picturePath = selectedImage.toString();
			}
			
			ExifInterface exif = null;
			 try {
					exif = new ExifInterface(picturePath);
				 } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
			 Log.v("----exifOrientation------", exifOrientation);
			Log.d("filepath", picturePath);
			photo = BitmapFactory.decodeFile(picturePath); 
			
		    	cameraboolean = false;
		    //}
		    	imgeview.setImageBitmap(photo);
        
        } else if (resultCode == RESULT_CANCELED) {
            
        	// user cancelled recording
            Toast.makeText(getApplicationContext(),
                    "Cancel By You", Toast.LENGTH_SHORT)
                    .show();
        
        } else {
            // failed to record video
            Toast.makeText(getApplicationContext(),
                    "Sorry! Failed to Get Picture", Toast.LENGTH_SHORT)
                    .show();
        }
      
 }


}



}


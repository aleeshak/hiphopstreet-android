package com.goigi.android.hiphopstreet;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.shared.UserShared;
import customfonts.MyEditText;
import customfonts.MyTextView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Changepassword extends Activity implements OnClickListener {
	private EditText old_pass,new_pass,confirm_pass;
	private TextView submit_password;
	UserShared psh;
	ConnectionDetector connection;
	String Old_Passwrod,New_Password,Confirm_Password;
	MultipartEntity reqEntity;
	private ProgressDialog progressDialog;

	
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
    setContentView(R.layout.change_password);
    connection=new ConnectionDetector(this);
	psh=new UserShared(this);
	
	intialization();
	xmlonclick();



}

private void intialization() {
	
	old_pass=(EditText)findViewById(R.id.old_pass);
	new_pass=(EditText)findViewById(R.id.new_password);
	confirm_pass=(EditText)findViewById(R.id.con_password);
	submit_password=(TextView)findViewById(R.id.submit_pas);
	
}

private void xmlonclick() {

	submit_password.setOnClickListener(this);
}



@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
	case R.id.submit_pas:
		
		validation();
		
		break;

	default:
		break;
	}
}



private void validation() {
	
	Old_Passwrod = old_pass.getText().toString().trim();
	New_Password = new_pass.getText().toString().trim();
	Confirm_Password = confirm_pass.getText().toString().trim();
	
	boolean cancel = false;
	String message="";
	View focusView = null;
	boolean tempCond = false;

	
	if(TextUtils.isEmpty(Old_Passwrod)){
		message = "Please enter your Old Password";
		focusView = old_pass;
		cancel = true;
		tempCond = false;
	}



	if(TextUtils.isEmpty(New_Password)){
		message = "Please enter your New Password";
		focusView = new_pass;
		cancel = true;
		tempCond = false;
	}
	
	
	if (!Confirm_Password.equals(New_Password)) {
		message = "Password Not Match !";
		focusView = confirm_pass;
		cancel = true;
		tempCond = false;
	}
	
	
	if (cancel) {
		// focusView.requestFocus();
		if (!tempCond) {
			focusView.requestFocus();
		}
		showToastLong(message);
	} else {
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);

		try {

			reqEntity = null;
			reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			reqEntity.addPart("old_pass", new StringBody(Old_Passwrod));
			reqEntity.addPart("newpassword", new StringBody(New_Password));
			reqEntity.addPart("uid", new StringBody(psh.getUserId()));
			
					}

		catch (Exception e) {
			e.printStackTrace();
		}


		Boolean isInternetPresent = connection.isConnectingToInternet();

		if (isInternetPresent) {
			progressDialog = ProgressDialog.show(this,
					"",
					getString(R.string.progress_bar_loading_message),
					false);

			UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
			editProfileAsyncTask.execute((Void) null);
		} else {
			showToastLong(getString(R.string.no_internet_message));
		}
	}
	
	
}///end of validation


public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
	String[] message;
	String responseString = "";

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO: attempt authentication against a network service.

		try {
			String apiUrl = getResources().getString(R.string.app_base_url)+"userpass?";
			Log.d("TestActivity", "Action: accountupdate");
			responseString = "";

			
			HttpParams params1 = new BasicHttpParams();
			params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
					HttpVersion.HTTP_1_1);
		
			DefaultHttpClient httpClient = new DefaultHttpClient();
			
			HttpPost postRequest = new HttpPost(apiUrl);
						
			postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");
		
		
			
			/*to print in log*/
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();

			reqEntity.writeTo(bytes);

			String content = bytes.toString();

			Log.e("MultiPartEntityRequest:",content);
			
			/*to print in log*/
			postRequest.setEntity(reqEntity);
	    	HttpResponse response = httpClient.execute(postRequest);

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(
							response.getEntity().getContent(), "UTF-8"));

			String sResponse;

			StringBuilder s = new StringBuilder();

			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			

			responseString = s.toString();
			Log.d("TestActivity ResponseString =>", responseString);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	protected void onPostExecute(final Boolean success) {

		try {
			if (!responseString.equals("")) {

				JSONObject jsonObject = new JSONObject(responseString);
				String Ack = jsonObject.getString("status");
				String msg = jsonObject.getString("message");
				
				if (Ack.equals("successfully password updated") ) {
				
				  showToastLong(msg);
					
					progressDialog.dismiss();
					progressDialog.cancel();
					//showToastLong("Data inserted successfully.");
					
					Intent i=new Intent(Changepassword.this,SigninActivity.class);
					startActivity(i);

				}
				else  {
					
					progressDialog.dismiss();
					progressDialog.cancel();
					showToastLong(msg);
				}
			}
			else {
				progressDialog.dismiss();
				progressDialog.cancel();
				showToastLong("Sorry! Problem cannot be recognized.");
			}
		} catch (Exception e) {
			progressDialog.dismiss();
			progressDialog.cancel();
			e.printStackTrace();
		}
	}


}


private void showToastLong(String message) {
	// TODO Auto-generated method stub
	Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
}



}

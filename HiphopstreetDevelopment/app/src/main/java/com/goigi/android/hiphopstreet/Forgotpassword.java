package com.goigi.android.hiphopstreet;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.goigi.android.hiphopstreet.SigninActivity.UploadProfileAsyncTask;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.home.HomepageActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import customfonts.MyEditText;
import customfonts.MyTextView;

public class Forgotpassword extends Activity implements OnClickListener{

	private EditText mailid;
	private TextView send;
	private ConnectionDetector connection;
	private String Mail;
	private ImageView signinback;
	private MultipartEntity reqEntity;
	private ProgressDialog progressDialog;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.forgot_password);
		xmlinit();
		xmlonclick();

	}//end of oncreate

	private void xmlinit() {
		connection=new ConnectionDetector(this);
		mailid=(EditText)findViewById(R.id.username_edt);
		send=(TextView)findViewById(R.id.send);
		

	}

	private void xmlonclick() {
		send.setOnClickListener(this);
		

	}
	
	private void showToastLong(String string) {
		Toast.makeText(this, string, Toast.LENGTH_SHORT)
		.show();

	}
	
	
	@Override
	public void onBackPressed() {
	Intent i=new Intent(Forgotpassword.this,SigninActivity.class);
	startActivity(i);
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.send:
			forgotpasswordvalidation();
			
			
			break;
			
		

		default:
			break;
		}

	}//end of onclick

	private void forgotpasswordvalidation() {

		// TODO Auto-generated method stub
		Mail = mailid.getText().toString().trim();
		


		boolean cancel = false;
		String message="";
		View focusView = null;
		boolean tempCond = false;




		if(TextUtils.isEmpty(Mail)){
			message = "Please enter your Email.";
			focusView = mailid;
			cancel = true;
			tempCond = false;
		}




		if (cancel) {
			// focusView.requestFocus();
			if (!tempCond) {
				focusView.requestFocus();
			}
			showToastLong(message);
		} else {
			InputMethodManager imm = (InputMethodManager) this
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			try {

				reqEntity = null;
				reqEntity = new MultipartEntity(
						HttpMultipartMode.BROWSER_COMPATIBLE);

				reqEntity.addPart("email_id", new StringBody(Mail));
				

			}

			catch (Exception e) {
				e.printStackTrace();
			}
		
			Boolean isInternetPresent = connection.isConnectingToInternet();

			if (isInternetPresent) {
				progressDialog = ProgressDialog.show(this,
						"",
						getString(R.string.progress_bar_loading_message),
						false);

				UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
				editProfileAsyncTask.execute((Void) null);
			} else {
				showToastLong(getString(R.string.no_internet_message));
			}
		}	






}

public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
	String[] message;
	String responseString = "";

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO: attempt authentication against a network service.

		try {
			String apiUrl = getResources().getString(R.string.app_base_url)+"forgotpassword";
			Log.d("TestActivity", "Action: accountupdate");
			responseString = "";

			HttpParams params1 = new BasicHttpParams();
			params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
					HttpVersion.HTTP_1_1);
			HttpClient httpClient = new DefaultHttpClient(params1);
			HttpPost postRequest = new HttpPost(apiUrl);
			
			postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");

			postRequest.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(postRequest);

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(
							response.getEntity().getContent(), "UTF-8"));

			String sResponse;

			StringBuilder s = new StringBuilder();

			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}

			responseString = s.toString();
			Log.d("TestActivity ResponseString =>", responseString);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	protected void onPostExecute(final Boolean success) {

		try {
			if (!responseString.equals("")) {

				JSONObject jsonObject = new JSONObject(responseString);
				
				String Ack = jsonObject.getString("status");
				String msg = jsonObject.getString("message");
				
			
				
				if (Ack .equals("Send") ) {
					showToastLong(msg);
					
					Intent i=new Intent(Forgotpassword.this,SigninActivity.class);
					startActivity(i);

				}
				else {
					progressDialog.dismiss();
					progressDialog.cancel();
					showToastLong("Please check your email address");
				}
			}
			else {
				progressDialog.dismiss();
				progressDialog.cancel();
				showToastLong("Sorry! Problem cannot be recognized.");
			}
			
			
		} catch (Exception e) {
			progressDialog.dismiss();
			progressDialog.cancel();
			e.printStackTrace();
		}
	}




}



}

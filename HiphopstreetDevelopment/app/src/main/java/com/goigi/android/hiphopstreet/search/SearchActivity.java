package com.goigi.android.hiphopstreet.search;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.MainActivity;
import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.adapter.Searchmusiclistadapter;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.Uploadmusiclistmodelclass;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import customfonts.MyEditText;

public class SearchActivity extends MainActivity implements OnClickListener{
ImageView search_icon;
private ImageView close,search;   
MyEditText serach_edt;	
RecyclerView recyclerView;	
private ArrayList<Uploadmusiclistmodelclass> UploadMusicArraylist;
ConnectionDetector connection;
public static String Search_str;

long delay = 1000; // 1 seconds after user stops typing
long last_text_edit = 0;
Handler handler = new Handler();
private Button btn_prev;
private Button btn_next;
private int pageCount ;
private int increment = 0;

public int TOTAL_LIST_ITEMS = 0;
public int NUM_ITEMS_PAGE   = 10;

@Override
protected void onCreate(Bundle savedInstancestate){
	super.onCreate(savedInstancestate);
	 //getLayoutInflater().inflate(R.layout.search_activity, container);
	setContentView(R.layout.search_activity);
	connection=new ConnectionDetector(this); 
	
	  initToolbar();
      xmlinitilization();
      xmlonclick();
	
}



private void xmlonclick() {
	// TODO Auto-generated method stub
	
close.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	finish();
	
	
	}
});



btn_next.setOnClickListener(new OnClickListener() {
	
	public void onClick(View v) {
		
		increment++;
		loadList(increment,UploadMusicArraylist);
		CheckEnable();
	}
});

btn_prev.setOnClickListener(new OnClickListener() {
	
	public void onClick(View v) {
		
		increment--;
		loadList(increment,UploadMusicArraylist);
		CheckEnable();
	}
});



serach_edt.addTextChangedListener(new TextWatcher() {
    @Override
    public void beforeTextChanged (CharSequence s,int start, int count,
    int after){
    }
    @Override
    public void onTextChanged ( final CharSequence s, int start, int before,
    int count){
        //You need to remove this to run only once
        handler.removeCallbacks(input_finish_checker);

    }
    @Override
    public void afterTextChanged ( final Editable s){
        //avoid triggering event when text is empty
    	Search_str=s.toString();
        if (s.length() > 0) {
            last_text_edit = System.currentTimeMillis();
            handler.postDelayed(input_finish_checker, delay);
        } else {

        }
    }
}

);




}


   private void CheckEnable()
{
	if(increment+1 == pageCount)
	{
		btn_next.setEnabled(false);
	}
	else if(increment == 0)
	{
		btn_prev.setEnabled(false);
	}
	else
	{
		btn_prev.setEnabled(true);
		btn_next.setEnabled(true);
	}
}
private Runnable input_finish_checker = new Runnable() {
    public void run() {
        if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
            // TODO: do what you need here
            // ............
            // ............
           callasynchtask(Search_str);
        }
    }
};



    private void xmlinitilization() {
	// TODO Auto-generated method stub
	close=(ImageView)findViewById(R.id.close_);
	search=(ImageView)findViewById(R.id.search_music_now);
    serach_edt=(MyEditText)findViewById(R.id.search_edt);	
    recyclerView=(RecyclerView)findViewById(R.id.recycler_view_search);
    btn_prev = (Button)findViewById(R.id.prev);
    btn_next = (Button)findViewById(R.id.next);
    
    btn_prev.setEnabled(false);
    LinearLayoutManager layoutManager
    = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

      recyclerView.setLayoutManager(layoutManager);
  
     recyclerView.setLayoutManager(layoutManager);

}


private void initToolbar() {
	// TODO Auto-generated method stub
}


@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	
switch (v.getId()) {


default:
	break;
}

}



private void callasynchtask(String ss) {
	// TODO Auto-generated method stub
	GeneralAsynctask submitAsync = new GeneralAsynctask(SearchActivity.this) {

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				JSONObject jobj=new JSONObject(result);
							
				String Ack = jobj.getString("status");
				
				if (Ack.equals("success"))  {
					
					JSONArray jsonarray=jobj.getJSONArray("songanduserlist");
					UploadMusicArraylist=new ArrayList<Uploadmusiclistmodelclass>();
										
					for (int i = 0; i < jsonarray.length(); i++) {
						
						JSONObject jobj1=jsonarray.getJSONObject((i));
						JSONObject jobj2=jobj1.getJSONObject("user");
						Uploadmusiclistmodelclass item=new Uploadmusiclistmodelclass(jobj1.getString("songid"),
								jobj1.getString("song_name"),jobj1.getString("song_category"),
								jobj1.getString("song_image"),jobj1.getString("likecount"),       jobj1.getString("song_file"),
								jobj1.getString("userid"),
								jobj2.getString("id"),jobj2.getString("name"),jobj2.getString("pic"));

					          UploadMusicArraylist.add(item);
												
						}
					
					setlist();
				}
				
				else {
					
					showToastLong("No Songs in This Week");
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
				// showToastLong("Error: Unable to Login.");
			} finally {
				pdspinnerGeneral.dismiss();
			}
		}

	};

	try {
		Boolean isInternetPresent = connection.isConnectingToInternet();
		HttpURLConnection connection = null;
		String url1 = getResources().getString(R.string.app_base_url)
				+ "songsearch/"+ss;
		URL object = new URL(url1);
		connection = (HttpURLConnection) object.openConnection();
		connection.setRequestProperty("x-api-key","25d55ad283aa400af464c76d713c07ad");
		if (isInternetPresent) {

			Log.d("LogIn", "Action: login");



			String url = getResources().getString(R.string.app_base_url)
					+ "songsearch/"+ss;
			String finalurl = url.replaceAll(" ", "%20");

			submitAsync.execute(finalurl);

		} else {
			showToastLong(getString(R.string.no_internet_message));
		}

	} catch (Exception e) {
		e.printStackTrace();
	}	



}



protected void showToastLong(String string) {
	// TODO Auto-generated method stub
Toast.makeText(getApplicationContext(),string , Toast.LENGTH_LONG).show();	
}



protected void setlist() {
	// TODO Auto-generated method stub

		// TODO Auto-generated method stub
	TOTAL_LIST_ITEMS=UploadMusicArraylist.size();
	
	 int val = TOTAL_LIST_ITEMS%NUM_ITEMS_PAGE;
		val = val==0?0:1;
		pageCount = TOTAL_LIST_ITEMS/NUM_ITEMS_PAGE+val;
		
		    
		    loadList(0,UploadMusicArraylist);
	 
	}	


private void loadList(int number,ArrayList<Uploadmusiclistmodelclass> data)
{
ArrayList<Uploadmusiclistmodelclass> sort=new ArrayList<Uploadmusiclistmodelclass>();



	int start = number * NUM_ITEMS_PAGE;
	for(int i=start;i<(start)+NUM_ITEMS_PAGE;i++)
	{
		if(i<data.size())
		{
			Uploadmusiclistmodelclass mo=new Uploadmusiclistmodelclass(data.get(i).songid, data.get(i).song_name, data.get(i).song_category,
					 data.get(i).song_image,data.get(i) .likecount,data.get(i).song_file,data.get(i).userid ,data.get(i).id,data.get(i).name,data.get(i).pic);
		   	         sort.add(mo);
		}
		else
		{
			break;
		}
	}
	
	Searchmusiclistadapter dashboardAdapter = new Searchmusiclistadapter(SearchActivity.this,sort);
	recyclerView.setAdapter(dashboardAdapter);
	
}
}

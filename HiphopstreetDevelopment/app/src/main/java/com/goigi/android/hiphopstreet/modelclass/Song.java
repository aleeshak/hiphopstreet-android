package com.goigi.android.hiphopstreet.modelclass;

import java.io.Serializable;

public class Song implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	public String SONG_FILE;
	public String SONG_ID;
	public String SONG_IMAGE;
	public String SONG_NAME;
	public String SONG_URL;
	public String USER_ID;
	public String LIKE_COUNT;
	public String ARTIST_ID;
	public String ARTIST_NAME;
	public String ARTIST_IMAGE;


	public Song(String SONG_ID,String SONG_NAME,
			String SONG_IMAGE,String USER_ID,
			String SONG_URL, String SONG_FILE,String LIKE_COUNT,
				String ARTIST_ID,String ARTIST_NAME,String ARTIST_IMAGE

	){
		this.SONG_ID=SONG_ID;
		this.SONG_NAME=SONG_NAME;
		this.SONG_IMAGE=SONG_IMAGE;
		this.USER_ID=USER_ID;
		this.SONG_URL=SONG_URL;
		this.SONG_FILE=SONG_FILE;
		this.LIKE_COUNT=LIKE_COUNT;
		this.ARTIST_ID=ARTIST_ID;
		this.ARTIST_NAME=ARTIST_NAME;

		this.ARTIST_IMAGE=ARTIST_IMAGE;




	}
















}

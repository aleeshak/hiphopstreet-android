package com.goigi.android.hiphopstreet.audionotification;

/**
 * Created by Valdio Veliu on 16-07-29.
 */
public enum PlaybackStatus {
    PLAYING,
    PAUSED
}

package com.goigi.android.hiphopstreet;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
protected FrameLayout container;
	private NavigationView navigationView;

ImageView edit;
ImageView search,optionwhite_img;
TextView mail,name;
MenuItem log;
ImageView profile_img_user;

String pro_image;

@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_main);


Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
container = (FrameLayout) findViewById(R.id.container);

setSupportActionBar(toolbar);




/*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
		toolbar, R.string.navigation_drawer_open,
		R.string.navigation_drawer_close);
drawer.setDrawerListener(toggle);
toggle.syncState();*/

//navigationView = (NavigationView) findViewById(R.id.nav_view);

//navigationView.setNavigationItemSelectedListener(this);
/*View headerview = navigationView.getHeaderView(0);
edit = (ImageView) headerview.findViewById(R.id.edit_icon);
name=(TextView)headerview.findViewById(R.id.name);
name.setText("App");
mail=(TextView)headerview.findViewById(R.id.mail);
mail.setText("test");*/
//navigationView.setVerticalScrollBarEnabled(false);
/*search=(ImageView)findViewById(R.id.search_icon);
optionwhite_img=(ImageView)findViewById(R.id.optionwhite);*/

/*
search.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	Intent i=new Intent(MainActivity.this,SearchActivity.class);
	startActivity(i);
	
	
	
	}
});
*/

/*optionwhite_img.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		popupoption();
	
	
	
	}
});
*/

}



/*@SuppressLint("NewApi") protected void popupoption() {
	// TODO Auto-generated method stub
	 PopupMenu menu = new PopupMenu (this,optionwhite_img);
		//menu.getMenuInflater().inflate(R.menu.popup_menu);
		 
		 
		 
		 menu.setOnMenuItemClickListener (new PopupMenu.OnMenuItemClickListener ()
		    {
		        @SuppressLint("NewApi") @Override
		        public boolean onMenuItemClick (MenuItem item)
		        {
		            int id = item.getItemId();
		            switch (id)
		            {
		                case R.id.action_settings: 
		                	
		                Toast.makeText(getApplicationContext(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();  
		                    return true;  
		                case R.id.popularsong: 
		                	
		                	Toast.makeText(getApplicationContext(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();  
		                    return true;  
		                
                     case R.id.playedsong: 
		                	
		                	Toast.makeText(getApplicationContext(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();  
		                    return true;  
		                  
		            }
		            return true;
		        }
		    });
		    menu.inflate (R.menu.main);
		    menu.show();	
	
	
	
	
	
	}	*/



@Override
public void onBackPressed() {
/*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
if (drawer.isDrawerOpen(GravityCompat.START)) {
	drawer.closeDrawer(GravityCompat.START);
} else {
	super.onBackPressed();
}*/
super.onBackPressed();
}


@Override
public boolean onNavigationItemSelected(MenuItem item) {
// Handle navigation view item clicks here.
int id = item.getItemId();



if (id == R.id.nav_home) {
Intent i=new Intent(MainActivity.this,DashBoardActivity.class);
startActivity(i);	
}



/*else if (id == R.id.mymusic) {
	
Intent i=new Intent(MainActivity.this,Uploadedmusiclist.class);
startActivity(i);





} */

/*
else if (id==R.id.uploadmusic) {
	Intent ii=new Intent(MainActivity.this,UploadmusicActivity.class);
	startActivity(ii);	
		
	
	
}
*/

/*else if (id==R.id.nav_menu) {
	
	
	
}*/

else if (id==R.id.chartsandplaylist) {
	Intent ii=new Intent(MainActivity.this,ProfileActivity.class);
	startActivity(ii);	
	
	
}

/*else if (id == R.id.nav_gallery) {

	

} */
/*else if (id == R.id.nav_edit_account) {
Intent ii=new Intent(MainActivity.this,SigninActivity.class);
startActivity(ii);
	
	Toast.makeText(this, "User Development", Toast.LENGTH_SHORT).show();

} */

       else if (id == R.id.nav_change_password) {

	Intent ii=new Intent(MainActivity.this,Changepassword.class);
	startActivity(ii);	
	

} 

else if (id == R.id.nav_logout) {
	
	new AlertDialog.Builder(this)
	.setIcon(android.R.drawable.ic_dialog_alert)
	.setTitle("Logout")
	.setMessage("Do you want to logout?")
	.setPositiveButton("Yes",
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					SharedPreferences myPrefs = getSharedPreferences(
							"MY_SHARED_PREF",
							Context.MODE_PRIVATE);
					Editor editor = myPrefs.edit();
					editor.clear();
					editor.commit();
					Intent intent = new Intent(
							MainActivity.this,
							SigninActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}
			}).setNegativeButton("No", null).show();


} 



DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
drawer.closeDrawer(GravityCompat.START);
return true;
}
}

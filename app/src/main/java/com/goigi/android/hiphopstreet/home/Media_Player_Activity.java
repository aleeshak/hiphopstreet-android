package com.goigi.android.hiphopstreet.home;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.goigi.android.hiphopstreet.ArtistProfileActivity;
import com.goigi.android.hiphopstreet.DashBoardActivity;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.adapter.MusiccategoryAdapter;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.MusiccategoryModel;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;
import com.goigi.android.hiphopstreet.shared.UserShared;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class Media_Player_Activity extends Activity implements OnClickListener,
		OnPreparedListener, OnErrorListener, OnCompletionListener,
		SeekBar.OnSeekBarChangeListener {

	ImageButton playbt, pausebt;
	SeekBar songProgressBar;
	private int currentSongIndex = 0;
	private TextView songCurrentDurationLabel;
	private TextView songTotalDurationLabel,song_Name,artist_floow;
	private ImageView banner,like_view;
	ProgressDialog progressDialog;
	private MediaPlayer mediaPlayer;
	private int mediaFileLengthInMilliseconds;
	private final Handler handler = new Handler();
	Intent mIntent;
	ConnectionDetector connection;
	private MultipartEntity reqEntity;
	UserShared psh;
	String LIKE_STATUS, IMg,song_url;
	private ImageButton btnForward;
	private ImageView addtoplaylist;
	private ImageButton btnBackward;
	private int seekForwardTime = 5000; // 5000 milliseconds
	private int seekBackwardTime = 5000; // 5000 milliseconds
	private ImageButton btnNext,showplaylist;
	private ImageButton btnPrevious;
	private ArrayList<TopRatedsong> dataSet1;
	private MediaSessionCompat mMediaSessionCompat;
	Intent mintent;
	String category_name_str;
	public ArrayList<MusiccategoryModel> musiccategorylist;
	MusiccategoryAdapter adapter;
	RecyclerView recyclerView;
	public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
		setContentView(R.layout.musicplayer);
		connection=new ConnectionDetector(this);
		playbt = (ImageButton) findViewById(R.id.btnPlay);
		btnForward = (ImageButton) findViewById(R.id.btnForward);
		songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
		songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
		songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
		banner = (ImageView) findViewById(R.id.banner_pic);
		mintent=getIntent();
		category_name_str=mintent.getStringExtra("categoryname");
	
		callasynchtask();
		
		
		
		
		/*playbt.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			try {
	            mediaPlayer.setDataSource(musiccategorylist.get().get("songPath")); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
	            mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
	        } catch (Exception e) {
	            e.printStackTrace();
	        }

	        mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL

	        if (!mediaPlayer.isPlaying()) {
	            mediaPlayer.start();
	            playbt.setImageResource(R.drawable.btn_pause);
	        } else {
	            mediaPlayer.pause();
	            playbt.setImageResource(R.drawable.btn_play);
	        }

	        primarySeekBarProgressUpdater();
	    		
		
		
		}
			
		
	});
	*/
	
	
	
	}
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	
	switch (v.getId()) {
	
	case R.id.btnPlay:
	break;	 
	
	
	}
	
	}
		// TODO Auto-generated method stub
		private void primarySeekBarProgressUpdater() {
			 songProgressBar.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song length"
		        if (mediaPlayer.isPlaying()) {
		            Runnable notification = new Runnable() {
		                public void run() {
		                    primarySeekBarProgressUpdater();
		                }
		            };
		            handler.postDelayed(notification, 1000);
		        }
		    }	
	
		private void callasynchtask() {
			// TODO Auto-generated method stub
			GeneralAsynctask submitAsync = new GeneralAsynctask(
					this) {

				@Override
				protected void onPostExecute(String result) {
					super.onPostExecute(result);
					try {

						JSONObject jobj=new JSONObject(result);
									
						String Ack = jobj.getString("status");
						
						if (Ack.equals("success"))  {
							
							JSONArray jarray=jobj.getJSONArray("songanduserlist");
							
							musiccategorylist=new ArrayList<MusiccategoryModel>();
							
							for (int i = 0; i < jarray.length(); i++) {
								
								JSONObject jobj1=jarray.getJSONObject(i);
								
								JSONObject jobj2=jobj1.getJSONObject("user");
								
								MusiccategoryModel item=new MusiccategoryModel(jobj1.getString("songid"), jobj1.getString("song_name"),jobj1.getString("song_category"),jobj1.getString("song_image"),
										jobj1.getString("song_file"),jobj1.getString("likecount"),jobj2.getString("id"),jobj2.getString("name"),jobj2.getString("pic"),jobj2.getString("biography"));
								
								musiccategorylist.add(item);
								
								
							}
							
							//setlist();
						}
						
						else {
							
							showToastLong("No Songs in This Week");
						}
						
						
					} catch (Exception e) {
						e.printStackTrace();
						// showToastLong("Error: Unable to Login.");
					} finally {
						pdspinnerGeneral.dismiss();
					}
				}

				
			};

			try {
				Boolean isInternetPresent = connection.isConnectingToInternet();
				if (isInternetPresent) {

					Log.d("LogIn", "Action: login");
					
						String url = getResources().getString(R.string.app_base_url)
							+ "songlists/"+category_name_str;
					String finalurl = url.replaceAll(" ", "%20");

					submitAsync.execute(finalurl);

				} else {
					showToastLong(getString(R.string.no_internet_message));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}	
		
			
		}

		
			// TODO Auto-generated method stub
			private void setlist() {
				// TODO Auto-generated method stub
				adapter = new MusiccategoryAdapter(Media_Player_Activity.this, musiccategorylist);
				recyclerView.setAdapter(adapter); 
			}	
		
		
		
		
		

		private void showToastLong(String string) {
			// TODO Auto-generated method stub
			
		}


}
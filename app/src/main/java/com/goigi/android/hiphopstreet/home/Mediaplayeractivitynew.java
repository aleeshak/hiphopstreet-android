package com.goigi.android.hiphopstreet.home;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.session.MediaSession;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.session.MediaSessionCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.ArtistProfileActivity;
import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.ViewallActivity;
import com.goigi.android.hiphopstreet.audionotification.MediaPlayerService;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.MusiccategoryModel;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.modelclass.Viewallmodelclass;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;
import com.goigi.android.hiphopstreet.shared.UserShared;
import com.goigi.android.hiphopstreet.sqlite.DBHelper;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;



@SuppressLint("ClickableViewAccessibility")
public class Mediaplayeractivitynew extends Activity implements OnClickListener, OnTouchListener, OnCompletionListener, OnBufferingUpdateListener ,SeekBar.OnSeekBarChangeListener{

	private static String ACTION_EXIT="1";
	public static final String Broadcast_PLAY_NEW_AUDIO = "com.valdioveliu.valdio.audioplayer.PlayNewAudio";
	ImageView add_playlist;
	private ImageView addtoplaylist;
	private ImageView banner,like_view;
	private ImageButton btnBackward;
	private ImageButton btnForward;
	private ImageButton btnNext,showplaylist;
	private ImageButton btnPrevious;
	ViewallActivity child;
	ConnectionDetector connection;
	private int currentSongIndex = 0;
	private ArrayList<TopRatedsong> dataSet1;
	private final Handler handler = new Handler();
	private boolean isRepeat = false;
	private boolean isShuffle = false;
	MusiccategoryModel item;
	String LIKE_STATUS, IMg,song_url,userid;
	private int mediaFileLengthInMilliseconds;
	private MediaPlayer mediaPlayer;
	// Handler to update UI timer, progress bar etc,.
	private final Handler mHandler = new Handler();
	Intent mIntent;
	private MediaSessionCompat mMediaSessionCompat;
	MediaSession mSession;
	Viewallmodelclass itemm;



	private final Runnable mUpdateTimeTask = new Runnable() {
		@Override
		public void run() {
			long totalDuration = mediaPlayer.getDuration();
			long currentDuration = mediaPlayer.getCurrentPosition();
			// Displaying Total Duration time
			songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
			// Displaying time completed playing
			songCurrentDurationLabel.setText(""+utils.milliSecondsToTimer(currentDuration));

			// Updating progress bar
			int progress = (utils.getProgressPercentage(currentDuration, totalDuration));
			//Log.d("Progress", ""+progress);
			songProgressBar.setProgress(progress);

			// Running this thread after 100 milliseconds
			mHandler.postDelayed(this, 100);
		}
	};

	private ArrayList<MusiccategoryModel> musiccategorylist;
	ArrayList<Viewallmodelclass> dataSett;

	ImageButton playbt, pausebt;
	private MediaPlayerService player;
   ProgressDialog progressDialog;
	UserShared psh;
	private MultipartEntity reqEntity;
	private final int seekBackwardTime = 1000; // 5000 milliseconds;
	private final int seekForwardTime = 1000; // 5000 milliseconds
	private TextView songCurrentDurationLabel;
   	String songid_str, artist_id_str,
	image_str,songname_str,songimage_str,
			songfile_str,artistname_str,artistimage_str,likecount_str;

   	SeekBar songProgressBar;
	private TextView songTotalDurationLabel,song_Name,artist_floow,numberoflike;
	private Utilities utils;
	private RelativeLayout wholescreen;

	//this is for category
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.musicplayer);
		setContentView(R.layout.music_player_new);
				// changes by me
		PhoneStateListener phoneStateListener = new PhoneStateListener() {
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				if (state == TelephonyManager.CALL_STATE_RINGING) {
					//INCOMING call
					//do all necessary action to pause the audio
					if(mediaPlayer!=null){//check mediaPlayer
						setPlayerButton(true, false, true);

						if(mediaPlayer.isPlaying()){

							mediaPlayer.pause();
						}
					}

				} else if(state == TelephonyManager.CALL_STATE_IDLE) {
					//Not IN CALL
					//do anything if the phone-state is idle
				} else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
					//A call is dialing, active or on hold
					//do all necessary action to pause the audio
					//do something here
					if(mediaPlayer!=null){//check mediaPlayer
						setPlayerButton(true, false, true);

						if(mediaPlayer.isPlaying()){

							mediaPlayer.pause();
						}
					}
				}
				super.onCallStateChanged(state, incomingNumber);
			}
		};//end PhoneStateListener

		TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if(mgr != null) {
			mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
		}

	//changes ends here


		psh=new UserShared(this);
		connection=new ConnectionDetector(this);
		mIntent = getIntent();
		utils = new Utilities();
		//child=new ViewallActivity();
		playbt = (ImageButton) findViewById(R.id.btnPlay);
		btnForward = (ImageButton) findViewById(R.id.btnForward);
		songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
		songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
		songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
		wholescreen=(RelativeLayout)findViewById(R.id.songThumbnail);
		wholescreen.setVisibility(View.VISIBLE);
		banner = (ImageView) findViewById(R.id.banner_pic);
		like_view=(ImageView)findViewById(R.id.like);
		song_Name=(TextView)findViewById(R.id.songTitle);
		add_playlist=(ImageView)findViewById(R.id.addto_playlist);
		artist_floow=(TextView)findViewById(R.id.floww_artist);
		artist_floow.setText("Artist :"+artistname_str+" >");
		numberoflike=(TextView)findViewById(R.id.numberoflike);
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		btnPrevious=(ImageButton)findViewById(R.id.btnPrevious);
		addtoplaylist=(ImageView)findViewById(R.id.btnRepeat);
		showplaylist=(ImageButton)findViewById(R.id.btnPlaylist);
		songProgressBar.setMax(99); // It means 100% .0-99
		songProgressBar.setOnTouchListener(this);
		playbt.setOnClickListener(this);
		like_view.setOnClickListener(this);
		artist_floow.setOnClickListener(this);
		btnForward.setOnClickListener(this);
		btnNext.setOnClickListener(this);
		btnPrevious.setOnClickListener(this);
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnBufferingUpdateListener(this);
		mediaPlayer.setOnCompletionListener(this);
		addtoplaylist.setOnClickListener(this);
		showplaylist.setOnClickListener(this);
		add_playlist.setOnClickListener(this);

		String songlist=mIntent.getStringExtra("allsong");

		JSONArray jarray;
		try {
			jarray = new JSONArray(songlist);
			musiccategorylist=new ArrayList<MusiccategoryModel>();

			for (int i = 0; i < jarray.length(); i++) {

				JSONObject jobj1=jarray.getJSONObject(i);

				JSONObject jobj2=jobj1.getJSONObject("user");

				item=new MusiccategoryModel(jobj1.getString("songid"),
						jobj1.getString("song_name"),jobj1.getString("song_category"),
						jobj1.getString("song_image"),jobj1.getString("song_file"),
						jobj1.getString("likecount"),
						jobj2.getString("id"),jobj2.getString("name"),
						jobj2.getString("pic"),jobj2.getString("biography"));

				 numberoflike.setText(jobj1.getString("likecount").toString() + " Likes");

				musiccategorylist.add(item);
			}

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if(musiccategorylist.size()==0){
			wholescreen.setVisibility(View.GONE);
			Toast.makeText(getApplicationContext(), "no song in list", Toast.LENGTH_SHORT).show();
		}
		else{

			playSong(0);
		}

		checklikesatus();


	}// end of oncreate

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub

	}



	/**
	 * When user starts moving the progress handler
	 * */
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// remove message Handler from updating progress bar
		mHandler.removeCallbacks(mUpdateTimeTask);
	}

	/**
	 * When user stops moving the progress hanlder
	 * */
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		mHandler.removeCallbacks(mUpdateTimeTask);
		int totalDuration = mediaPlayer.getDuration();
		int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

		// forward or backward to certain seconds
		mediaPlayer.seekTo(currentPosition);

		// update timer progress again
		updateProgressBar();


	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.songProgressBar) {
			/** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
			if (mediaPlayer.isPlaying()) {
				SeekBar sb = (SeekBar) v;
				int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
				mediaPlayer.seekTo(playPositionInMillisecconds);
			}
		}


		return false;
	}
	public void  playSong(int songIndex){
		// Play song


		//Check is service is active
		/*if (!serviceBound) {
			//Store Serializable audioList to SharedPreferences
			StorageUtil storage = new StorageUtil(getApplicationContext());
			storage.storeAudio(musiccategorylist);
			storage.storeAudioIndex(songIndex);
			Intent playerIntent = new Intent(this, MediaPlayerService.class);
			startService(playerIntent);
			bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);

		} else {
			//Store the new audioIndex to SharedPreferences
			StorageUtil storage = new StorageUtil(getApplicationContext());
			storage.storeAudioIndex(songIndex);

			//Service is active
			//Send a broadcast to the service -> PLAY_NEW_AUDIO
			Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
			sendBroadcast(broadcastIntent);
		}*/


		try {

			IMg = musiccategorylist.get(songIndex).song_image;
			if (!IMg.equals("")) {
				try {
					String apiLink = getResources().getString(R.string.image_base_url)+IMg;

					Log.d("apiLink", apiLink);
					String encodedurl = "";
					encodedurl = apiLink.substring(0, apiLink.lastIndexOf('/'))
							+ "/"
							+ Uri.encode(apiLink.substring(apiLink.lastIndexOf('/') + 1));
					Log.d("UserProfile", "encodedurl:"
							+ encodedurl);
					if (!apiLink.equals("") && apiLink != null) {
				/*		Picasso.with(this).load(encodedurl) // load: This path may
						// be a remote URL,
						.placeholder(R.drawable.placeholdergb)
						// .resize(130, 130)
						.error(R.drawable.placeholdergb)
						.into(banner);*/


						Picasso.get().load(encodedurl).into(banner);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			mediaPlayer.reset();
			mediaPlayer.setDataSource(getResources().getString(R.string.song_base_url)+musiccategorylist.get(songIndex).song_file);
			mediaPlayer.prepare();
			mediaPlayer.start();
			//player.playMedia();
			// Displaying Song title
			String songTitle = musiccategorylist.get(songIndex).song_name;

			song_Name.setText(songTitle);
			//song_url=mIntent.getStringExtra("myurl");
			// Changing Button Image to pause image
			playbt.setImageResource(R.drawable.btn_pause);
			artist_floow.setText("Artist :"+musiccategorylist.get(songIndex).name+" >");
			// set Progress bar values
			songProgressBar.setProgress(0);
			songProgressBar.setMax(100);

			updateProgressBar();

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}///try ends
	private void primarySeekBarProgressUpdater() {
		songProgressBar.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song length"
		if (mediaPlayer.isPlaying()) {
			Runnable notification = new Runnable() {
				@Override
				public void run() {
					primarySeekBarProgressUpdater();
				}
			};
			handler.postDelayed(notification, 1000);
		}
	}

	private void setPlayerButton(Boolean play, Boolean pause, Boolean stop){
		playbt.setEnabled(play);
		if(play==true)
			playbt.setEnabled(true);
		else
			playbt.setEnabled(false);

	}

	public void showNotification(View view){
		new MyNotification(this);
		finish();
	}

	private void showToastLong(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}

	/**
	 * Update timer on seekbar
	 * */
	public void updateProgressBar() {
		mHandler.postDelayed(mUpdateTimeTask, 100);
	}


	public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
		String[] message;
		String responseString = "";

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {
				String apiUrl = getResources().getString(R.string.app_base_url)+"like";
				Log.d("TestActivity", "Action: accountupdate");
				responseString = "";

				HttpParams params1 = new BasicHttpParams();
				params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
						HttpVersion.HTTP_1_1);
				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost postRequest = new HttpPost(apiUrl);
				//HttpPut postRequest = new HttpPut(apiUrl);
				postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");
				/*to print in log*/
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				reqEntity.writeTo(bytes);
				String content = bytes.toString();
				Log.e("MultiPartEntityRequest:",content);
				/*to print in log*/
				postRequest.setEntity(reqEntity);
				HttpResponse response = httpClient.execute(postRequest);

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(
								response.getEntity().getContent(), "UTF-8"));

				String sResponse;

				StringBuilder s = new StringBuilder();

				while ((sResponse = reader.readLine()) != null) {
					s = s.append(sResponse);
				}

				responseString = s.toString();
				Log.d(" ResponseString =>", responseString);

			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			try {
				if (!responseString.equals("")) {

					JSONObject jsonObject = new JSONObject(responseString);
					String Ack = jsonObject.getString("status");
					String msg=jsonObject.getString("message");

					if (Ack.equals("success")   ) {
						progressDialog.dismiss();
						progressDialog.cancel();
						showToastLong(msg);

						like_view.setImageResource(R.drawable.liked);

					}
					else {
						progressDialog.dismiss();
						progressDialog.cancel();
						showToastLong(msg);
					}
				}
				else {
					progressDialog.dismiss();
					progressDialog.cancel();
					showToastLong("Sorry! Problem cannot be recognized.");
				}
			} catch (Exception e) {
				progressDialog.dismiss();
				progressDialog.cancel();
				e.printStackTrace();
			}
		}

	}

	private void checklikesatus() {
		// TODO Auto-generated method stub
		GeneralAsynctask submitAsync = new GeneralAsynctask(
				this) {

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {

					JSONObject jobj=new JSONObject(result);
					String Ack = jobj.getString("status");
					if (Ack.equals("found"))  {
						like_view.setImageResource(R.drawable.liked);
						LIKE_STATUS="1";

						//setlist();
					}

					else {

						//showToastLong("No Songs in This Week");
					}


				} catch (Exception e) {
					e.printStackTrace();
					// showToastLong("Error: Unable to Login.");
				} finally {
					pdspinnerGeneral.dismiss();
				}
			}

		};

		try {
			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {

				Log.d("LogIn", "Action: login");

				String url = getResources().getString(R.string.app_base_url)
						+ "like/"+songid_str+"/"+artist_id_str;
				String finalurl = url.replaceAll(" ", "%20");

				submitAsync.execute(finalurl);

			} else {
				showToastLong(getString(R.string.no_internet_message));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		if ( mediaPlayer.isPlaying()) {
			mediaPlayer.stop();

		 finish();

		}
		else {

			finish();
		}


	}

	@Override
	public void onBufferingUpdate(MediaPlayer mediaPlayer, int percent) {
		// TODO Auto-generated method stub
		/** Method which updates the SeekBar secondary progress by current song loading from URL position*/
		songProgressBar.setSecondaryProgress(percent);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
			case R.id.btnPlay:

				// check for already playing
				if(mediaPlayer.isPlaying()){
					if(mediaPlayer!=null){
						mediaPlayer.pause();

						playbt.setImageResource(R.drawable.btn_play);
					}
				}else{

					if(mediaPlayer!=null){
						mediaPlayer.start();
						// Changing button image to pause button
						playbt.setImageResource(R.drawable.btn_pause);
					}
				}

				primarySeekBarProgressUpdater();

				break;

			case R.id.like:
				likeasynchtask();
				break;

			case R.id.floww_artist:
				if ( mediaPlayer.isPlaying()) {
					mediaPlayer.stop();
				}

				for (int i = 0; i <musiccategorylist.size() ; i++) {


					if(currentSongIndex==i){
						songid_str=musiccategorylist.get(i).songid;
						songname_str=musiccategorylist.get(i).song_name;
						songimage_str=musiccategorylist.get(i).song_image;
						songfile_str=musiccategorylist.get(i).song_file;
						artist_id_str=musiccategorylist.get(i).id;
						artistname_str =musiccategorylist.get(i).name;;
						artistimage_str =musiccategorylist.get(i).pic;
						likecount_str=musiccategorylist.get(i).likecount;

					}

				}

				Intent o=new Intent(this,ArtistProfileActivity.class);
				o.putExtra("datamodel",item);
				o.putExtra("artistid",artist_id_str);
				o.putExtra("artistname",artistname_str);
				o.putExtra("artistimage",artistimage_str);


				startActivity(o);


				break;


			case R.id.btnForward:
				// get current song position
				int currentPosition = mediaPlayer.getCurrentPosition();
				// check if seekForward time is lesser than song duration
				if(currentPosition + seekForwardTime <= mediaPlayer.getDuration()){
					// forward song
					mediaPlayer.seekTo(currentPosition + seekForwardTime);
				}else{
					// forward to end position
					mediaPlayer.seekTo(mediaPlayer.getDuration());
				}


				break;

			case R.id.btnBackward:

				// get current song position
				int currentPosition2 = mediaPlayer.getCurrentPosition();
				// check if seekBackward time is greater than 0 sec
				if(currentPosition2 - seekBackwardTime >= 0){
					// forward song
					mediaPlayer.seekTo(currentPosition2 - seekBackwardTime);
				}else{
					// backward to starting position
					mediaPlayer.seekTo(0);
				}


				break;

			case R.id.btnNext:

				if(musiccategorylist.size()==0){

					Toast.makeText(Mediaplayeractivitynew.this, "No Song found", Toast.LENGTH_SHORT).show();

				}
				else if((currentSongIndex < (musiccategorylist.size() - 1))){
					playSong(currentSongIndex + 1);
					currentSongIndex = currentSongIndex + 1;
				}
				else{
					// play first song
					playSong(0);
					currentSongIndex = 0;
				}

				break;

			case R.id.btnRepeat:


				if(isRepeat){
					isRepeat = false;
					//Toast.makeText(this, "Repeat is OFF", Toast.LENGTH_SHORT).show();
					addtoplaylist.setImageResource(R.drawable.replay);
				}else{
					// make repeat to true
					isRepeat = true;
					//Toast.makeText(this, "Repeat is ON", Toast.LENGTH_SHORT).show();
					// make shuffle to false
					isShuffle = false;
					addtoplaylist.setImageResource(R.drawable.replay);
					//btnShuffle.setImageResource(R.drawable.btn_shuffle);
				}

				break;

			case R.id.addto_playlist:
				Toast.makeText(getApplicationContext(), "Song Added to Playlist", Toast.LENGTH_SHORT).show();

				for (int i = 0; i <musiccategorylist.size() ; i++) {


					if(currentSongIndex==i){
				songid_str=musiccategorylist.get(i).songid;
				songname_str=musiccategorylist.get(i).song_name;
				songimage_str=musiccategorylist.get(i).song_image;
				songfile_str=musiccategorylist.get(i).song_file;
				artist_id_str=musiccategorylist.get(i).id;
				artistname_str =musiccategorylist.get(i).name;;
				artistimage_str =musiccategorylist.get(i).pic;
				likecount_str=musiccategorylist.get(i).likecount;

					}

				}


				  DBHelper db=new DBHelper(this);
				   db.insertMUSIC_TABLE(songid_str,songname_str,
					 songimage_str, userid,song_url,songfile_str,
					  likecount_str,artist_id_str,artistname_str,artistimage_str);


				   break;


			case R.id.btnPlaylist:
				mediaPlayer.pause();
				playbt.setImageResource(R.drawable.btn_play);
				Intent i=new Intent(this,ListActivity.class);
				mediaPlayer.stop();
				startActivity(i);

				break;

			case R.id.btnPrevious:
				if(musiccategorylist.size()==0){

					Toast.makeText(Mediaplayeractivitynew.this, "No Song found", Toast.LENGTH_SHORT).show();

				}else if(currentSongIndex < (musiccategorylist.size() - 1)){
					playSong(currentSongIndex - 1);
					currentSongIndex = currentSongIndex -1 ;
				}else{
					// play first song
					//playSong(0);
					playSong(currentSongIndex);
					currentSongIndex = musiccategorylist.size() - 1;
				}

				break;


			default:
				break;
		}


	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// check for repeat is ON or OFF
		if(isRepeat){
			// repeat is on play same song again
			playSong(currentSongIndex);
		} else if(isShuffle){
			// shuffle is on - play a random song
			Random rand = new Random();
			currentSongIndex = rand.nextInt((musiccategorylist.size() - 1) - 0 + 1) + 0;
			playSong(currentSongIndex);
		} else{
			// no repeat or shuffle ON - play next song
			if(currentSongIndex < (musiccategorylist.size() - 1)){
				playSong(currentSongIndex + 1);
				currentSongIndex = currentSongIndex + 1;
			}else{

				currentSongIndex = 0;
			}
		}
	}

	/*private void likeasynchtask() {
		// TODO Auto-generated method stub
		try {

			reqEntity = null;
			reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			reqEntity.addPart("userid", new StringBody(artist_id_str));
			reqEntity.addPart("songid", new StringBody(songid_str));

		}
		catch (Exception e) {
			e.printStackTrace();
		}

		Boolean isInternetPresent = connection.isConnectingToInternet();

		if (isInternetPresent) {
			progressDialog = ProgressDialog.show(this,
					"",
					getString(R.string.progress_bar_loading_message),
					false);

			UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
			editProfileAsyncTask.execute((Void) null);
		} else {
			showToastLong(getString(R.string.no_internet_message));
		}

	}*/

	private void likeasynchtask() {
		// TODO Auto-generated method stub
		try {

			reqEntity = null;
			reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			reqEntity.addPart("userid", new StringBody(psh.getUserId()));
			reqEntity.addPart("songid", new StringBody(mIntent.getStringExtra("id")));


		} catch (Exception e) {
			e.printStackTrace();
		}


		Boolean isInternetPresent = connection.isConnectingToInternet();

		if (isInternetPresent) {
			progressDialog = ProgressDialog.show(this,
					"",
					getString(R.string.progress_bar_loading_message),
					false);

			Mediaplayeractivitynew.UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
			editProfileAsyncTask.execute((Void) null);
		} else {
			showToastLong(getString(R.string.no_internet_message));
		}

	}

}

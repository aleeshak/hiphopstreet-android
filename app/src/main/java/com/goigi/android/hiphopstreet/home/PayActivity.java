package com.goigi.android.hiphopstreet.home;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.DashBoardActivity;
import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.home.WalletActivity.UploadProfileAsyncTask;
import com.goigi.android.hiphopstreet.shared.UserShared;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

public class PayActivity extends Activity {

	//	sandbox:  AYbt_eZE37xSWswaBcO0rdcFKF1mdDgQdyrWu_LRdLud9kyrl-RfA96iw4Kl_IRqJRwJw_UG_FtUL8Al
	
	//live: Ac1Jma6lsa6Zbn-8zQdAIkDOI1zb8yLMjnS3FFBoecnmRSVoLyQ6mNlP-KugjrjPpyQpEMAXocr03-oF
	//Ac1Jma6lsa6Zbn-8zQdAIkDOI1zb8yLMjnS3FFBoecnmRSVoLyQ6mNlP-KugjrjPpyQpEMAXocr03-oF
	
	private static final String CONFIG_CLIENT_ID = "ATDCaNQRZwG6xCprthRrwLmT0Yqx6RDWDzTa1EvjlpQwnLPITqyIDZvZjqhgnNa_Re9CiERY107k02zE";
	private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
	private static final int REQUEST_PAYPAL_PAYMENT = 1;
	private TextView addmoney,remain_money;
	private String amount="0.00";
	private String song_id_str="";
	private ProgressDialog progressDialog;
	private ConnectionDetector connection;
	
  	private MultipartEntity reqEntity;
	private ImageView details;
	private String paymentId="0.00";

	private static PayPalConfiguration config = new PayPalConfiguration()
	.environment(CONFIG_ENVIRONMENT)
	.clientId(CONFIG_CLIENT_ID)
	// The following are only used in PayPalFuturePaymentActivity.
	.merchantName("Hiphopstreets")
	.merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
	.merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

	private boolean ff=false;
	Intent i;

	/* private static final String CONFIG_CLIENT_ID = "Ab5dobPclUpA7-gHHs2PSk1WPLWF2H4DKxXv3zRwevArFBCq3wMKIlffZF2cDqUjfbUwFzFTZSejgitg";*/

	private UserShared psh;
	private String remain;

	private void Addmoneytowallet(String string) {

		// TODO Auto-generated method stub
		try {

			reqEntity = null;
			reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			
			reqEntity.addPart("uid", new StringBody(psh.getUserId()));
			reqEntity.addPart("songid", new StringBody(song_id_str));
			reqEntity.addPart("ref_id", new StringBody(paymentId));
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}


		Boolean isInternetPresent = connection.isConnectingToInternet();

		if (isInternetPresent) {
			progressDialog = ProgressDialog.show(this,
					"",
					getString(R.string.progress_bar_loading_message),
					false);

			UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
			editProfileAsyncTask.execute((Void) null);
		} else {
			showToastLong(getString(R.string.no_internet_message));
		}
	}

	private String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current date => "+c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_PAYPAL_PAYMENT) {
			if (resultCode == Activity.RESULT_OK) {
				PaymentConfirmation confirm = data
						.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
				if (confirm != null) {
					try {
						System.out.println("Responseeee"+confirm);
						Log.i("paymentExample", confirm.toJSONObject().toString());


						JSONObject jsonObj=new JSONObject(confirm.toJSONObject().toString());
						// JSONObject jsonchild=jsonObj.getJSONObject("")
						
						paymentId=jsonObj.getJSONObject("response").getString("id");
						System.out.println("payment id:-=="+paymentId);
						Toast.makeText(getApplicationContext(), paymentId, Toast.LENGTH_LONG).show();
						Addmoneytowallet(amount);

					} catch (JSONException e) {
						Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
					}
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Log.i("paymentExample", "The user canceled.");
			} else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
				Log.i("paymentExample", "An invalid Payment was submitted. Please see the docs.");
			}
		}


	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wallet);
		
		amount=getIntent().getStringExtra("money");
		
		song_id_str=getIntent().getStringExtra("songid");
		connection=new ConnectionDetector(this);
		psh=new UserShared(this);
		
		//remain_money.setText(remain);
		/**
		 * call pay pal services
		 */

		Intent intent = new Intent(this, PayPalService.class);
		intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
		startService(intent);
		
		PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(amount),"USD", "Hiphopstreets",
				PayPalPayment.PAYMENT_INTENT_AUTHORIZE);

		Intent i = new Intent(PayActivity.this, PaymentActivity.class);

		i.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

		startActivityForResult(i, REQUEST_PAYPAL_PAYMENT);
		
		
		



	}//end of oncreate

	protected void showToastLong(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(PayActivity.this, string, Toast.LENGTH_SHORT).show();
	}

	private String time() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => "+c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	
	public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
  		String[] message;
  		String responseString = "";

  		@Override
  		protected Boolean doInBackground(Void... params) {
  			// TODO: attempt authentication against a network service.

  			try {
  				String apiUrl = getResources().getString(R.string.app_base_url)+"featuredsong";
  				Log.d("TestActivity", "Action: accountupdate");
  				responseString = "";

  				HttpParams params1 = new BasicHttpParams();
  				params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
  						HttpVersion.HTTP_1_1);
  				HttpClient httpClient = new DefaultHttpClient(params1);
  				HttpPost postRequest = new HttpPost(apiUrl);
  				//HttpPut postRequest = new HttpPut(apiUrl);
  				postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");

  				
  				
				/*to print in log*/
				
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();

				reqEntity.writeTo(bytes);

				String content = bytes.toString();

				Log.e("MultiPartEntityRequest:",content);
				
				/*to print in log*/
  				
  				postRequest.setEntity(reqEntity);
  				HttpResponse response = httpClient.execute(postRequest);

  				BufferedReader reader = new BufferedReader(
  						new InputStreamReader(
  								response.getEntity().getContent(), "UTF-8"));

  				String sResponse;

  				StringBuilder s = new StringBuilder();

  				while ((sResponse = reader.readLine()) != null) {
  					s = s.append(sResponse);
  				}

  				responseString = s.toString();
  				Log.d("TestActivity ResponseString =>", responseString);

  			} catch (Exception e) {
  				e.printStackTrace();
  			}

  			return false;
  		}

  		@Override
  		protected void onPostExecute(final Boolean success) {

  			try {
  				if (!responseString.equals("")) {

  					JSONObject jsonObject = new JSONObject(responseString);
  					String Ack = jsonObject.getString("status");
  					String msg=jsonObject.getString("message");
  					
  					
  					
  					if (Ack.equals("success")   ) {
  						showToastLong(msg);
  						progressDialog.dismiss();
  						progressDialog.cancel();
  						
  						Intent i=new Intent(PayActivity.this,DashBoardActivity.class);  					
  					      startActivity(i);
  				
  					
  					
  					}
  					
  					
  					else {
  						progressDialog.dismiss();
  						progressDialog.cancel();
  						showToastLong(msg);
  					}
  				}
  				else {
  					progressDialog.dismiss();
  					progressDialog.cancel();
  					showToastLong("Sorry! Problem cannot be recognized.");
  				}
  			} catch (Exception e) {
  				progressDialog.dismiss();
  				progressDialog.cancel();
  				e.printStackTrace();
  			}
  		}




	}	



}

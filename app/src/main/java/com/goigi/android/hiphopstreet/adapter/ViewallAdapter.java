package com.goigi.android.hiphopstreet.adapter;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.home.Media_Player_Activity;
import com.goigi.android.hiphopstreet.home.MediaplayerActivity_search;
import com.goigi.android.hiphopstreet.home.Mediaplayeractivitynew;
import com.goigi.android.hiphopstreet.home.Mediaplayeractivitynew1;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.sqlite.DBHelper;
import com.squareup.picasso.Picasso;

import customfonts.MyTextView;

public class ViewallAdapter extends RecyclerView.Adapter<ViewallAdapter.ViewHolder>{
      private LayoutInflater layoutInflater;
    Context context;
     int[] propic;
     private ImageView imaheview;
     private ArrayList<TopRatedsong> dataSet1;
	    //private ArrayList<Categorylist>dataset2; 
	    String song_id;
	    private String Imgview;
	    private ArrayList<String> musicid;
	    private String ID,Name,Image,songfile;
	    
	    String Path;
	    private String fileName ;
	    ConnectionDetector connection;
	    String apiLink;
	    
	    
	    
	    public ViewallAdapter(Context context,ArrayList<TopRatedsong> dataSet1) {
	        this.context = context;
	        this.dataSet1=dataSet1;
	        //this.dataset2=dataset2;
	    
	    
	    }

	    // Create new views (invoked by the layout manager)
	    @Override
	    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
	        // create a new view
	        View itemLayoutView = LayoutInflater.from(parent.getContext())
	                .inflate(R.layout.viewall_item, null);

	        // create ViewHolder

	        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
	        return viewHolder;
	    }

	    @Override
	    public void onBindViewHolder( final ViewHolder viewHolder, final int position) {
	    		    	   	
	    	 ImageView imageView = viewHolder.imgflag;
	    	 Imgview=dataSet1.get(position).song_image;
	    	 final TextView name=viewHolder.song_name;
	    	 name.setText(dataSet1.get(position).song_name);
	    	 name.setVisibility(View.VISIBLE);
	    	 song_id=dataSet1.get(position).songid;
	    	 songfile=dataSet1.get(position).song_file;
	    	 Name=dataSet1.get(position).song_name;
	    	 connection=new ConnectionDetector(context);
	    	 
	    	 
	    	 if (!Imgview.equals("")) {
				try {
					//String apiLink = Link+Imgview;
					apiLink = context.getResources().getString(R.string.image_base_url)+Imgview;

					String encodedurl = "";
					encodedurl = apiLink.substring(0,apiLink.lastIndexOf('/'))+ "/"+ Uri.encode(apiLink.substring(
							apiLink.lastIndexOf('/') + 1));
					Log.d("UserProfile Social List adapter", "encodedurl:"+encodedurl);
					if (!apiLink.equals("") && apiLink != null) {
				/*		Picasso.with(context)
						.load(encodedurl) // load: This path may be a remote URL,
						.placeholder(R.drawable.placeholdergb)
						.error(R.drawable.placeholdergb)
						.resize(1280, 720)
						.into(imageView); // Into: ImageView into which the final image has to be passed
						//.resize(130, 130);*/

						Picasso.get().load(encodedurl).placeholder(R.drawable.placeholdergb).into(imageView);

						//Picasso.get().load(encodedurl).placeholder(R.drawable.default_pic).into(imageView);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
	    	
	    	         viewHolder.itemView.setOnClickListener(new OnClickListener() {
	    		    				
				@Override
				public void onClick(View v) {
					 Intent intent = new Intent(context, MediaplayerActivity_search.class);
					 intent.putExtra("BrandDetails", dataSet1.get(position));
					 intent.putExtra("position",	String.valueOf(position));
					 intent.putExtra("image",dataSet1.get(position).song_image);
					 intent.putExtra("id",dataSet1.get(position).songid);
					 intent.putExtra("name", dataSet1.get(position).song_name);
					 intent.putExtra("songfile",dataSet1.get(position).song_file);
					 intent.putExtra("artist_id", dataSet1.get(position).userid);
					 intent.putExtra("artist_name", dataSet1.get(position).name);
					 intent.putExtra("artist_image", dataSet1.get(position).pic);
					 intent.putExtra("allsongs", dataSet1);
					 context.startActivity(intent);
					 Toast.makeText(context, dataSet1.get(position).song_file, Toast.LENGTH_SHORT).show();
				
				}
			});

	    }

	    // inner class to hold a reference to each item of RecyclerView
	    public static class ViewHolder extends RecyclerView.ViewHolder {

	        public ImageView imgflag;
	        public MyTextView song_name,artistname,song_file;

	        public ViewHolder(View itemLayoutView) {
	            super(itemLayoutView);
	                
	            
	            
	            imgflag = (ImageView) itemLayoutView.findViewById(R.id.img2);
	            song_name=(MyTextView)itemLayoutView.findViewById(R.id.songname2);
	            
	        }
	    }

	    // Return the size of your itemsData (invoked by the layout manager)
	    @Override
	    public int getItemCount() {
	        return dataSet1.size();
	    }

	    	
	  

}

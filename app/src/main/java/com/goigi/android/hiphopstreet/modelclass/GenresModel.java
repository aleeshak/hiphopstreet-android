package com.goigi.android.hiphopstreet.modelclass;

import java.io.Serializable;



public class GenresModel implements Serializable{
	
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -330948787339116169L;
	
	
	public String Genres_id;
	
	public String Genres_Name;
	
	
	
	
	public GenresModel(String Genres_id,String Genres_Name){
		
		this.Genres_id=Genres_id;
	
		this.Genres_Name=Genres_Name;
		
		
		
		
	}

	
	@Override
    public String toString() {
        return this.Genres_Name;            // What to display in the Spinner list.
    }
	
	
}
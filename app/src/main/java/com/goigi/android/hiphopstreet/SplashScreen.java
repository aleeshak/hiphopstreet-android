package com.goigi.android.hiphopstreet;

import com.goigi.android.hiphopstreet.UserPanel.UserDashBoard;
import com.goigi.android.hiphopstreet.home.HomepageActivity;
import com.goigi.android.hiphopstreet.shared.UserShared;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout.LayoutParams;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

public class SplashScreen extends Activity{
	
	RelativeLayout rl;
	VideoView video;
	MediaController Controller;
	private MediaController media_control;
	private VideoView video_view;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		video_view = (VideoView) findViewById(R.id.VideoView);

		 Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.demo);

		    media_control = new MediaController(this);
		    media_control.setVisibility(View.GONE);
		    video_view.setMediaController(media_control);

		    video_view.setVideoURI(uri);

		    video_view.start();
            myThread(); 	
			
	}

	public void myThread(){
 		Thread th=new Thread(){
 	       
 			@Override
 			public void run(){
 				try {
 					Thread.sleep(20000);
 					runOnUiThread(new Runnable() {
 			             
 						@Override
 						public void run() {
 							video_view.stopPlayback();
 							
 							if(new UserShared(SplashScreen.this).getLoggedInStatus()){
								Intent intent = new Intent(SplashScreen.this,UserDashBoard.class);
								startActivity(intent);
							}



							else if(new UserShared(SplashScreen.this).getLoggedInStatusArtist()){
								Intent intent = new Intent(SplashScreen.this,  DashBoardActivity.class);
								startActivity(intent);
							}
 							else {
 								
 								Intent intent = new Intent(SplashScreen.this, SigninActivity.class);
								startActivity(intent);
							}
 							
 						}
 					});
 				}
 				catch (Exception e) {
 					e.printStackTrace();
 				}
 			}
 		}; //End of thread class
 		
 		th.start();
 	}//End of myThread()

    



	    }




package com.goigi.android.hiphopstreet.home;

import java.util.ArrayList;

import org.apache.http.entity.mime.MultipartEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.DashBoardActivity;
import com.goigi.android.hiphopstreet.MainActivity;
import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.adapter.MusiccategoryAdapter;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.MusicModel;
import com.goigi.android.hiphopstreet.modelclass.MusiccategoryModel;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;

public class CategorylistActivity extends AppCompatActivity {

	RecyclerView recyclerView;

	ConnectionDetector connection;
	
	private MultipartEntity reqEntity;
	private ProgressDialog progressDialog;
	ImageView noMusic;
	public ArrayList<MusiccategoryModel> musiccategorylist;
	MusiccategoryAdapter adapter;
	Intent mintent;
	String category_name_str;
	Toolbar toolbar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//getLayoutInflater().inflate(R.layout.music_list_activity, container);
		setContentView(R.layout.music_list_activity);
		
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Hiphopstreet");
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
/*
				Intent intent=new Intent();

				setResult(100,intent);*/
				finish();
				/*Intent i= new Intent(LoginActivity.this,StartUpActivity.class);
					startActivity(i);*/
			}
		});
		connection=new ConnectionDetector(this);
		xmlinits();
		callasynchtask();
		 
	}

	

	
	
	
	
	private void callasynchtask() {
		// TODO Auto-generated method stub
		GeneralAsynctask submitAsync = new GeneralAsynctask(
				this) {

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {

					JSONObject jobj=new JSONObject(result);
								
					String Ack = jobj.getString("status");
					
					if (Ack.equals("success"))  {
						
						JSONArray jarray=jobj.getJSONArray("songanduserlist");
						
						musiccategorylist=new ArrayList<MusiccategoryModel>();
						
						for (int i = 0; i < jarray.length(); i++) {
							
							JSONObject jobj1=jarray.getJSONObject(i);
							
							JSONObject jobj2=jobj1.getJSONObject("user");
							
							MusiccategoryModel item=new MusiccategoryModel(jobj1.getString("songid"), jobj1.getString("song_name"),
									jobj1.getString("song_category"),jobj1.getString("song_image"),
									jobj1.getString("song_file"),jobj1.getString("likecount"),jobj2.getString("id"),jobj2.getString("name"),
									jobj2.getString("pic"),jobj2.getString("biography"));
							
							     musiccategorylist.add(item);
							
							
						}
						
						setlist();
					}
					
					else {
						
						showToastLong("No Songs in This Week");
					}
					
					
				} catch (Exception e) {
					e.printStackTrace();
					// showToastLong("Error: Unable to Login.");
				} finally {
					pdspinnerGeneral.dismiss();
				}
			}

		};

		try {
			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {

				Log.d("LogIn", "Action: login");
				
					String url = getResources().getString(R.string.app_base_url)
						+ "songlists/"+category_name_str;
				String finalurl = url.replaceAll(" ", "%20");

				submitAsync.execute(finalurl);

			} else {
				showToastLong(getString(R.string.no_internet_message));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}	
	
		
	}

	protected void showToastLong(String string) {
		// TODO Auto-generated method stub
	Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG).show();	
	}

	private void xmlinits() {
		// TODO Auto-generated method stub
		mintent=getIntent();
		category_name_str=mintent.getStringExtra("categoryname");
		noMusic=(ImageView)findViewById(R.id.no_music);
		
		 recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
	        recyclerView.setHasFixedSize(true);
	        LinearLayoutManager layoutManager = new LinearLayoutManager(CategorylistActivity.this);
	        GridLayoutManager  gridLayoutManagerVertical =
	                new GridLayoutManager(this,
	                        2, //The number of Columns in the grid
	                        LinearLayoutManager.HORIZONTAL,
	                        false);
	        recyclerView.setLayoutManager(gridLayoutManagerVertical);
	        
		
	}


	private void setlist() {
		// TODO Auto-generated method stub
		adapter = new MusiccategoryAdapter(CategorylistActivity.this, musiccategorylist);
		recyclerView.setAdapter(adapter); 
	}



}

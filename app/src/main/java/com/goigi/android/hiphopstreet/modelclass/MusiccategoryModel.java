package com.goigi.android.hiphopstreet.modelclass;

import java.io.Serializable;

public class MusiccategoryModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    public String songid;
	public String song_name;
	public String song_category;
	public String song_image;
	public String song_file;
	public String likecount;
	public String id;
	public String name;
	public String pic;
	public String biography;
	
	public MusiccategoryModel(String songid,
							  String song_name,
							  String song_category,
			                  String song_image,
							  String song_file,
							  String likecount,
			                  String id,
							  String name,
							  String pic,
							  String biography){
		
		this.songid=songid;
		this.song_name=song_name;
		this.song_category=song_category;
		this.song_image=song_image;
		this.song_file=song_file;
		this.likecount=likecount;
		 this.id=id;
	     this.name=name;
	     this.pic=pic;
	     this.biography=biography;
	
	
	}


}

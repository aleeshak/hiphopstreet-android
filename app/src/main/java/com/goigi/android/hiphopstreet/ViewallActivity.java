package com.goigi.android.hiphopstreet;


import java.util.ArrayList;

import org.apache.http.entity.mime.MultipartEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.adapter.ViewallAdapter;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;

public class ViewallActivity extends MainActivity implements OnClickListener{
	private Button btn_next;
	private Button btn_prev;
	ConnectionDetector connection;
	private int increment = 0;
	private LinearLayoutManager mLayoutManager;
	private RecyclerView mRecyclerView;
	public int NUM_ITEMS_PAGE   = 10;
	private int pageCount ;
	private ProgressDialog progressDialog;
	private MultipartEntity reqEntity;

	public ArrayList<TopRatedsong> topratedsong;


	public int TOTAL_LIST_ITEMS = 0;

	private void callasynctask() {
		// TODO Auto-generated method stub
		GeneralAsynctask submitAsync = new GeneralAsynctask(
				this) {

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {

					JSONObject jobj=new JSONObject(result);

					String Ack = jobj.getString("status");

					if (Ack.equals("success"))  {

						JSONArray jarray=jobj.getJSONArray("songanduserlist");

						topratedsong=new ArrayList<TopRatedsong>();

						for (int i = 0; i <jarray.length(); i++) {

							JSONObject jobj1=jarray.getJSONObject(i);
							JSONObject jobj2=jobj1.getJSONObject("user");

							TopRatedsong item=new TopRatedsong(jobj1.getString("songid"), jobj1.getString("song_name"),
									jobj1.getString("song_image"),jobj1.getString("likecount"),
									jobj1.getString("song_file"),jobj2.getString("id"),jobj2.getString("name"),jobj2.getString("pic"));

							      topratedsong.add(item);

						}

						setlist();
					}

					else {

						showToastLong("No Songs in This Week");
					}




				} catch (Exception e) {
					e.printStackTrace();
					// showToastLong("Error: Unable to Login.");
				} finally {
					pdspinnerGeneral.dismiss();
				}
			}


		};

		try {
			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {

				Log.d("LogIn", "Action: login");


				String url = getResources().getString(R.string.app_base_url)
						+ "songlist/";
				Log.d("url",url);

				String finalurl = url.replaceAll(" ", "%20");

				submitAsync.execute(finalurl);

			} else {
				showToastLong(getString(R.string.no_internet_message));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private void CheckEnable()
	{
		if(increment+1 == pageCount)
		{
			btn_next.setEnabled(false);
		}
		else if(increment == 0)
		{
			btn_prev.setEnabled(false);
		}
		else
		{
			btn_prev.setEnabled(true);
			btn_next.setEnabled(true);
		}
	}

	private void loadList(int number,ArrayList<TopRatedsong> data)
	{
		ArrayList<TopRatedsong> sort=new ArrayList<TopRatedsong>();
		int start = number * NUM_ITEMS_PAGE;
		for(int i=start;i<(start)+NUM_ITEMS_PAGE;i++)
		{
			if(i<data.size())

			{
				TopRatedsong mo=new TopRatedsong(data.get(i).songid, data.get(i).song_name,
						data.get(i).song_image,data.get(i).likecount,
						data.get(i).song_file,data.get(i).userid,data.get(i).name,data.get(i).pic);
				sort.add(mo);
			}
			else
			{
				break;
			}
		}

		ViewallAdapter dashboardAdapter = new ViewallAdapter(ViewallActivity.this,sort);
		mRecyclerView.setAdapter(dashboardAdapter);

	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.prev:
			increment--;
			loadList(increment,topratedsong);
			CheckEnable();

			break;



		case R.id.next:
			increment++;
			loadList(increment,topratedsong);
			CheckEnable();

			break;


		default:
			break;
		}


	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.viewall);
		getLayoutInflater().inflate(R.layout.viewall, container);

		connection=new ConnectionDetector(this);

		xmlinits();
		xmlonclicks();

		callasynctask();

	}//end of oncreate

	protected void setlist() {
		// TODO Auto-generated method stub

		TOTAL_LIST_ITEMS=topratedsong.size();

		int val = TOTAL_LIST_ITEMS%NUM_ITEMS_PAGE;
		val = val==0?0:1;
		pageCount = TOTAL_LIST_ITEMS/NUM_ITEMS_PAGE+val;


		loadList(0,topratedsong);



	}

	private void showToastLong(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG).show();
	}

	private void xmlinits() {
		// TODO Auto-generated method stub
		mRecyclerView=(RecyclerView)findViewById(R.id.recycler_view_all);
		btn_prev = (Button)findViewById(R.id.prev);
		btn_next = (Button)findViewById(R.id.next);
		mRecyclerView.setHasFixedSize(true);
		mLayoutManager = new GridLayoutManager(this, 2);
		mRecyclerView.setLayoutManager(mLayoutManager);
		btn_prev.setEnabled(false);




	}

	private void xmlonclicks() {
		// TODO Auto-generated method stub
		btn_next.setOnClickListener(this);
		btn_prev.setOnClickListener(this);

	}

}

package com.goigi.android.hiphopstreet.adapter;

import java.util.ArrayList;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.adapter.DashboardAdapter.ViewHolder;
import com.goigi.android.hiphopstreet.adapter.Uploadmusiclistadapter.MyViewHolder;
import com.goigi.android.hiphopstreet.home.Media_Player_Activity;
import com.goigi.android.hiphopstreet.home.Mediaplayeractivitynew;
import com.goigi.android.hiphopstreet.home.Mediaplayeractivitynew1;
import com.goigi.android.hiphopstreet.home.PayActivity;
import com.goigi.android.hiphopstreet.home.Uploadedmusiclist;
import com.goigi.android.hiphopstreet.home.UploadmusicActivity;
import com.goigi.android.hiphopstreet.home.WalletActivity;

import com.goigi.android.hiphopstreet.modelclass.MusicModel;
import com.goigi.android.hiphopstreet.modelclass.TopReleases;
import com.goigi.android.hiphopstreet.modelclass.Uploadmusiclistmodelclass;
import com.goigi.android.hiphopstreet.modelclass.Uploadmusicmodel;
import com.squareup.picasso.Picasso;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Uploadmusiclistadapter extends RecyclerView.Adapter<Uploadmusiclistadapter.MyViewHolder> {

    private ArrayList<Uploadmusiclistmodelclass> dataSet;
   // private ArrayList<String> musicid;
    Context ctx;
    private String fileName,Imageview ;
    ImageView  moreoption,img;
    
        
	private ProgressDialog pDialog;
   
	public static final int progress_bar_type = 0;
	String Path;
	String MUSIC_ID;

    
    public static class MyViewHolder extends RecyclerView.ViewHolder {

    	ImageView songimage;
        TextView songname_tv ;
        TextView songcategory_tv;
        TextView songfile_tv;
        TextView makefeatured;
        


            public MyViewHolder(View itemView) {
            super(itemView);
            songimage = (ImageView) itemView.findViewById(R.id.uploadimg);
            songname_tv = (TextView) itemView.findViewById(R.id.songnamee);
            songfile_tv=(TextView)itemView.findViewById(R.id.songfilee);
            makefeatured=(TextView)itemView.findViewById(R.id.makefeatured); 
            

            
         }
    }

    public Uploadmusiclistadapter(Context ctx,ArrayList<Uploadmusiclistmodelclass> data) {
    	this.ctx=ctx;
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.uploaded_music_item, parent, false);
      
       
     

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

    	
    	
    	holder.songname_tv.setText(dataSet.get(listPosition).song_name);
    	holder.songfile_tv.setText(dataSet.get(listPosition).song_file);
    	img=holder.songimage;

        
        Imageview=dataSet.get(listPosition).song_image;
        
        if (!Imageview.equals("")) {
			try {
				//String apiLink = Link+Imgview;
				String apiLink = ctx. getResources().getString(R.string.image_base_url)+Imageview;

				String encodedurl = "";
				encodedurl = apiLink.substring(0,apiLink.lastIndexOf('/'))+ "/"+ Uri.encode(apiLink.substring(
						apiLink.lastIndexOf('/') + 1));
				Log.d("UserProfile Social List adapter", "encodedurl:"+encodedurl);
				if (!apiLink.equals("") && apiLink != null) {
		/*			Picasso.with(ctx)
					.load(encodedurl) // load: This path may be a remote URL,
					.placeholder(R.drawable.placeholderg)
					.error(R.drawable.placeholderg)
					.into(img); // Into: ImageView into which the final image has to be passed
					//.resize(130, 130)*/

					Picasso.get().load(encodedurl).into(img);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    
        holder.itemView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ctx, Mediaplayeractivitynew1.class);
				intent.putExtra("image",dataSet.get(listPosition).song_image);
				 intent.putExtra("id",dataSet.get(listPosition).songid);
				 intent.putExtra("name", dataSet.get(listPosition).song_name);
				 intent.putExtra("songfile",dataSet.get(listPosition).song_file);
				 //intent.putExtra("artist_id", dataSet.get(listPosition).artist_id);
				 //intent.putExtra("artist_name", dataSet.get(listPosition).artist_name);
				 ctx.startActivity(intent);
				 Toast.makeText(ctx, dataSet.get(listPosition).song_file, Toast.LENGTH_SHORT).show();	
			
			}
		});
    
    
    holder.makefeatured.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent i=new Intent(ctx,PayActivity.class);	
			i.putExtra("money", "44.95");
			i.putExtra("songid",dataSet.get(listPosition).songid );
			
			ctx.startActivity(i);	
		
		
		
		}
	});
    
    
    
    
    }    
        
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

	
}

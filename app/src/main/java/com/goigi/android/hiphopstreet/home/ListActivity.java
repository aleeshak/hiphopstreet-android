package com.goigi.android.hiphopstreet.home;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.SimpleDividerItemDecoration;
import com.goigi.android.hiphopstreet.adapter.SonglistAdapter;
import com.goigi.android.hiphopstreet.modelclass.Song;
import com.goigi.android.hiphopstreet.modelclass.TopRatedsong;
import com.goigi.android.hiphopstreet.sqlite.DBHelper;

import java.util.ArrayList;

public class ListActivity extends Activity implements OnClickListener{
	ImageView back_img;
	RecyclerView lv;

	public ArrayList<Song> songsList = new ArrayList<Song>();
	private ArrayList<TopRatedsong> dataSet;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.play_list);
		//getLayoutInflater().inflate(R.layout.play_list ,container);
		xmlinits();
		xmlonclicks();

	}

	     private void populateadapter() {
		SonglistAdapter adapter=new SonglistAdapter(ListActivity.this, songsList);
		lv.setAdapter(adapter);

	}

	     private void xmlinits() {
		// TODO Auto-generated method stub
		lv=(RecyclerView)findViewById(R.id.listView1);
		lv.setHasFixedSize(true);

		LinearLayoutManager layoutManager
		= new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
		lv.setLayoutManager(layoutManager);
		lv.addItemDecoration(new SimpleDividerItemDecoration(this));

		back_img=(ImageView)findViewById(R.id.back);

		DBHelper db=new DBHelper(this);
		//db.getAllMusic();
		this.songsList=db.getAllMusic();

		populateadapter();


	}

	private void xmlonclicks() {

		back_img.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.back:
				finish();

				break;

			default:
				break;
		}
	}



}

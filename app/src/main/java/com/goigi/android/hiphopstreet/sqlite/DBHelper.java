package com.goigi.android.hiphopstreet.sqlite;


import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.goigi.android.hiphopstreet.modelclass.Song;

public class DBHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "MyDBNamehiphopmusic.db";
	public static final String MUSIC_TABLE = "Hiphopmusicc";
	public static final String SONG_ID = "song_id";
	public static final String SONG_FILE="song_file";
	public static final String SONG_IMAGE="song_image";
	public static final String SONG_NAME = "song_name";
	public static final String SONG_URL="song_url";
	public static final String USER_ID="user_id";
	public static final String LIKE_COUNT="like_count";
	public static final String ARTIST_ID="artist_id";
	public static final String ARTIST_NAME="artist_name";
	public static final String ARTIST_IMAGE="artist_image";




	public DBHelper(Context context)
	{
		super(context, DATABASE_NAME , null, 1);
	}

	public ArrayList<Song> getAllMusic()
	{
		ArrayList<Song> array_list = new ArrayList<Song>();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor res =  db.rawQuery( "select * from Hiphopmusicc", null );

		try {
			while (res.moveToNext()) {

				Song song=new Song(res.getString(res.getColumnIndex(SONG_ID)),
						res.getString(res.getColumnIndex(SONG_NAME)),
						res.getString(res.getColumnIndex(SONG_IMAGE)),
						res.getString(res.getColumnIndex(USER_ID)),
						res.getString(res.getColumnIndex(SONG_URL)),
						res.getString(res.getColumnIndex(SONG_FILE)),
				        res.getString(res.getColumnIndex(LIKE_COUNT)),
						res.getString(res.getColumnIndex(ARTIST_ID)),
						res.getString(res.getColumnIndex(ARTIST_NAME)),
                        res.getString(res.getColumnIndex(ARTIST_IMAGE))


				);

				array_list.add(song);
			}
		} finally {
			res.close();
		}

		return array_list;
	}

	public Cursor getData(String music_id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor res =  db.rawQuery( "select * from Hiphopmusicc where id="+music_id, null );
		return res;
	}

	public boolean insertMUSIC_TABLE(String song_id,String song_name,
			String song_image,String user_id,
			String song_url,String song_file,
			String like_count,String artist_id,String artist_name,String artist_image

	)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("SONG_ID",     song_id);
		contentValues.put("SONG_NAME",   song_name);
		contentValues.put("SONG_IMAGE",  song_image);
		contentValues.put("USER_ID",      user_id);
		contentValues.put("SONG_URL",    song_url);
		contentValues.put("SONG_FILE",   song_file);
		contentValues.put("LIKE_COUNT",  like_count);
		contentValues.put("ARTIST_ID", artist_id);
        contentValues.put("ARTIST_NAME", artist_name);
		contentValues.put("ARTIST_IMAGE", artist_image);

		db.insert(MUSIC_TABLE, null, contentValues);
		return true;
	}



	public int numberOfRows(){
		SQLiteDatabase db = this.getReadableDatabase();
		int numRows = (int) DatabaseUtils.queryNumEntries(db, MUSIC_TABLE);
		return numRows;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		// creating table
		String query = "CREATE TABLE " + MUSIC_TABLE +
				" ( " +
				SONG_ID + " TEXT PRIMARY KEY ," +
				SONG_NAME + " TEXT," +
				SONG_IMAGE + " TEXT ,"+USER_ID+" TEXT,"+SONG_URL + " TEXT,"+SONG_FILE + " TEXT,"

               +LIKE_COUNT+ " TEXT, "+ARTIST_ID+" TEXT, "+ARTIST_NAME + " TEXT,"+ARTIST_IMAGE + " TEXT);";

		       db.execSQL(query);



	}



	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS Hiphopmusicc");
		onCreate(db);
	}
}
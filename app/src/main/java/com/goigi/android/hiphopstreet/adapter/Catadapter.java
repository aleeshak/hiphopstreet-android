package com.goigi.android.hiphopstreet.adapter;




import java.util.ArrayList;
import java.util.Random;

import org.json.JSONObject;


import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.modelclass.GenresModel;
import com.squareup.picasso.Picasso;


import customfonts.MyTextView;


import android.content.ClipData.Item;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Catadapter extends ArrayAdapter<GenresModel>{

	/*public ListAdapter(Context context, int resource) {
		super(context, resource);
		// TODO Auto-generated constructor stub
	}*/

	// Declare Variables
	private Context ctx;
	private int layoutId;
	private ArrayList<GenresModel> catDataList;
	
	
	String _Img;
	
	
	
	    
	public Catadapter(Context ctx, ArrayList<GenresModel> catDataList, int layoutId) {
		super(ctx,layoutId);
		this.ctx = ctx;
	
		this.catDataList = catDataList;
		
		this.layoutId = layoutId;
	
	}

	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return catDataList.size() ;
	}

	@Override
	public GenresModel getItem(int position) {
		// TODO Auto-generated method stub
		return catDataList.get(position);
	}

	/*@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}*/
	
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		//return super.getDropDownView(position, convertView, parent);
		
		
		TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.GRAY);
        label.setText(this.getItem(position).Genres_Name);
        return label;
	}
	

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View result = convertView;
		
		if (result == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			result = inflater.inflate(layoutId, parent, false);
		}
		
		//u_id=psh.getUserId();
		
		

		TextView title=(TextView)result.findViewById(R.id.cat_name);
		
	
		
		
		 title.setText(catDataList.get(position).Genres_Name);
		 
		
		
		
		 
		return result;
	
	}



	
  
	
}

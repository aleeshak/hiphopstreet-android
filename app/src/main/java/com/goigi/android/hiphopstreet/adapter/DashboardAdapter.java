package com.goigi.android.hiphopstreet.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;



import java.util.ArrayList;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.modelclass.TopReleases;
import com.squareup.picasso.Picasso;


public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {
    Context context;
    
       private ArrayList<TopReleases> data;
        public DashboardAdapter(Context context,ArrayList<TopReleases> data) {
        this.context = context;
        this.data=data;
           }

    @Override
    public DashboardAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
    	
    	View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dashboard_item_list, viewGroup, false);
    	
        
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DashboardAdapter.ViewHolder viewHolder, final int position) {
    	viewHolder.songtitle_tv.setText(data.get(position).song_name);
    	viewHolder.songfilename_tv.setText(data.get(position).song_file);
    	 ImageView imageView = viewHolder.image;
          String img=data.get(position).song_image;

    	
    	 //Picasso.with(context).load(data.get(position).song_image).into(viewHolder.image);
    	
    	if (!img.equals("")) {		         
	         try {
      	   String apiLink = context. getResources().getString(R.string.image_base_url)+img;
	         //String apiUrl = getResources().getString(R.string.app_base_url)+"login";
		       
	        	 
	        	 
	        	 
	        	 //Log.d("User Profile Image Parsing", "apiLink:"+apiLink);
		       String encodedurl = "";
		       encodedurl = apiLink.substring(0,apiLink.lastIndexOf('/'))+ "/"+ Uri.encode(apiLink.substring(
		       apiLink.lastIndexOf('/') + 1));
			   Log.d("UserProfile Social List adapter", "encodedurl:"+encodedurl);
				if (!apiLink.equals("") && apiLink != null) {
				   /* Picasso.with(context)
				   .load(encodedurl) // load: This path may be a remote URL,				  
				   .placeholder(R.drawable.ic_launcher)
				   .resize(240, 120)
				   .error(R.drawable.no_image)
				   .into(imageView); // Into: ImageView into which the final image has to be passed*/


                    Picasso.get().load(encodedurl).into(imageView);
                  //  Picasso.get().load(encodedurl).placeholder(R.drawable.default_pic).into(imageView);
                }

			 } catch (Exception e) {
				e.printStackTrace();
			 }
		 }
    	
    }
    	
    
    @Override
    public int getItemCount() {
        return data.size();
            
    	
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView songtitle_tv ;
        TextView songfilename_tv;
        private RecyclerView viewPager_body;

        public ViewHolder(View view) {
            super(view);

            image = (ImageView) view.findViewById(R.id.img);
            songtitle_tv = (TextView) view.findViewById(R.id.songname);
            songfilename_tv=(TextView)view.findViewById(R.id.songfile);
            
            
        }
    }

}

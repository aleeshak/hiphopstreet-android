package com.goigi.android.hiphopstreet;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.adapter.Artistsonglistadapter;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.MusiccategoryModel;
import com.goigi.android.hiphopstreet.modelclass.Uploadmusicmodel;
import com.goigi.android.hiphopstreet.network.GeneralAsynctask;
import com.goigi.android.hiphopstreet.shared.UserShared;
import com.squareup.picasso.Picasso;

import customfonts.MyTextView;

public class Artistprofilecategory extends AppCompatActivity implements OnClickListener{

	MyTextView Ar_name, Follow_count;
	TextView artist_biography, textViewUser, follow, songlist_tv;
	ConnectionDetector connection;
	MusiccategoryModel detailModel;
	Intent mIntent;
	ImageView pro_pic;
	ProgressDialog progressDialog;
	UserShared psh;
	RecyclerView recyclerView;
	private MultipartEntity reqEntity;
	public ArrayList<Uploadmusicmodel> UploadMusicArraylist;
	String User_Name, artistid, img_str_srt,
	apiLink,artist_id,artist_name,artist_image,bio_str,follow_Count_str;

	@Override


	protected void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.artist_profile);
		mIntent = getIntent();
		psh = new UserShared(this);
		connection = new ConnectionDetector(this);
		xmlinits();
	    xmlonclick();


	}

	private void xmlonclick() {
		artist_biography.setOnClickListener(this);
		follow.setOnClickListener(this);

	}

	    private void xmlinits() {
	    recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

	   // recyclerView.setHasFixedSize(true);
	   LinearLayoutManager layoutManager
			= new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
		recyclerView.setLayoutManager(layoutManager);

       Ar_name = (MyTextView) findViewById(R.id.artist_name);
	  artist_biography = (TextView) findViewById(R.id.artist_bio);
	  follow = (TextView) findViewById(R.id.artist_follow);
	  Follow_count = (MyTextView) findViewById(R.id.follow_count);
	  pro_pic = (ImageView) findViewById(R.id.artist_image);
	  songlist_tv = (TextView) findViewById(R.id.songlist);
	  Ar_name.setText(mIntent.getStringExtra("artistname"));



	  img_str_srt=mIntent.getStringExtra("img_str");

		if (!img_str_srt.equals("")) {
			try {
				//String apiLink = Link+Imgview;
				apiLink =getResources().getString(R.string.artist_image_url)+img_str_srt;
				Log.d("link", apiLink);
				String encodedurl = "";
				encodedurl = apiLink.substring(0,apiLink.lastIndexOf('/'))+ "/"+ Uri.encode(apiLink.substring(
						apiLink.lastIndexOf('/') + 1));
				Log.d("UserProfile adapter", "encodedurl:"+encodedurl);
				if (!apiLink.equals("") && apiLink != null) {
			/*		Picasso.with(Artistprofilecategory.this)
							.load(encodedurl) // load: This path may be a remote URL,
							.placeholder(R.drawable.placeholdergb)
							.error(R.drawable.placeholdergb)
							//.resize(1280, 720)
							.into(pro_pic); // Into: ImageView into which the final image has to be passed
					//.resize(130, 130);*/

					Picasso.get().load(encodedurl).into(pro_pic);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		  uploadmusicapi();



	}

	     private void uploadmusicapi() {
		GeneralAsynctask submitAsync = new GeneralAsynctask(
				this) {

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {

					JSONObject jobj=new JSONObject(result);

					String Ack = jobj.getString("status");
					if (Ack.equals("success"))  {
						//JSONArray jarray=jobj.getJSONArray("message");
						JSONObject jobj111=jobj.getJSONObject("message");
						artist_id=jobj111.getString("id");
						artist_name=jobj111.getString("name");
						artist_image=jobj111.getString("pic");
						bio_str= jobj111.getString("biography");
						follow_Count_str=jobj111.getString("followcount");

						UploadMusicArraylist=new ArrayList<Uploadmusicmodel>();
						JSONArray jarray=jobj111.getJSONArray("song");
						int post=jarray.length();
						songlist_tv.setText("Song"+"\n" +String.valueOf(post));

						for (int i = 0; i < jarray.length(); i++) {
							JSONObject jobj1=jarray.getJSONObject(i);
							Uploadmusicmodel item=new Uploadmusicmodel( artist_id,artist_name,artist_image, jobj1.getString("songid"),
									jobj1.getString("song_name"),jobj1.getString("song_category"),
									jobj1.getString("song_image"),jobj1.getString("song_file"),jobj1.getString("likecount"));
							UploadMusicArraylist.add(item);

						}

						setlist(result);
					}

					else {

						showToastLong("No Songs in This Week");
					}


				} catch (Exception e) {
					e.printStackTrace();
					// showToastLong("Error: Unable to Login.");
				} finally {
					pdspinnerGeneral.dismiss();
				}
			}

		};

		try {
			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {

				Log.d("LogIn", "Action: login");
				String url = getResources().getString(R.string.app_base_url)
						+ "user/"+mIntent.getStringExtra("artistid");
				String finalurl = url.replaceAll(" ", "%20");

				submitAsync.execute(finalurl);

			} else {
				showToastLong(getString(R.string.no_internet_message));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showToastLong(String string) {
	Toast.makeText(Artistprofilecategory.this,string,Toast.LENGTH_LONG).show();


	}


	    protected void setlist(String sr) {
		// TODO Auto-generated method stub
		Artistsonglistadapter dashboardAdapter = new Artistsonglistadapter(Artistprofilecategory.this,UploadMusicArraylist,sr);
		recyclerView.setAdapter(dashboardAdapter);
		Follow_count.setText("Singer,  "+follow_Count_str+" Followers");


	}

	@Override
	public void onClick(View v) {
	switch (v.getId()){

		case R.id.artist_follow:
			callfollow();
			break;

		case R.id.artist_bio:
		final Dialog dialog = new Dialog(Artistprofilecategory.this);
		dialog.setContentView(R.layout.dialog_biography);
		dialog.setTitle("Artist Biography");
		textViewUser = (TextView) dialog.findViewById(R.id.biography);
		textViewUser.setText(bio_str);

			ImageView cross=(ImageView)dialog.findViewById(R.id.close);
			dialog.show();
			cross.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();

				}
			});
			break;





	}


	}

	  private void callfollow() {
		try {

			reqEntity = null;
			reqEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			reqEntity.addPart("user_id", new StringBody(psh.getUserId()));
			reqEntity.addPart("follower_id", new StringBody(mIntent.getStringExtra("artistid")));

		}
		catch (Exception e) {
			e.printStackTrace();
		}


		Boolean isInternetPresent = connection.isConnectingToInternet();

		if (isInternetPresent) {
			progressDialog = ProgressDialog.show(this,
					"",
					getString(R.string.progress_bar_loading_message),
					false);

			UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
			editProfileAsyncTask.execute((Void) null);
		} else {
			showToastLong(getString(R.string.no_internet_message));
		}



	}

	public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
		String[] message;
		String responseString = "";

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {
				String apiUrl = getResources().getString(R.string.app_base_url) + "follow";
				Log.d("TestActivity", "Action: accountupdate");
				responseString = "";

				HttpParams params1 = new BasicHttpParams();
				params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
						HttpVersion.HTTP_1_1);
				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost postRequest = new HttpPost(apiUrl);
				//HttpPut postRequest = new HttpPut(apiUrl);
				postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");


				/*to print in log*/

				ByteArrayOutputStream bytes = new ByteArrayOutputStream();

				reqEntity.writeTo(bytes);

				String content = bytes.toString();

				Log.e("MultiPartEntityRequest:", content);

				/*to print in log*/

				postRequest.setEntity(reqEntity);
				HttpResponse response = httpClient.execute(postRequest);

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(
								response.getEntity().getContent(), "UTF-8"));

				String sResponse;

				StringBuilder s = new StringBuilder();

				while ((sResponse = reader.readLine()) != null) {
					s = s.append(sResponse);
				}

				responseString = s.toString();
				Log.d("TestActivity ResponseString =>", responseString);

			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			try {
				if (!responseString.equals("")) {

					JSONObject jsonObject = new JSONObject(responseString);
					String Ack = jsonObject.getString("status");
					String msg = jsonObject.getString("message");

					if (Ack.equals("success")) {
						progressDialog.dismiss();
						progressDialog.cancel();
						showToastLong(msg);

					} else {
						progressDialog.dismiss();
						progressDialog.cancel();
						showToastLong(msg);
					}
				} else {
					progressDialog.dismiss();
					progressDialog.cancel();
					showToastLong("Sorry! Problem cannot be recognized.");
				}
			} catch (Exception e) {
				progressDialog.dismiss();
				progressDialog.cancel();
				e.printStackTrace();
			}
		}


	}






}



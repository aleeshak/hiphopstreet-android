package com.goigi.android.hiphopstreet;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import com.goigi.android.hiphopstreet.UserPanel.UserDashBoard;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.home.HomepageActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity implements OnClickListener, OnCheckedChangeListener {

    TextView log_now;
    private ImageView back, userimage;
    private EditText name_edt, email, phone_number, password, confirm_password;
    private TextView submit;
    String Name, Email, PhoneNumber, Password, ConfirmPassword;
    private ProgressDialog progressDialog;
    ConnectionDetector connection;
    private static MultipartEntity reqEntity;
    private static final String TAG = SignupActivity.class.getSimpleName();
    private String ba1;
    RelativeLayout relativelayout;
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private Pattern pattern;
    private Matcher matcher;
    RadioButton radio_btn_female;
    RadioButton radio_btn_male;
    private RadioButton radioSexButton;
    private RadioGroup radioSexGroup;
    private String selectedType = "";
    Intent mIntent;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_upp);
        connection = new ConnectionDetector(this);
        mIntent = getIntent();
        prefs = getSharedPreferences("MY_SHARED_PREF", Context.MODE_PRIVATE);
        xmlinitialisation();
        xmloncick();


    }

    // change by me


    private void xmlinitialisation() {
        // TODO Auto-generated method stub

        pattern = Pattern.compile(EMAIL_PATTERN);

        log_now = (TextView) findViewById(R.id.log_now);

        email = (EditText) findViewById(R.id.email_edt);
        name_edt = (EditText) findViewById(R.id.name);
        password = (EditText) findViewById(R.id.password_edt);
        confirm_password = (EditText) findViewById(R.id.confirmpassword_edt);
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        radio_btn_male = (RadioButton) radioSexGroup.findViewById(R.id.radioMale);
        radio_btn_female = (RadioButton) radioSexGroup.findViewById(R.id.radioFemale);
        submit = (TextView) findViewById(R.id.register_btn_textview);


    }

    private void xmloncick() {
        // TODO Auto-generated method stub


        submit.setOnClickListener(this);
        log_now.setOnClickListener(this);

        radioSexGroup.setOnCheckedChangeListener(this);


    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.register_btn_textview:

                validation();

                //Toast.makeText(getApplicationContext(), "hiiii", Toast.LENGTH_LONG).show();


                break;
            case R.id.log_now:

                Intent i = new Intent(SignupActivity.this, SigninActivity.class);
                startActivity(i);
                break;

            default:
                break;
        }


    }


    private void validation() {
        // TODO Auto-generated method stub

        Name = name_edt.getText().toString();
        Email = email.getText().toString().trim();
        Password = password.getText().toString().trim();
        ConfirmPassword = confirm_password.getText().toString().trim();


        boolean cancel = false;
        String message = "";
        View focusView = null;
        boolean tempCond = false;


        if (TextUtils.isEmpty(Name)) {
            message = "Please enter your Name.";
            focusView = name_edt;
            cancel = true;
            tempCond = false;
        }


        if (TextUtils.isEmpty(Email)) {
            message = "Please enter your Email.";
            focusView = email;
            cancel = true;
            tempCond = false;
        } else if (!validatePattern(Email)) {
            message = "Invalid email id.";
            focusView = email;
            cancel = true;
            tempCond = false;
        }


        if (TextUtils.isEmpty(Password)) {
            message = "Please enter your Password";
            focusView = password;
            cancel = true;
            tempCond = false;
        }

        if (Password.length() < 8) {
            Toast.makeText(getApplicationContext(), "Type minimum 8 characters password", Toast.LENGTH_LONG).show();


        }

        if (TextUtils.isEmpty(ConfirmPassword)) {
            message = "Please enter your Confirm Password.";
            focusView = confirm_password;
            cancel = true;
            tempCond = false;
        }

        if (!Password.equals(ConfirmPassword)) {
            message = "Password Not Match.";
            focusView = password;
            cancel = true;
            tempCond = false;
        }


        if (cancel) {
            // focusView.requestFocus();
            if (!tempCond) {
                focusView.requestFocus();
            }
            showToastLong(message);
        } else {
            InputMethodManager imm = (InputMethodManager) this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            try {

                reqEntity = null;
                reqEntity = new MultipartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE);


                reqEntity.addPart("name", new StringBody(Name));
                reqEntity.addPart("email_id", new StringBody(Email));
                reqEntity.addPart("password", new StringBody(Password));
                reqEntity.addPart("sex", new StringBody(selectedType));
                reqEntity.addPart("user_cat", new StringBody(mIntent.getStringExtra("key")));


            } catch (Exception e) {
                e.printStackTrace();
            }


            Boolean isInternetPresent = connection.isConnectingToInternet();

            if (isInternetPresent) {
                progressDialog = ProgressDialog.show(this,
                        "",
                        getString(R.string.progress_bar_loading_message),
                        false);

                UploadProfileAsyncTask editProfileAsyncTask = new UploadProfileAsyncTask();
                editProfileAsyncTask.execute((Void) null);
            } else {
                showToastLong(getString(R.string.no_internet_message));
            }
        }


    }

    private void showToastLong(String message) {
        // TODO Auto-generated method stub

    }


    public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
        String[] message;
        String responseString = "";

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                String apiUrl = getResources().getString(R.string.app_base_url) + "user";
                Log.d("TestActivity", "Action: accountupdate");
                responseString = "";

                HttpParams params1 = new BasicHttpParams();
                params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
                        HttpVersion.HTTP_1_1);
                HttpClient httpClient = new DefaultHttpClient(params1);
                HttpPost postRequest = new HttpPost(apiUrl);
                //HttpPut postRequest = new HttpPut(apiUrl);
                postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");

  				
  				
				/*to print in log*/

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();

                reqEntity.writeTo(bytes);

                String content = bytes.toString();

                Log.e("MultiPartEntityRequest:", content);

				/*to print in log*/

                postRequest.setEntity(reqEntity);
                HttpResponse response = httpClient.execute(postRequest);

                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(
                                response.getEntity().getContent(), "UTF-8"));

                String sResponse;

                StringBuilder s = new StringBuilder();

                while ((sResponse = reader.readLine()) != null) {
                    s = s.append(sResponse);
                }

                responseString = s.toString();
                Log.d("Testttttttttttt", responseString);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            try {
                if (!responseString.equals("")) {

                    JSONObject jsonObject = new JSONObject(responseString);
                    String Ack = jsonObject.getString("status");
                    if (Ack.equals("success")) {
                        showToastLong("Data inserted successfully.");
                        JSONObject jobj = jsonObject.getJSONObject("message");


                        if (jobj.getString("user_cat").equals("artist")) {

                            Editor editor = prefs.edit();
                            editor.putString(getString(R.string.shared_user_id), jobj.getString("id"));
                            editor.putString(getString(R.string.shared_user_name), jobj.getString("name"));
                            editor.putString(getString(R.string.shared_user_email), jobj.getString("email_id"));
                            editor.putString(getString(R.string.shared_user_type), jobj.getString("user_cat"));
                            editor.putBoolean(getString(R.string.shared_loggedin_status_artist), true);
                            editor.commit();
                            Intent i = new Intent(SignupActivity.this, DashBoardActivity.class);
                            startActivity(i);

                        } else {


                            Editor editor = prefs.edit();
                            editor.putString(getString(R.string.shared_user_id), jobj.getString("id"));
                            editor.putString(getString(R.string.shared_user_name), jobj.getString("name"));
                            editor.putString(getString(R.string.shared_user_email), jobj.getString("email_id"));
                            editor.putString(getString(R.string.shared_user_type), jobj.getString("user_cat"));
                            editor.putBoolean(getString(R.string.shared_loggedin_status), true);
                            editor.commit();
                            Intent i = new Intent(SignupActivity.this, UserDashBoard.class);
                            startActivity(i);
                        }


                    } else {
                        progressDialog.dismiss();
                        progressDialog.cancel();
                        showToastLong("");
                    }
                } else {
                    progressDialog.dismiss();
                    progressDialog.cancel();
                    showToastLong("Sorry! Problem cannot be recognized.");
                }
            } catch (Exception e) {
                progressDialog.dismiss();
                progressDialog.cancel();
                e.printStackTrace();
            }
        }


    }


    public boolean validatePattern(final String hex) {
        matcher = pattern.matcher(hex);
        boolean temp = matcher.matches();
        return temp;
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        // TODO Auto-generated method stub
        if (checkedId == R.id.radioMale) {
            selectedType = radio_btn_male.getText().toString();

        } else if (checkedId == R.id.radioFemale) {
            selectedType = radio_btn_female.getText().toString();


        }


    }


}

package com.goigi.android.hiphopstreet.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.home.MediaplayerPlaylist;
import com.goigi.android.hiphopstreet.modelclass.Song;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SonglistAdapter extends RecyclerView.Adapter<SonglistAdapter.MyViewHolder> {

	public static class MyViewHolder extends RecyclerView.ViewHolder {

		TextView songcategory_tv;
		TextView songfile_tv;
		ImageView songimage;
		TextView songname_tv ;
		public MyViewHolder(View itemView) {
			super(itemView);
			songimage = (ImageView) itemView.findViewById(R.id.uploadimg);
			songname_tv = (TextView) itemView.findViewById(R.id.songnamee);

		}
	}

	Context ctx;
	private final ArrayList<Song> dataSet;

	ImageView  moreoption,img;
	String MUSIC_ID;
	String Path;

	ConnectionDetector connection;
	private ProgressDialog pDialog;
	private String Imgview;
	public SonglistAdapter(Context ctx,ArrayList<Song> data) {
		this.ctx=ctx;
		this.dataSet = data;

	}

	@Override
	public int getItemCount() {
		return dataSet.size();
	}

	@Override
	public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {




		//ImageView imageView = viewHolder.imgflag;
		//holder.songimage=dataSet1.get(listPosition).song_image;
		String apiLink;
		Imgview=dataSet.get(listPosition).SONG_IMAGE;
		// mydb = new DBHelper(context);
		connection=new ConnectionDetector(holder.songimage.getContext());
		//musicid=mydb.getAllMusic();
		if (!Imgview.equals("")) {
			try {
				//String apiLink = Link+Imgview;
				apiLink = holder.songimage.getResources().getString(R.string.image_base_url) + Imgview;
				Log.d("link", apiLink);
				String encodedurl = "";
				encodedurl = apiLink.substring(0, apiLink.lastIndexOf('/')) + "/" + Uri.encode(apiLink.substring(
						apiLink.lastIndexOf('/') + 1));
				Log.d("UserProfile adapter", "encodedurl:" + encodedurl);
				if (!apiLink.equals("") && apiLink != null) {
				/*	Picasso.with(holder.songimage.getContext())
							.load(encodedurl) // load: This path may be a remote URL,
							.placeholder(R.drawable.placeholdergb)
							.error(R.drawable.placeholdergb)
							//.resize(1280, 720)
							.into(holder.songimage); // Into: ImageView into which the final image has to be passed*/
					//.resize(130, 130);

					Picasso.get().load(encodedurl).into(holder.songimage);

					//Picasso.get().load(encodedurl).placeholder(R.drawable.default_pic).into(holder.songimage);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}



		holder.songname_tv.setText(dataSet.get(listPosition).SONG_NAME);
		//holder.songfile_tv.setText(dataSet.get(listPosition).SONG_URL);
		//img=holder.songimage;


		//imageview_str=dataSet.get(listPosition).SONG_IMAGE;

		/*if (!imageview_str.equals("")) {
			try {
				//String apiLink = Link+Imgview;
				String apiLink = ctx. getResources().getString(R.string.image_base_url)+imageview_str;

				String encodedurl = "";
				encodedurl = apiLink.substring(0,apiLink.lastIndexOf('/'))+ "/"+ Uri.encode(apiLink.substring(
						apiLink.lastIndexOf('/') + 1));
				Log.d("UserProfile Social List adapter", "encodedurl:"+encodedurl);
				if (!apiLink.equals("") && apiLink != null) {
					Picasso.with(ctx)
					.load(encodedurl) // load: This path may be a remote URL,
					.placeholder(R.drawable.placeholderg)
					.error(R.drawable.placeholderg)
					//.resize(130, 130)

					.into(img); // Into: ImageView into which the final image has to be passed

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
*/
		       holder.itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				/*if(MediaplayerActivity_search.active ==true){
					MediaplayerActivity_search.
				}*/
				Intent intent = new Intent(ctx, MediaplayerPlaylist.class);
				intent.putExtra("Branddetails",dataSet.get(listPosition));
				intent.putExtra("image",dataSet.get(listPosition).SONG_IMAGE);
				intent.putExtra("id",dataSet.get(listPosition).SONG_ID);
				intent.putExtra("name", dataSet.get(listPosition).SONG_NAME);
				intent.putExtra("songfile",dataSet.get(listPosition).SONG_FILE);
				intent.putExtra("likecount",dataSet.get(listPosition).LIKE_COUNT);
				intent.putExtra("artistname",dataSet.get(listPosition).ARTIST_NAME);
				intent.putExtra("artist_id", dataSet.get(listPosition).ARTIST_ID);
				intent.putExtra("artist_image", dataSet.get(listPosition).ARTIST_IMAGE);
				intent.putExtra("allsongs", dataSet);
				intent.putExtra("position",String.valueOf(listPosition));
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("allsongs", dataSet);


				ctx.startActivity(intent);





			}
		});





	}


	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent,
			int viewType) {
		final View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.playlist_item, parent, false);



		MyViewHolder myViewHolder = new MyViewHolder(view);
		return myViewHolder;
	}




}

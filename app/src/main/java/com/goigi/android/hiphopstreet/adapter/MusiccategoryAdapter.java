package com.goigi.android.hiphopstreet.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.home.Mediaplayeractivitynew;
import com.goigi.android.hiphopstreet.modelclass.MusiccategoryModel;
import com.squareup.picasso.Picasso;

import customfonts.MyTextView;

public class MusiccategoryAdapter extends RecyclerView.Adapter<MusiccategoryAdapter.MyViewHolder> {

	// inner class to hold a reference to each item of RecyclerView
	public static class MyViewHolder extends RecyclerView.ViewHolder {

		public ImageView imgflag;
		public MyTextView song_name,artistname,song_file;

		public MyViewHolder(View itemLayoutView) {
			super(itemLayoutView);
			imgflag = (ImageView) itemLayoutView.findViewById(R.id.imgevw);
			song_name=(MyTextView)itemLayoutView.findViewById(R.id.musicname);
			artistname=(MyTextView)itemLayoutView.findViewById(R.id.musicby);
		}
	}
	String apiLink;
	private final Context context;

	private final ArrayList<MusiccategoryModel> dataSet1;
	ImageView imageView;
	private String Imgview;
	private LayoutInflater layoutInflater;
	private ArrayList<String> musicid;
	String song_id;

	String songname_str,songfile_str;

	public MusiccategoryAdapter(Context context,ArrayList<MusiccategoryModel> dataSet1) {
		this.context = context;
		this.dataSet1=dataSet1;


	}

	// Return the size of your itemsData (invoked by the layout manager)
	@Override
	public int getItemCount() {
		return dataSet1.size();
	}

	@Override
	public void onBindViewHolder(MyViewHolder viewHolder, final int position) {



		imageView = viewHolder.imgflag;
		Imgview=dataSet1.get(position).song_image;
		if (!Imgview.equals("")) {
			try {
				//String apiLink = Link+Imgview;
				apiLink = context.getResources().getString(R.string.image_base_url)+Imgview;
				Log.d("apilink",apiLink);
				String encodedurl = "";
				encodedurl = apiLink.substring(0,apiLink.lastIndexOf('/'))+ "/"+ Uri.encode(apiLink.substring(
						apiLink.lastIndexOf('/') + 1));
				Log.d("UserProfile Social List adapter", "encodedurl:"+encodedurl);
				if (!apiLink.equals("") && apiLink != null) {
				/*	Picasso.with(context)
					.load(encodedurl) // load: This path may be a remote URL,
					.placeholder(R.drawable.placeholdergb)
					.error(R.drawable.placeholdergb)
					.resize(130, 130)

					.into(imageView); // Into: ImageView into which the final image has to be passed*/

					Picasso.get().load(encodedurl).into(viewHolder.imgflag);

				//	Picasso.get().load(encodedurl).placeholder(R.drawable.default_pic).into(imageView);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		final TextView name=viewHolder.song_name;
		name.setText(dataSet1.get(position).song_name);
		song_id=dataSet1.get(position).songid;
		songname_str=dataSet1.get(position).song_name;
		songfile_str=dataSet1.get(position).song_file;

		TextView artistname=viewHolder.artistname;
		artistname.setText(dataSet1.get(position).name);



		viewHolder.imgflag.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i= new Intent(context,Mediaplayeractivitynew.class);
				i.putExtra("songfile", dataSet1.get(position).song_file);
				i.putExtra("image", dataSet1.get(position).song_image);
				i.putExtra("id", dataSet1.get(position).songid);
				i.putExtra("name", dataSet1.get(position).song_name);
				i.putExtra("artist_id", dataSet1.get(position).id);
				i.putExtra("artist_name", dataSet1.get(position).name);



				context.startActivity(i);



			}
		});



	}

	// Create new views (invoked by the layout manager)
	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		// create a new view
		View itemLayoutView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.music_list_item, null);

		// create ViewHolder

		MyViewHolder viewHolder = new MyViewHolder(itemLayoutView);
		return viewHolder;
	}


}

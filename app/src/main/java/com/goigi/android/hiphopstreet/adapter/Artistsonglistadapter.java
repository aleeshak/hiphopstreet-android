package com.goigi.android.hiphopstreet.adapter;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.home.Mediaplayeractivitynew3;
import com.goigi.android.hiphopstreet.modelclass.Uploadmusicmodel;
import com.squareup.picasso.Picasso;

public class Artistsonglistadapter extends RecyclerView.Adapter<Artistsonglistadapter.MyViewHolder> {
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView makefeatured;
        TextView song_biography;
        TextView songcategory_tv;
        TextView songfile_tv;
        ImageView songimage;
        TextView songname_tv;


        public MyViewHolder(View itemView) {
            super(itemView);
            songimage = (ImageView) itemView.findViewById(R.id.uploadimg);
            songname_tv = (TextView) itemView.findViewById(R.id.songnamee);
            songfile_tv = (TextView) itemView.findViewById(R.id.songfilee);

            makefeatured = (TextView) itemView.findViewById(R.id.makefeatured);
            //song_biography=(TextView)itemView.findViewById(R.id.biography);


        }
    }

    public static final int progress_bar_type = 0;
    // private ArrayList<String> musicid;
    Context ctx;
    private final ArrayList<Uploadmusicmodel> dataSet;
    private String fileName, Imageview;
    ImageView moreoption, img;
    String MUSIC_ID, arr;
    String Path;


    private ProgressDialog pDialog;

    public Artistsonglistadapter(Context ctx, ArrayList<Uploadmusicmodel> data, String arr) {
        this.ctx = ctx;
        this.dataSet = data;
        this.arr = arr;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

       //holder.song_biography.setText(dataSet.get(listPosition).biography);
        holder.songname_tv.setText(dataSet.get(listPosition).song_name);
        holder.songfile_tv.setText(dataSet.get(listPosition).song_file);
        holder.songfile_tv.setVisibility(View.GONE);
        img = holder.songimage;
        Imageview = dataSet.get(listPosition).song_image;

        if (!Imageview.equals("")) {
            try {
                //String apiLink = Link+Imgview;
                String apiLink = ctx.getResources().getString(R.string.image_base_url) + Imageview;

                String encodedurl = "";
                encodedurl = apiLink.substring(0, apiLink.lastIndexOf('/')) + "/" + Uri.encode(apiLink.substring(
                        apiLink.lastIndexOf('/') + 1));
                Log.d("UserProfile Social List adapter", "encodedurl:" + encodedurl);
                if (!apiLink.equals("") && apiLink != null) {
  /*                  Picasso.with(ctx)
                            .load(encodedurl) // load: This path may be a remote URL,
                            .placeholder(R.drawable.placeholderg)
                            .error(R.drawable.placeholderg)
                            .into(img); // Into: ImageView into which the final image has to be passed*/
                    //.resize(130, 130)

                   Picasso.get().load(encodedurl).into(img);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

               holder.itemView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(ctx, Mediaplayeractivitynew3.class);
                intent.putExtra("BrandDetails", dataSet.get(listPosition));
				intent.putExtra("image",dataSet.get(listPosition).song_image);
                intent.putExtra("id",dataSet.get(listPosition).songid);
				intent.putExtra("name", dataSet.get(listPosition).song_name);
				intent.putExtra("songfile",dataSet.get(listPosition).song_file);
				intent.putExtra("artist_id", dataSet.get(listPosition).id);
				intent.putExtra("artist_name", dataSet.get(listPosition).name);
				intent.putExtra("artist_image", dataSet.get(listPosition).pic);
                intent.putExtra("likecount", dataSet.get(listPosition).likecount);

			//intent.putExtra("fullarray", arr);
                intent.putExtra("allsong", dataSet);
                intent.putExtra("position", String.valueOf(listPosition));

				ctx.startActivity(intent);
                Toast.makeText(ctx, dataSet.get(listPosition).song_file, Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.artist_music_item, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }


}

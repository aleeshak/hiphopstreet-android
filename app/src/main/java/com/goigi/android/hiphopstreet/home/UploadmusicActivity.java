package com.goigi.android.hiphopstreet.home;


import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.goigi.android.hiphopstreet.ApplicationSingleton;
import com.goigi.android.hiphopstreet.DashBoardActivity;
import com.goigi.android.hiphopstreet.MainActivity;
import com.goigi.android.hiphopstreet.R;
import com.goigi.android.hiphopstreet.adapter.Catadapter;
import com.goigi.android.hiphopstreet.connection.ConnectionDetector;
import com.goigi.android.hiphopstreet.modelclass.GenresModel;
import com.goigi.android.hiphopstreet.shared.UserShared;
public class UploadmusicActivity extends MainActivity implements OnClickListener {
	public class UploadProfileAsyncTask extends AsyncTask<Void, Void, Boolean> {
		String[] message;
		String responseString = "";

		@Override
		protected Boolean doInBackground(Void... params) {

			try {
				String apiUrl = getResources().getString(R.string.app_base_url)+"song";
				Log.d("TestActivity", "Action: accountupdate");
				responseString = "";

				HttpParams params1 = new BasicHttpParams();
				params1.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
						HttpVersion.HTTP_1_1);
				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost postRequest = new HttpPost(apiUrl);
				postRequest.setHeader("x-api-key", "25d55ad283aa400af464c76d713c07ad");
				/*to print in log*/

				ByteArrayOutputStream bytes = new ByteArrayOutputStream();

				reqEntity.writeTo(bytes);

				String content = bytes.toString();

				Log.e("MultiPartEntityRequest:",content);

				/*to print in log*/

				postRequest.setEntity(reqEntity);
				HttpResponse response = httpClient.execute(postRequest);

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(
								response.getEntity().getContent(), "UTF-8"));

				String sResponse;

				StringBuilder s = new StringBuilder();

				while ((sResponse = reader.readLine()) != null) {
					s = s.append(sResponse);
				}

				responseString = s.toString();
				Log.d("TestActivity ResponseString =>", responseString);

			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			try {
				if (!responseString.equals("")) {

					JSONObject jsonObject = new JSONObject(responseString);
					String Ack = jsonObject.getString("status");
					//String message = jsonObject.getString("Message");
					//showAlertMessage(message, "Yoozia");

					if (Ack.equals("success")) {

						progressDialog.dismiss();
						progressDialog.cancel();
						showToastLong("Music uploaded sucessfully");

						Intent i=new Intent(UploadmusicActivity.this,DashBoardActivity.class);
						startActivity(i);

					}
					else {
						progressDialog.dismiss();
						progressDialog.cancel();
						//showToastLong(message);
					}
				}
				else {
					progressDialog.dismiss();
					progressDialog.cancel();
					showToastLong("Sorry! Problem cannot be recognized.");
				}
			} catch (Exception e) {
				progressDialog.dismiss();
				progressDialog.cancel();
				e.printStackTrace();
			}
		}

	}
	private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int RequestPermissionCode = 1;
	private static final int SELECT_AUDIO = 1;
	private static final String TAG = UploadmusicActivity.class.getSimpleName();
	public static String getDataColumn(Context context, Uri uri,
			String selection, String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = { column };

		try {
			cursor = context.getContentResolver().query(uri, projection,
					selection, selectionArgs, null);
			if (cursor != null && cursor.moveToFirst()) {
				final int index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}
	/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type){

		File mediaStorageDir = new File(
				Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				com.goigi.android.hiphopstreet.Config.IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(TAG, "Oops! Failed create "
						+ com.goigi.android.hiphopstreet.Config.IMAGE_DIRECTORY_NAME + " directory");


				return null;
			}

		}
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".png");
		} /*else if (type == MEDIA_TYPE_VIDEO) {
					mediaFile = new File(mediaStorageDir.getPath() + File.separator
							+ "VID_" + timeStamp + ".mp4");
				}*/ else {
					return null;
				}

		return mediaFile;
	}
	/** Create a file Uri for saving an image or video */
	private static Uri getOutputMediaFileUri(int type){
		return Uri.fromFile(getOutputMediaFile(type));
	}
	public static String getPath(final Context context, final Uri uri) {

		// check here to KITKAT or new version
		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

		// DocumentProvider
		if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

			// ExternalStorageProvider
			if (isExternalStorageDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				if ("primary".equalsIgnoreCase(type)) {
					return Environment.getExternalStorageDirectory() + "/"
							+ split[1];
				}
			}
			// DownloadsProvider
			else if (isDownloadsDocument(uri)) {

				final String id = DocumentsContract.getDocumentId(uri);
				final Uri contentUri = ContentUris.withAppendedId(
						Uri.parse("content://downloads/public_downloads"),
						Long.valueOf(id));

				return getDataColumn(context, contentUri, null, null);
			}
			// MediaProvider
			else if (isMediaDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				Uri contentUri = null;
				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				} else if ("video".equals(type)) {
					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				} else if ("audio".equals(type)) {
					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				}

				final String selection = "_id=?";
				final String[] selectionArgs = new String[] { split[1] };

				return getDataColumn(context, contentUri, selection,
						selectionArgs);
			}
		}
		// MediaStore (and general)
		else if ("content".equalsIgnoreCase(uri.getScheme())) {

			// Return the remote address
			if (isGooglePhotosUri(uri))
				return uri.getLastPathSegment();

			return getDataColumn(context, uri, null, null);
		}
		// File
		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}
	public static String getPathForAudio(Context context, Uri uri)
	{
		String result = null;
		Cursor cursor = null;

		try {
			String[] proj = { MediaStore.Audio.Media.DATA };
			cursor = context.getContentResolver().query(uri, proj, null, null, null);
			if (cursor == null) {
				result = uri.getPath();
			} else {
				cursor.moveToFirst();
				int column_index = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
				result = cursor.getString(column_index);
				cursor.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return result;
	}
	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri
				.getAuthority());
	}
	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri
				.getAuthority());
	}
	public static boolean isGooglePhotosUri(Uri uri) {
		return "com.google.android.apps.photos.content".equals(uri
				.getAuthority());
	}
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri
				.getAuthority());
	}
	Catadapter adapter;
	ImageView addrecord_img,add_img,add_image,category_img;
	String AudioSavePathInDevice = null;
	String audioString="";
	private	String  ba1;
	final int CAMERA_CAPTURE = 100;
	Boolean cameraboolean = false,cropboolean = false;
	private ArrayList<GenresModel> catitem;
	CheckBox check_box;

	ConnectionDetector connection;
	final int CROP_FROM_CAMERA=2;
	ArrayList<String> difficultyLevelOptionsList = new ArrayList<String>();
	Uri fileUri;
	final int GALLARY_CAPTURE = 3;
	final Handler handler = new Handler();
	LinearLayout linearlayout;
	LinearLayout ll;
	MediaPlayer mediaPlayer ;
	MediaRecorder mediaRecorder ;
	int minute =0, seconds = 0, hour = 0;
	TimerTask mTimerTask;
	private Bitmap photo = null;
	String picturePath=null;
	private View popUpView;
	private ProgressDialog progressDialog;

	UserShared psh;
	Random random ;
	String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
	RelativeLayout relativelayout;
	private MultipartEntity reqEntity;




	String selectedPath = "";




	EditText songname_edt;




	String songname_str,selected;




	Spinner spinner;




	final Timer t = new Timer("hello", true);


	CountDownTimer t1;




	public TextView timer,upload_tv;

	TextView upload_tv_,desclaimer_tv;




	TextView uploadmusic_tv;


	String user_id;

	public boolean checkPermission() {

		int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
		int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);

		return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
	}


	public String CreateRandomAudioFileName(int string){

		StringBuilder stringBuilder = new StringBuilder( string );

		int i = 0 ;
		while(i < string ) {

			stringBuilder.append(RandomAudioFileName.charAt(random.nextInt(RandomAudioFileName.length())));

			i++ ;
		}
		return stringBuilder.toString();

	}


	private String getpathring(Uri path) {
		String result;
		String mp3_name;
		Cursor cursor = getContentResolver()
				.query(path, null, null, null, null);
		if (cursor == null) {
			result = path.getPath();
			int name = result.lastIndexOf("/");
			mp3_name = result.substring(name + 1);
		} else {
			cursor.moveToFirst();
			int idx = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);

			result = cursor.getString(idx);
			cursor.close();
			int name = result.lastIndexOf("/");
			mp3_name = result.substring(name + 1);
		}
		return result;
	}



	private String getRealPathFromURI(Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
		Cursor cursor = loader.loadInBackground();
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		String result = cursor.getString(column_index);
		cursor.close();
		return result;
	}


	public void MediaRecorderReady(){

		mediaRecorder=new MediaRecorder();

		mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

		mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

		mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);

		mediaRecorder.setOutputFile(AudioSavePathInDevice);


	}

	/*private String getPaths(Uri uri) {
	// TODO Auto-generated method stub
	String[] projection = { MediaStore.Images.Media.DATA };
    Cursor cursor = managedQuery(uri, projection, null, null, null);
    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
    cursor.moveToFirst();
    return cursor.getString(column_index);

}*/

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		//super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == CAMERA_CAPTURE) {
			if (resultCode == RESULT_OK) {


				picturePath=fileUri.getPath();

				BitmapFactory.Options options = new BitmapFactory.Options();


				options.inSampleSize = 8;

				photo = BitmapFactory.decodeFile(picturePath, options);

				add_image.setImageBitmap(photo);

			}
			else if (resultCode == RESULT_CANCELED) {

				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			}
			else{

				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();

			}



		}else if (requestCode == GALLARY_CAPTURE) {
			if (resultCode == RESULT_OK) {

				// video successfully recorded
				// launching upload activity
				//launchUploadActivity(false);
				Log.v("data", data.toString());

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Log.v("selectedImage", selectedImage.toString());
				Log.v("filePathColumn", filePathColumn.toString());

				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);

				if(cursor!=null) {
					cursor.moveToFirst();

					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					picturePath = cursor.getString(columnIndex);
					cursor.close();
				}else{
					picturePath = selectedImage.toString();
				}

				ExifInterface exif = null;
				try {
					exif = new ExifInterface(picturePath);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
				Log.v("----exifOrientation------", exifOrientation);
				Log.d("filepath", picturePath);
				photo = BitmapFactory.decodeFile(picturePath);

				cameraboolean = false;
				//}
				add_image.setImageBitmap(photo);

			} else if (resultCode == RESULT_CANCELED) {

				// user cancelled recording
				Toast.makeText(getApplicationContext(),
						"Cancel By You", Toast.LENGTH_SHORT)
						.show();

			} else {
				// failed to record video
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to Get Picture", Toast.LENGTH_SHORT)
						.show();
			}


		}

		/* else if (requestCode == SELECT_AUDIO) {
         if (resultCode == RESULT_OK) {

        	 //System.out.println("SELECT_AUDIO");
             Uri selectedImageUri = data.getData();
             AudioSavePathInDevice=getPath(this,selectedImageUri);
            // AudioSavePathInDevice=selectedImageUri.getPath().
            // AudioSavePathInDevice=getPathForAudio(this,selectedImageUri);
             AudioSavePathInDevice=getRealPathFromURI(this,selectedImageUri);

              //AudioSavePathInDevice=getpathring(selectedImageUri);
            // AudioSavePathInDevice=selectedImageUri.getPath();
            // AudioSavePathInDevice = getPaths(selectedImageUri);
             System.out.println("SELECT_AUDIO Path : " + AudioSavePathInDevice);

         } else if (resultCode == RESULT_CANCELED) {

         	// user cancelled recording
             Toast.makeText(getApplicationContext(),
                     "Cancel By You", Toast.LENGTH_SHORT)
                     .show();

         } else {
             // failed to record video
             Toast.makeText(getApplicationContext(),
                     "Sorry! Failed to Get audio", Toast.LENGTH_SHORT)
                     .show();
         }


	 }		*/


		if (requestCode == 0011 && resultCode == Activity.RESULT_OK){
			if ((data != null) && (data.getData() != null)){
				Uri audioFileUri = data.getData();
				AudioSavePathInDevice =  getRealPathFromURI(audioFileUri) ;
				// AudioSavePathInDevice=audioFileUri.getPath();
				Log.e(TAG, "onActivityResult: 232"+AudioSavePathInDevice);
				Toast.makeText(UploadmusicActivity.this, AudioSavePathInDevice, Toast.LENGTH_SHORT).show();


			}
		}


	}//End of activity result




	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		/*case R.id.addrecord:

	audiorecordpopup();


	break*/

		case R.id.addimg:
			uploadimage();

			break;

		case R.id.upload:
			uploadvalidation();
			break;

		case R.id.uploadd:
			ll.setVisibility(View.VISIBLE);
			uploadpricelist();
			break;

		case R.id.uploadmusic:
			uploadmusicdevice();
			ll.setVisibility(View.VISIBLE);
			break;

		case R.id.trm_txt:
			termsandcondition();
			break;

		default:
			break;
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.upload_video);
		getLayoutInflater().inflate(R.layout.upload_video, container);
		psh=new UserShared(this);
		user_id=psh.getUserId();
		connection=new ConnectionDetector(this);
		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		((ApplicationSingleton)getApplicationContext()).setImageUri(fileUri);
		xmlinit();
		xmlonclik();

	}

	@SuppressLint("Override") @Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
		case RequestPermissionCode:
			if (grantResults.length > 0) {

				boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
				boolean RecordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

				if (StoragePermission && RecordPermission) {

					Toast.makeText(UploadmusicActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
				}
				else {
					Toast.makeText(UploadmusicActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();

				}
			}

			break;
		}
	}

	private void populatespinner() {
		// TODO Auto-generated method stub

		catitem=new ArrayList<GenresModel>();

		GenresModel item=new GenresModel("0", "Club");
		catitem.add(item);
		GenresModel item1=new GenresModel("1", "Conscious");
		catitem.add(item1);
		GenresModel item2=new GenresModel("2", "Freestyle");
		catitem.add(item2);
		GenresModel item3=new GenresModel("3", "Gangsta");
		catitem.add(item3);
		GenresModel item4=new GenresModel("4", "R&B");
		catitem.add(item4);
		GenresModel item5=new GenresModel("5", "Trap");
		catitem.add(item5);
		GenresModel item6=new GenresModel("6", "Tribute");
		catitem.add(item6);
		adapter=new Catadapter(this, catitem, R.layout.cat_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				//item_sp = parent.getItemAtPosition(position).toString();
				//Log.d("tag", item_sp);
				selected=catitem.get(position).Genres_Name;
				Log.d("tag", selected);

				//Catid=item_sp;
				//callmainsearchAsyn();


			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {


			}
		});

	}

	private void requestPermission() {

		ActivityCompat.requestPermissions(UploadmusicActivity.this, new String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);

	}







	private void showAlertMessage(String message, String string) {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG);

	}

	private void showToastLong(String string) {
		// TODO Auto-generated method stub

	}


	private void termsandcondition() {
		final Dialog dialog = new Dialog(UploadmusicActivity.this);
		dialog.setContentView(R.layout.dialog_layout);
		dialog.setTitle("Terms and Condition");
		TextView textViewUser = (TextView) dialog.findViewById(R.id.textBrand);
		ImageView cross=(ImageView)dialog.findViewById(R.id.close);

		String formattedText = getResources().getString(R.string.terms);
		//Spanned result = Html.fromHtml(formattedText);
		//String ss=txtToHtml(Html.fromHtml(formattedText));
		//textViewUser.setText(Html.fromHtml(formattedText));
		dialog.show();
		cross.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});


	}

	private void uploadimage() {
		// TODO Auto-generated method stub
		Context mContext = UploadmusicActivity.this;
		final Dialog d = new Dialog(mContext);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);

		Button b,camera,gallary;

		d.setContentView(R.layout.dialog_cameragallary);
		b=(Button)d.findViewById(R.id.button1);
		camera=(Button)d.findViewById(R.id.camera_bt_image);
		gallary=(Button)d.findViewById(R.id.gallary_bt_image);
		final Window window = d.getWindow();
		//    window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
		window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		d.show();

		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				d.dismiss();
			}
		});

		camera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

					captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
					startActivityForResult(captureIntent, CAMERA_CAPTURE);
					cameraboolean = true;
					cropboolean = true;

				}catch (ActivityNotFoundException anfe) {

					// display an error message
					String errorMessage = "oops! your device doesn't support capturing images!";

					Toast.makeText(getApplicationContext(), errorMessage,
							Toast.LENGTH_SHORT).show();

				}

				d.dismiss();
			}
		});
		gallary.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

				startActivityForResult(i, GALLARY_CAPTURE);
				d.dismiss();
			}
		});



	}// end of uploadimage


	private void uploadmusicdevice() {
		// TODO Auto-generated method stub
		/*Intent intent = new Intent();
    intent.setType("audio/*");
    intent.setAction(Intent.ACTION_GET_CONTENT);
    startActivityForResult(Intent.createChooser(intent,"Select Audio "), SELECT_AUDIO);
		 */

		Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(i,0011);



	}

	private void uploadpricelist() {


		final PopupWindow mpopup;
		popUpView = getLayoutInflater().inflate(R.layout.popup_price_list, null); // inflating popup layout
		mpopup = new PopupWindow(popUpView, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, true); //Creation of popup
		mpopup.showAtLocation(popUpView, Gravity.BOTTOM, 0, 0);


		LinearLayout facebook=(LinearLayout)popUpView.findViewById(R.id.fb);

		facebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(UploadmusicActivity.this,WalletActivity.class);
				i.putExtra("money", "5.99");
				i.putExtra("track", "5");
				startActivity(i);


			}
		});



		LinearLayout message=(LinearLayout)popUpView.findViewById(R.id.msg);

		message.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(UploadmusicActivity.this,WalletActivity.class);
				i.putExtra("money", "4.99");
				i.putExtra("track", "3");

				startActivity(i);

			}
		});

		LinearLayout mail=(LinearLayout)popUpView.findViewById(R.id.mail);

		mail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i=new Intent(UploadmusicActivity.this,WalletActivity.class);
				i.putExtra("money", "2.99");
				i.putExtra("track", "1");

				startActivity(i);

			}
		});


		//LinearLayout twitter=(LinearLayout)popUpView.findViewById(R.id.twitter);
		LinearLayout cancelpopup=(LinearLayout)popUpView.findViewById(R.id.cancel);

		cancelpopup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mpopup.dismiss();
				ll.setVisibility(View.GONE);

			}
		});



	}

	private void uploadvalidation() {
		// TODO Auto-generated method stub

		songname_str=(songname_edt).getText().toString();
		boolean cancel = false;
		String message = "";
		View focusView = null;
		boolean tempCond = false;

		if (TextUtils.isEmpty(songname_str)) {
			message = "Please enter your song name";
			focusView = songname_edt;
			cancel = true;
			tempCond = false;
		}

		if(!check_box.isChecked()) {
			message = "Please Agree to the terms and condition";
			focusView = check_box;
			cancel = true;
			tempCond = false;
		}

		if (cancel) {
			// focusView.requestFocus();
			if (!tempCond) {
				focusView.requestFocus();
			}
			showAlertMessage(message,"Upload song");
		}else{
			InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(songname_edt.getWindowToken(), 0);
			reqEntity = null;
			reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

			try{
				reqEntity.addPart("song_name", new StringBody(songname_str));
				reqEntity.addPart("song_category", new StringBody(selected));
				reqEntity.addPart("userid", new StringBody(user_id));
				File sourceFile1 = new File(AudioSavePathInDevice);
				reqEntity.addPart("song_file", new FileBody(sourceFile1));

				//reqEntity.addPart("song", new StringBody(AudioSavePathInDevice));
				File sourceFile = new File(picturePath);
				reqEntity.addPart("song_image", new FileBody(sourceFile));

			}
			catch(Exception e){
				e.printStackTrace();
			}

			Boolean isInternetPresent = connection.isConnectingToInternet();
			if (isInternetPresent) {
				progressDialog = ProgressDialog.show(this, "", getString(R.string.progress_bar_loading_message), false);
				/* Editprofileasyn editAsyncTask = new Editprofileasyn();
		 editAsyncTask.execute((Void) null);*/

				new UploadProfileAsyncTask().execute();
			}else{
				showToastLong(getString(R.string.no_internet_message));
			}
		}


	}///end of validation

	private void xmlinit() {
		// TODO Auto-generated method stub
		//addrecord_img=(ImageView)findViewById(R.id.addrecord);
		add_img=(ImageView)findViewById(R.id.addimg);
		add_image=(ImageView)findViewById(R.id.addimagee);
		//category_img=(ImageView)findViewById(R.id.category);
		spinner = (Spinner) findViewById(R.id.spinner1);
		//timer=(TextView)findViewById(R.id.timer);
		songname_edt=(EditText)findViewById(R.id.songedt);
		upload_tv=(TextView)findViewById(R.id.upload);
		//final TextView buttonStart = (TextView)findViewById(R.id.record);
		//final TextView buttonStop = (TextView)findViewById(R.id.stop);
		upload_tv_=(TextView)findViewById(R.id.uploadd);
		uploadmusic_tv=(TextView)findViewById(R.id.uploadmusic);
		ll=(LinearLayout)findViewById(R.id.linarlayout);
		desclaimer_tv=(TextView)findViewById(R.id.trm_txt);
		String htmlString="<u>I Agree To The Terms & Condition</u>";
		desclaimer_tv.setText(Html.fromHtml(htmlString));
		check_box=(CheckBox)findViewById(R.id.checkbox);

		//buttonPlay = (Button)d1. findViewById(R.id.format);
		//buttonStop.setEnabled(false);
		//buttonPlay.setEnabled(false);
		random = new Random();


		/* buttonStart.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View view) {
     buttonStart.setVisibility(View.GONE);

    	 t.schedule(new TimerTask() {

    	 @Override
    	 public void run() {
    	     timer.post(new Runnable() {

    	     public void run() {
    	         seconds++;
    	         if (seconds == 60) {
    	         seconds = 0;
    	         minute++;
    	         }
    	         if (minute == 60) {
    	         minute = 0;
    	         hour++;
    	         }
    	         timer.setText(""
    	             + (hour > 9 ? hour : ("0" + hour)) + " : "
    	             + (minute > 9 ? minute : ("0" + minute))
    	             + " : "
    	             + (seconds > 9 ? seconds : "0" + seconds));

    	     }
    	     });

    	     }
    	     }, 1000, 1000);


     	if(checkPermission()) {

             AudioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + CreateRandomAudioFileName(1) + "AudioRecording.mp3";

             MediaRecorderReady();

             try {
                 mediaRecorder.prepare();
                 mediaRecorder.start();
             } catch (IllegalStateException e) {
                 // TODO Auto-generated catch block
                 e.printStackTrace();
             } catch (IOException e) {
                 // TODO Auto-generated catch block
                 e.printStackTrace();
             }

             buttonStart.setEnabled(false);
             buttonStop.setEnabled(true);

             Toast.makeText(UploadmusicActivity.this, "Recording started", Toast.LENGTH_LONG).show();
         }
         else {

             requestPermission();

         }

     }



 });
		 */

		/*buttonStop.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View view) {
    	 //final Timer t = new Timer("hello", true);
    	  if (t != null)
             t.cancel();


    	 mediaRecorder.stop();

         buttonStop.setEnabled(false);
         buttonStart.setEnabled(true);
         //buttonPlay.setEnabled(false);

         Toast.makeText(UploadmusicActivity.this, "Recording Completed", Toast.LENGTH_LONG).show();

     }
 });*/


		populatespinner();


	}

	private void xmlonclik() {
		// TODO Auto-generated method stub
		//addrecord_img.setOnClickListener(this);
		add_img.setOnClickListener(this);
		upload_tv.setOnClickListener(this);
		upload_tv_.setOnClickListener(this);
		uploadmusic_tv.setOnClickListener(this);
		desclaimer_tv.setOnClickListener(this);

	}


}













